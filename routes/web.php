<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

\App\Cms\Routes::all();

Route::get('/', function () {
    return view('welcome');
});
Route::get('/install', function () {
    if (Schema::hasTable('migrations'))
        abort(404, 'Таблицы в БД существуют!');

    Artisan::call('key:generate');
    Artisan::call('migrate');
    Artisan::call('db:seed');
    Artisan::call('storage:link');

    return redirect('/');
});