<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Языковые ресурсы аутентификации
    |--------------------------------------------------------------------------
    |
    | Следующие языковые ресурсы используются во время аутентификации для
    | различных сообщений которые мы должны вывести пользователю на экран.
    | Вы можете свободно изменять эти языковые ресурсы в соответствии
    | с требованиями вашего приложения.
    |
    */

//1xx: Informational (информационные):
    '100' => 'Продолжай',
    '101' => 'Переключение протоколов',
    '102' => 'Идёт обработка',
//2xx: Success (успешно):
    '200' => 'Хорошо',
    '201' => 'Создано',
    '202' => 'Принято',
    '203' => 'Информация не авторитетна',
    '204' => 'Нет содержимого',
    '205' => 'Сбросить содержимое',
    '206' => 'Частичное содержимое',
    '207' => 'Многостатусный',
    '208' => 'Уже сообщалось',
    '226' => 'Использовано IM',
//3xx: Redirection (перенаправление):
    '300' => 'Множество выборов',
    '301' => 'Перемещено навсегда',
    '302' => 'Перемещено временно',
    '302-2' => 'Найдено',
    '303' => 'Смотреть другое',
    '304' => 'Не изменялось',
    '305' => 'Использовать прокси',
    '306' => ' — зарезервировано (код использовался только в ранних спецификациях)',
    '307' => 'Временное перенаправление',
    '308' => 'Постоянное перенаправление',
//4xx: Client Error (ошибка клиента):
    '400' => 'Плохой, неверный запрос',
    '401' => 'Не авторизован (не представился)',
    '402' => 'Необходима оплата',
    '403' => 'Запрещено',
    '404' => 'Не найдено',
    '405' => 'Метод не поддерживается',
    '406' => 'Неприемлемо',
    '407' => 'Необходима аутентификация прокси',
    '408' => 'Истекло время ожидания',
    '409' => 'Конфликт',
    '410' => 'Удалён',
    '411' => 'Необходима длина',
    '412' => 'Условие ложно',
    '413' => 'Полезная нагрузка слишком велика',
    '414' => 'URI слишком длинный',
    '415' => 'Неподдерживаемый тип данных',
    '416' => 'Диапазон не достижим',
    '417' => 'Ожидание не удалось',
    '418' => 'Я — чайник',
    '421' => 'Misdirected Request',
    '422' => 'Необрабатываемый экземпляр',
    '423' => 'Заблокировано',
    '424' => 'Невыполненная зависимость',
    '426' => 'Необходимо обновление',
    '428' => 'Необходимо предусловие',
    '429' => 'Слишком много запросов',
    '431' => 'Поля заголовка запроса слишком большие',
    '449' => 'Пповторить с',
    '451' => 'Недоступно по юридическим причинам',
//5xx: Server Error (ошибка сервера):
    '500' => 'Внутренняя ошибка сервера',
    '501' => 'Не реализовано',
    '502' => 'Плохой, ошибочный шлюз',
    '503' => 'Сервис недоступен',
    '504' => 'Шлюз не отвечает',
    '505' => 'Версия HTTP не поддерживается',
    '506' => 'Вариант тоже проводит согласование',
    '507' => 'Переполнение хранилища',
    '508' => 'Обнаружено бесконечное перенаправление',
    '509' => 'Исчерпана пропускная ширина канала',
    '510' => 'Не расширено',
    '511' => 'Требуется сетевая аутентификация',
    '520' => 'Неизвестная ошибка',
    '521' => 'Веб-сервер не работает',
    '522' => 'Соединение не отвечает',
    '523' => 'Источник недоступен',
    '524' => 'Время ожидания истекло',
    '525' => 'Квитирование SSL не удалось',
    '526' => 'Недействительный сертификат SSL',
];
