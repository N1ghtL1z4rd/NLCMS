<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Языковые ресурсы панели администратора
    |--------------------------------------------------------------------------
    |
    |
    */

    'admin' => 'Admin panel',
    'site-settings' => [
        'index' => 'Site settings',
        'edit' => 'Editing site settings'
    ],
    'sections' => 'List of site sections',
    'users' => 'Users',
    'groups' => 'Groups',
    'setting-lists' => 'Configurable parameter',
    'setting-list-objects' => 'Configuration objects',
    'data-types' => 'Data type',
    'entity-types' => 'Entity type',
    'entities' => 'Entities',
    'entity-elements' => 'Entity elements',
    'properties' => 'Entity property',
    'journal' => 'Journal',
    'journal-objects' => 'Journal objects',
    'journal-operation-types' => 'Type of operation',
    'php-info' => 'Information about PHP',


    'edit' => 'Editing :name',
    'create' => 'Adding :name',
    'show' => 'View :name',
    'tmp-create' => [
        'withoutSubName' => 'Adding :name',
        'withSubName' => 'Adding :name ":subName"'
    ],
    'message' => [
        'record-added' => 'Record added',
        'record-changed' => 'Record changed',
        'record-deleted' => 'Record deleted',
        'changes-saved' => 'The changes are saved',
        'code-exists' => 'Record with the same character code already exists',
        'many-code-exists' => 'Multiple records found with the same character code',
        'not-remove' => 'Removal is not possible. Elements are bound to this type, first remove them',
        'delete-true' => 'Are you sure you want to delete the record?',
    ],
    'button' => [
        'add' => 'Add',
        'edit' => 'Edit',
        'save' => 'Save',
        'delete' => 'Delete',
        'cancel' => 'Cancel',
        'previous' => 'Previous',
        'next' => 'Next',
    ],
];
