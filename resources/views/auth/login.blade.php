@extends('layouts.login')

@section('content')
    <div class="container">
        <div class="col-lg-4 col-md-6 col-sm-6 ml-auto mr-auto mt-auto" style="">
            <form class="form" method="POST" aria-label="{{ __('Login') }}" action="{{ route('login') }}">
                @csrf
                <div class="card card-login">
                    <div class="card-header card-header-primary text-center">
                        <h4 class="card-title py-5">{{ trans('auth.authorization') }}</h4>
                    </div>

                    <div class="card-body">
                        <div class="form-group">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fas fa-envelope"></i>
                                    </span>
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <input id="email" type="email" placeholder="Email..." name="email" required autofocus
                                       class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                       value="{{ old('email') }}">
                            </div>

                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fas fa-key"></i>
                                    </span>
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <input id="password" type="password" name="password" placeholder="Password..." required
                                       class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}">
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fas fa-save text-white"></i>
                                    </span>
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox" name="remember"
                                               id="remember" {{ old('remember') ? 'checked' : '' }}>
                                        {{ __('Запомнить') }}
                                        <span class="form-check-sign">
                                            <span class="check"></span>
                                        </span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-footer justify-content-center">
                        <button type="submit" class="btn btn-outline-primary">
                            {{ trans('auth.enter') }}
                        </button>
                    </div>
                </div>

            </form>
        </div>
    </div>
@endsection
