@extends('layouts.admin')
@section('content')
    <div class="card-header">
        <div class="row">
            <ol class="breadcrumb bg-white py-0">
                <li class="breadcrumb-item">Контент</li>
                <li class="breadcrumb-item">
                    <a href="{{ route('admin-entity-types.index') }}">Типы сущностей</a>
                </li>
            </ol>
        </div>
        <h4 class="card-title">{{trans('admin.create',['name'=>'типа сущности'])}}</h4>
    </div>
    <div class="card-body">
        <form method="POST" role="form" action="{{ route('admin-entity-types.store') }}" class="form-horizontal"
              enctype="multipart/form-data">
            @csrf
            @include ('admin.entity-types.form')
        </form>
    </div>
@endsection
