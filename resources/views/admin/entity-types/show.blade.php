@extends('layouts.admin')
@section('content')
    <div class="card-header">
        <div class="row">
            <ol class="breadcrumb bg-white py-0">
                <li class="breadcrumb-item">Контент</li>
                <li class="breadcrumb-item">
                    <a href="{{ route('admin-entity-types.index') }}">Типы сущностей</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">{{$entityType->name}}</li>
            </ol>
        </div>
        <h4 class="card-title float-left">
            {{trans('admin.entities')}} типа "{{$entityType->name}}"
            @if($access['edit'])
                <div class="btn-group">
                    <a href="#" class="btn btn-link btn-primary p-0 mx-3" data-toggle="dropdown">
                        <i class="fas fa-bars fa-2x"></i>
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item"
                           href="{{ route('admin-entity-types.edit',['edit'=>$entityType->id]) }}">
                            {{trans('admin.button.edit')}} тип
                        </a>
                    </div>
                </div>
            @endif
        </h4>
        @if($access['edit'])
            <a href="{{ route('admin-entities.create',['entity_type_id' => $entityType['id']]) }}"
               class="btn btn-outline-info btn-sm float-right">
                {{trans('admin.button.add')}}
            </a>
        @endif
    </div>
    <div class="card-body">
        <form method="POST" action="{{ route('admin-entities.destroy') }}" id="chActionsForm">
            @method('DELETE')
            @csrf
            <table class="table table-striped table-bordered dataTable-table-ajax"></table>
        </form>
        <hr>
        @if($access['edit'])
            <div class="dropdown d-none" id="itemsListActions">
                <button class="btn btn-sm btn-outline-warning dropdown-toggle" data-toggle="dropdown">
                    Действия
                </button>
                <div class="dropdown-menu px-1">
                    @if($access['delete'])
                        <button class="dropdown-item bg-hover-danger w-100 m-0" type="submit" form="chActionsForm">
                            Удалить
                        </button>
                    @endif
                </div>
            </div>
        @endif
    </div>
@endsection