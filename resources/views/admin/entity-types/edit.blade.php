@extends('layouts.admin')
@section('content')
    <div class="card-header">
        <div class="row">
            <ol class="breadcrumb bg-white py-0">
                <li class="breadcrumb-item">Контент</li>
                <li class="breadcrumb-item">
                    <a href="{{ route('admin-entity-types.index') }}">Типы сущностей</a>
                </li>
            </ol>
        </div>
        <h4 class="card-title">{{trans('admin.edit',['name'=>'типа сущности'])}} "{{ $name }}"</h4>
    </div>
    <div class="card-body">
        <form method="POST" role="form" action="{{ route('admin-entity-types.update',['id' => $id]) }}"
              class="form-horizontal"
              enctype="multipart/form-data">
            @method('PATCH')
            @csrf
            @include ('admin.entity-types.form')
        </form>
    </div>
@endsection