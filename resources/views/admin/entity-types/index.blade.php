@extends('layouts.admin')
@section('content')
    <div class="card-header">
        <div class="row">
            <ol class="breadcrumb bg-white py-0">
                <li class="breadcrumb-item">Контент</li>
                <li class="breadcrumb-item active"
                    aria-current="page">Типы сущностей
                </li>
            </ol>
        </div>
        <h4 class="card-title float-left">{{trans('admin.entity-types')}}</h4>
        @if($access['edit'])
            <a href="{{ route('admin-entity-types.create') }}" class="btn btn-outline-info btn-sm float-right">
                {{trans('admin.button.add')}}
            </a>
        @endif
    </div>
    <div class="card-body">
        <form method="POST" action="{{ route('admin-entity-types.destroy') }}" id="chActionsForm">
            @method('DELETE')
            @csrf
            <table class="table table-striped table-bordered dataTable-table-ajax"></table>
        </form>
        <hr>
        @if($access['edit'])
            <div class="dropdown d-none" id="itemsListActions">
                <button class="btn btn-sm btn-outline-warning dropdown-toggle" data-toggle="dropdown">
                    Действия
                </button>
                <div class="dropdown-menu px-1">
                    @if($access['delete'])
                        <button class="dropdown-item bg-hover-danger w-100 m-0" type="submit" form="chActionsForm">
                            Удалить
                        </button>
                    @endif
                </div>
            </div>
        @endif
    </div>
@endsection