@extends('layouts.admin')
@section('content')
    <div class="card-header card-header-icon">
        <h4 class="card-title">{{ trans('admin.journal') }}</h4>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-striped table-bordered dataTable-table-ajax"></table>
        </div>
    </div>
@endsection