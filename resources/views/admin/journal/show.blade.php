@extends('layouts.admin')
@section('content')
    <div class="card-header card-header-icon">
        <h4 class="card-title">{{trans('admin.show',['name'=>'элемента'])}}</h4>
    </div>
    <div class="card-body">
        <div class="row form-group">
            <div class="col-md-6">ID элемента: {{ $item->element_id }}</div>
            <div class="col-md-6 text-right text-muted">{{ $item->created_at }}</div>
        </div>
        <hr>
        <div class="row form-group">
            <div class="col-md-12">Наименование: {{ $item->element->name }}</div>
        </div>
        <div class="row form-group">
            <div class="col-md-6">Тип операции: {{ $item->operation->name }}</div>
        </div>
        <div class="row form-group">
            <div class="col-md-12">Описание:</div>
            <div class="col-md-12">
                @if(!empty($item->condition))
                    @php
                        dump(json_decode($item->condition));
                    @endphp
                @endif
            </div>
        </div>
    </div>
@endsection