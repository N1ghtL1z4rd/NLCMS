@extends('layouts.admin')
@section('content')
    <div class="card-header">
        <h4 class="card-title float-left">{{trans('admin.backups')}}</h4>
        <a href="{{ route('admin-backups.create') }}" class="btn btn-outline-info btn-sm float-right">
            {{trans('admin.button.create-backup')}}
        </a>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-striped table-bordered dataTable-table">
                <thead>
                <tr>
                    <th class="">#</th>
                    <th class="">Наименование</th>
                    <th class="text-center">Размер</th>
                    <th class="text-center">Действия</th>
                </tr>
                </thead>
                <tbody>
                @if($items)
                    @foreach($items as $item)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $item->getBasename() }}</td>
                            <td class="text-center">{{ App\Cms\Helpers::bytesToHuman($item->getSize()) }}</td>
                            <td class="td-actions text-center">
                                <div class="btn-group">
                                    <a href="{{ route('admin-backups.show',['file'=>$item->getBasename()]) }}"
                                       class="btn btn-primary btn-link" rel="tooltip" target="_blank">
                                        <i class="fas fa-download"></i>
                                    </a>
                                    @if($access['delete'])
                                        {!! Form::open(['method' => 'DELETE', 'route' => ['admin-backups.destroy', $item->getBasename()]]) !!}
                                        <button class="btn btn-danger btn-link" type="submit"
                                                onclick="return confirm('{{trans('admin.message.delete-true')}}');">
                                            <i class="fas fa-trash-alt" aria-hidden="true"></i>
                                        </button>
                                        {!! Form::close() !!}
                                    @endif
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection
