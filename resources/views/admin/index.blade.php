@extends('layouts.admin')
@section('content')
    <div class="card-header">
        <h4 class="card-title float-left">{{trans('admin.dashboard')}}</h4>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header card-header-warning card-header-icon">
                        <div class="card-icon">
                            <i class="fas fa-code"></i>
                        </div>
                        <p class="card-category">HTML кэш</p>
                        <h3 class="card-title">
                            <small>файлов:</small>
                            {{ count(File::allFiles('../storage/framework/views')) }}
                        </h3>
                    </div>
                    <div class="card-footer">
                        <form method="POST" role="form" action="{{ route('admin-commands.run') }}"
                              class="form-horizontal" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="command" value="view:clear"/>
                            <button class="btn btn-sm btn-outline-warning" type="submit"> Очистить</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header card-header-success card-header-icon">
                        <div class="card-icon">
                            <i class="fas fa-user"></i>
                        </div>
                        <p class="card-category">Пользователей</p>
                        <h3 class="card-title">
                            {{ \App\User::count() }}
                        </h3>
                    </div>
                    <div class="card-footer"></div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header card-header-danger card-header-icon">
                        <div class="card-icon">
                            <i class="fas fa-users"></i>
                        </div>
                        <p class="card-category">Групп</p>
                        <h3 class="card-title">
                            {{ \App\Group::count() }}
                        </h3>
                    </div>
                    <div class="card-footer"></div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header card-header-info card-header-icon">
                        <div class="card-icon">
                            <i class="far fa-clone"></i>
                        </div>
                        <p class="card-category">Элементов</p>
                        <h3 class="card-title">
                            {{ \App\EntityElement::count() }}
                        </h3>
                    </div>
                    <div class="card-footer"></div>
                </div>
            </div>
        </div>
    </div>
@endsection