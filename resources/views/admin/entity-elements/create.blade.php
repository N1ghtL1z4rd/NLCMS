@extends('layouts.admin')
@section('content')
    <div class="card-header">
        <div class="row">
            <ol class="breadcrumb bg-white py-0">
                <li class="breadcrumb-item">Контент</li>
                <li class="breadcrumb-item">
                    <a href="{{ route('admin-entity-types.index') }}">Типы сущностей</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route('admin-entity-types.show',['id' => $entity->entityType->id]) }}">{{ $entity->entityType->name }}</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route('admin-entities.show',['id' => $entity->id]) }}">{{ $entity->name }}</a>
                </li>
            </ol>
        </div>
        <h4 class="card-title">
            {{trans('admin.create',['name'=>'элемента сущности'])}} "{{ $entity->name }}"
        </h4>
    </div>
    <div class="card-body">
        <ul class="nav nav-pills" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" href="#element" data-toggle="tab">
                    Основное
                </a>
            </li>
            @if($properties)
                <li class="nav-item">
                    <a class="nav-link" href="#properties" data-toggle="tab">
                        Свойства
                    </a>
                </li>
            @endif
        </ul>
        <form method="POST" role="form" action="{{ route('admin-entity-elements.store') }}" class="form-horizontal"
              enctype="multipart/form-data">
            @csrf
            <div class="tab-content pt-4">
                <div class="tab-pane active" id="element">

                    <input type="hidden" name="entity_id" value="{{$entity['id']}}">

                    <div class="row form-group">
                        <label class="col-md-3 pt-3 text-right">Активность </label>
                        <div class="col-md-9">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" value="1" name="active"
                                           checked="checked">
                                    <span class="form-check-sign">
                                        <span class="check"></span>
                                    </span>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="row form-group">
                        <label class="col-md-3 col-form-label" for="name">Наименование</label>
                        <div class="col-md-9">
                            <div class="form-group">
                                <input type="text" class="form-control" id="name" name="name" required="required">
                            </div>
                        </div>
                    </div>

                    <div class="row form-group">
                        <label class="col-md-3 col-form-label" for="code">Символьный код</label>
                        <div class="col-md-9">
                            <div class="form-group">
                                <input type="text" class="form-control" id="code" name="code">
                            </div>
                        </div>
                    </div>

                    <div class="row form-group">
                        <label class="col-md-3 col-form-label" for="extended_id">Расширенный
                            идентификатор</label>
                        <div class="col-md-9">
                            <div class="form-group">
                                <input type="number" class="form-control" id="extended_id" name="extended_id">
                            </div>
                        </div>
                    </div>

                    <div class="row form-group">
                        <label class="col-md-3 col-form-label" for="sort">Сортировка</label>
                        <div class="col-md-9">
                            <div class="form-group">
                                <input type="number" class="form-control" id="sort" name="sort"
                                       required="required" value="500">
                            </div>
                        </div>
                    </div>

                    <hr/>

                    <div class="row form-group">
                        <label class="col-md-3 col-form-label">Картинка для анонса</label>
                        <div class="col-md-9">
                            <div class="form-group clearfix">
                                <div class="col-2 float-left">
                                    <div class="col-12 btn btn-outline-primary btn-sm float-left">
                                        <span>Выберите файл</span>
                                        <input type="file" id="preview_img"
                                               name="preview_img"
                                               oninput="fileInputHandler(this)">
                                    </div>
                                </div>
                                <div class="col-8 float-left">
                                    <input class="col-12 form-control file_name-preview_img"
                                           type="text"
                                           placeholder="Выберите файл" disabled="disabled">
                                </div>
                                <div class="col-2 float-left">
                                    <div class="float-right input-group-btn">
                                        <button type="button" class="btn btn-sm btn-link btn-danger"
                                                data-property_id="preview_img"
                                                onclick="fileDeleteHandler(this)">
                                            <i class="fas fa-trash-alt"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row form-group">
                        <label class="col-md-3 col-form-label" for="preview">Анонс</label>
                        <div class="col-md-9">
                            <div class="form-group">
                                        <textarea class="form-control" id="preview" name="preview"
                                                  rows="5"></textarea>
                            </div>
                        </div>
                    </div>

                    <hr/>

                    <div class="row form-group">
                        <label class="col-md-3 col-form-label">Картинка детальная</label>
                        <div class="col-md-9">
                            <div class="form-group clearfix">
                                <div class="col-2 float-left">
                                    <div class="col-12 btn btn-outline-primary btn-sm float-left">
                                        <span>Выберите файл</span>
                                        <input type="file" id="detail_img"
                                               name="detail_img"
                                               oninput="fileInputHandler(this)">
                                    </div>
                                </div>
                                <div class="col-8 float-left">
                                    <input class="col-12 form-control file_name-detail_img"
                                           type="text"
                                           placeholder="Выберите файл" disabled="disabled">
                                </div>
                                <div class="col-2 float-left">
                                    <div class="float-right input-group-btn">
                                        <button type="button" class="btn btn-sm btn-link btn-danger"
                                                data-property_id="detail_img"
                                                onclick="fileDeleteHandler(this)">
                                            <i class="fas fa-trash-alt"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row form-group">
                        <label class="col-md-3 col-form-label" for="detail">Детальное описание</label>
                        <div class="col-md-9">
                            <div class="form-group">
                                        <textarea class="form-control" id="detail" name="detail"
                                                  rows="5"></textarea>
                            </div>
                        </div>
                    </div>

                </div>

                @if($properties)
                    <div class="tab-pane" id="properties">
                        @foreach($properties as $item)
                            <div class="row form-group">
                                @include('formHelpers.admin.properties-form')
                            </div>
                        @endforeach
                    </div>
                @endif

                <div class="row pt-4">
                    <div class="col-md-12">
                        <a href="{{ route('admin-entities.show',['id' => $entity['id']]) }}"
                           class="btn btn-sm btn-outline-primary float-left">{{trans('admin.button.previous')}}</a>
                        <button type="submit" class="btn btn-sm btn-outline-success float-right">
                            {{trans('admin.button.save')}}
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <script>
        function fileInputHandler(input) {
            if (input.files.length) {
                $(input)
                    .parents('.col-2.float-left')
                    .next()
                    .find('input.file_name-' + input.id)
                    .val(input.files[0].name);
            }
        }

        function fileDeleteHandler(btn) {
            var nameDiv = $(btn).parents('.col-2.float-left').prev();
            var fileDiv = $(nameDiv).prev();
            $(nameDiv).find('input').val('');
            $(fileDiv).find('input').val('');
        }
    </script>
@endsection
