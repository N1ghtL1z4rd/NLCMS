@extends('layouts.admin')
@section('content')
    <div class="card-header">
        <h4 class="card-title">{{trans('admin.show',['name'=>'настроек сайта'])}}</h4>
    </div>
    <div class="card-body">
        @foreach($items as $item)
            <div class="row form-group">
                @switch($item['data_type_id'])
                    @case(\App\DataType::STRING)
                    <label class="col-md-3 col-form-label" for="{{ $item['id'] }}">{{ $item['name'] }}</label>
                    <div class="col-md-9">
                        {{-- Если множественное --}}
                        @if($item['multiple'])
                            {{--Если есть данные по свойству, то вывести в цикле --}}
                            @if(!empty($propertiesData[$item['id']]))
                                @foreach($propertiesData[$item['id']] as $key => $val)
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="{{ $item['id'] }}"
                                               value="{{ $val['value'] ?? '' }}" disabled="disabled">
                                    </div>
                                @endforeach
                                {{--Если нет данные, то пустой input--}}
                            @else
                                <div class="form-group">
                                    <input type="text" class="form-control" id="{{ $item['id'] }}" disabled="disabled">
                                </div>
                            @endif
                            {{--Если не множественное, то вывести первый элемент из массива--}}
                        @else
                            <div class="form-group">
                                <input type="text" class="form-control" id="{{ $item['id'] }}"
                                       value="{{ $propertiesData[$item['id']][0]['value'] ?? '' }}" disabled="disabled">
                            </div>
                        @endif
                    </div>
                    @break

                    @case(\App\DataType::INTEGER)
                    <label class="col-md-3 col-form-label" for="{{ $item['id'] }}">{{ $item['name'] }}</label>
                    <div class="col-md-9">
                        {{-- Если множественное --}}
                        @if($item['multiple'])
                            {{--Если есть данные по свойству, то вывести в цикле --}}
                            @if(!empty($propertiesData[$item['id']]))
                                @foreach($propertiesData[$item['id']] as $key => $val)
                                    <div class="form-group">
                                        <input type="number" class="form-control" id="{{ $item['id'] }}"
                                               value="{{ $val['value'] ?? '' }}" disabled="disabled">
                                    </div>
                                @endforeach
                                {{--Если нет данные, то пустой input--}}
                            @else
                                <div class="form-group">
                                    <input type="number" class="form-control" id="{{ $item['id'] }}"
                                           disabled="disabled">
                                </div>
                            @endif
                            {{--Если не множественное, то вывести первый элемент из массива--}}
                        @else
                            <div class="form-group">
                                <input type="number" class="form-control" id="{{ $item['id'] }}"
                                       value="{{ $propertiesData[$item['id']][0]['value'] ?? '' }}" disabled="disabled">
                            </div>
                        @endif
                    </div>
                    @break

                    @case(\App\DataType::TEXT)
                    <label class="col-md-3 col-form-label" for="{{ $item['id'] }}">{{ $item['name'] }}</label>
                    <div class="col-md-9">
                        {{-- Если множественное --}}
                        @if($item['multiple'])
                            {{--Если есть данные по свойству, то вывести в цикле --}}
                            @if(!empty($propertiesData[$item['id']]))
                                @foreach($propertiesData[$item['id']] as $key => $val)
                                    <div class="form-group">
                                        <textarea class="form-control" id="{{ $item['id'] }}" rows="5"
                                                  disabled="disabled">{{ $val['value'] ?? '' }}</textarea>
                                    </div>
                                @endforeach
                                {{--Если нет данные, то пустой input--}}
                            @else
                                <div class="form-group">
                                    <textarea class="form-control" id="{{ $item['id'] }}" rows="5"
                                              disabled="disabled"></textarea>
                                </div>
                            @endif
                            {{--Если не множественное, то вывести первый элемент из массива--}}
                        @else
                            <div class="form-group">
                                <textarea class="form-control" id="{{ $item['id'] }}" rows="5"
                                          disabled="disabled">{{ $propertiesData[$item['id']][0]['value'] ?? '' }}</textarea>
                            </div>
                        @endif
                    </div>
                    @break

                    @case(\App\DataType::CHECKBOX)
                    <label class="col-md-3"></label>
                    <div class="col-md-9">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="checkbox" value="1" disabled="disabled"
                                        {{ !empty($propertiesData[$item['id']][0]['value']) ? ' checked="checked"' : ''}}>
                                {{ $item['name'] }}
                                <span class="form-check-sign">
                                    <span class="check"></span>
                                </span>
                            </label>
                        </div>
                    </div>
                    @break

                    @case(\App\DataType::DATE)
                    <label class="col-md-3 col-form-label" for="{{ $item['id'] }}">{{ $item['name'] }}</label>
                    <div class="col-md-9">
                        <div class="form-group">
                            <input type="text" class="form-control datepicker" id="{{ $item['id'] }}"
                                   value="{{ $propertiesData[$item['id']][0]['value'] ?? '' }}" disabled="disabled">
                        </div>
                    </div>
                    @break

                    @case(\App\DataType::DATETIME)
                    <label class="col-md-3 col-form-label" for="{{ $item['id'] }}">{{ $item['name'] }}</label>
                    <div class="col-md-9">
                        <div class="form-group">
                            <input type="text" class="form-control datetimepicker" id="{{ $item['id'] }}"
                                   value="{{ $propertiesData[$item['id']][0]['value'] ?? '' }}" disabled="disabled">
                        </div>
                    </div>
                    @break

                    @case(\App\DataType::SELECT)
                    <label class="col-md-3 col-form-label" for="{{ $item['id'] }}">{{ $item['name'] }}</label>
                    <div class="col-md-9">
                        <div class="form-group">
                            @php
                                $setOptions = [];
                                if(!empty($propertiesData[$item['id']]))
                                    $setOptions = collect($propertiesData[$item['id']])->keyBy('value')->toArray();
                            @endphp
                            <select class="form-control select2-material" id="{{ $item['id'] }}"
                                    {{ $item['multiple'] ? ' multiple="multiple"' : ''}}
                                    disabled="disabled">
                                @if(empty($item['required']) && empty($item['multiple']))
                                    <option></option>
                                @endif
                                @if(isset($item['options']))
                                    @foreach($item['options'] as $option)
                                        <option value="{{ $option['id'] }}"
                                                {{ isset($setOptions[$option['id']]) ? ' selected="selected"' : ''}}
                                        >{{ $option['name'] }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    @break

                    @case(\App\DataType::ENTITY)
                    <label class="col-md-3 col-form-label" for="{{ $item['id'] }}">{{ $item['name'] }}</label>
                    <div class="col-md-9">
                        <div class="form-group">
                            @php
                                $setEntities = [];
                                if(!empty($propertiesData[$item['id']]))
                                    $setEntities = collect($propertiesData[$item['id']])->keyBy('value')->toArray();
                            @endphp
                            <select class="form-control select2-material" id="{{ $item['id'] }}" disabled="disabled"
                                    {{ $item['multiple'] ? ' multiple="multiple"' : ''}}>
                                @if(empty($item['required']) && empty($item['multiple']))
                                    <option></option>
                                @endif
                                @if(isset($item['relations']))
                                    @foreach($item['relations'] as $entity)
                                        @if(isset($entity['elements']))
                                            @foreach($entity['elements'] as $eElement)
                                                <option value="{{ $eElement['id'] }}"
                                                        {{ isset($setEntities[$eElement['id']]) ? ' selected="selected"' : ''}}>
                                                    ({{ $eElement['id'] }}) {{ $eElement['name'] }}</option>
                                            @endforeach
                                        @endif
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    @break

                    @case(\App\DataType::FILE)
                    <label class="col-md-3 col-form-label">{{ $item['name'] }}</label>
                    <div class="col-md-9">
                        {{-- Если множественное --}}
                        @if($item['multiple'])
                            {{--Если есть данные по свойству, то вывести в цикле --}}
                            @if(!empty($propertiesData[$item['id']]))
                                @foreach($propertiesData[$item['id']] as $key => $val)
                                    <div class="form-group clearfix">
                                        <div class="col-2 float-left">
                                            <div class="col-12 btn btn-outline-primary btn-sm float-left">
                                                <span>Выберите файл</span>
                                                <input type="hidden"
                                                       value="{{$val['value'] ?? ''}}" disabled="disabled">
                                                <input type="file" id="{{ $item['id'] }}"
                                                       value="{{ $val['file']['name'] ?? '' }}"
                                                       oninput="fileInputHandler(this)" disabled="disabled">
                                            </div>
                                        </div>
                                        <div class="col-8 float-left">
                                            <input class="col-12 form-control file_name-{{ $item['id'] }}"
                                                   type="text"
                                                   value="{{ $val['file']['name'] ?? '' }}"
                                                   placeholder="Выберите файл"
                                                   disabled="disabled">
                                        </div>
                                        <div class="col-2 float-left">
                                            <div class="float-right input-group-btn">
                                                @if(!empty($val['file']))
                                                    <a href="{{ Storage::url($val['file']['path']) }}"
                                                       class="btn btn-sm btn-link btn-primary"
                                                       id="link-file-{{$item['id']}}">
                                                        <i class="fas fa-download"></i>
                                                    </a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                {{--Если нет данные, то пустой input--}}
                            @else
                                <div class="form-group clearfix">
                                    <div class="col-2 float-left">
                                        <div class="col-12 btn btn-outline-primary btn-sm float-left">
                                            <span>Выберите файл</span>
                                            <input type="file" id="{{ $item['id'] }}" disabled="disabled">
                                        </div>
                                    </div>
                                    <div class="col-8 float-left">
                                        <input class="col-12 form-control file_name-{{ $item['id'] }}"
                                               type="text" placeholder="Выберите файл" disabled="disabled">
                                    </div>
                                    <div class="col-2 float-left"></div>
                                </div>
                            @endif
                            {{--Если не множественное--}}
                        @else
                            {{--Если есть данные по свойству, то вывести первый элемент из массива --}}
                            @if(!empty($propertiesData[$item['id']]))
                                <div class="form-group clearfix">
                                    <div class="col-2 float-left">
                                        <div class="col-12 btn btn-outline-primary btn-sm float-left">
                                            <span>Выберите файл</span>
                                            <input type="hidden" disabled="disabled"
                                                   value="{{head($propertiesData[$item['id']])['value'] ?? ''}}">
                                            <input type="file" id="{{ $item['id'] }}" disabled="disabled">
                                        </div>
                                    </div>
                                    <div class="col-8 float-left">
                                        <input class="col-12 form-control file_name-{{ $item['id'] }}"
                                               type="text" placeholder="Выберите файл"
                                               disabled="disabled"
                                               value="{{!empty(head($propertiesData[$item['id']])['file']['name'])
                                                                   ? head($propertiesData[$item['id']])['file']['name'] : '' }}">
                                    </div>
                                    <div class="col-2 float-left">
                                        <div class="float-right input-group-btn">
                                            @if(!empty($propertiesData[$item['id']][0]['file']))
                                                <a href="{{ Storage::url(head($propertiesData[$item['id']])['file']['path']) }}"
                                                   class="btn btn-sm btn-link btn-primary"
                                                   id="link-file-{{$item['id']}}">
                                                    <i class="fas fa-download"></i>
                                                </a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                {{--Если нет данные, то пустой input--}}
                            @else
                                <div class="form-group clearfix">
                                    <div class="col-2 float-left">
                                        <div class="col-12 btn btn-outline-primary btn-sm float-left">
                                            <span>Выберите файл</span>
                                            <input type="file" id="{{ $item['id'] }}" disabled="disabled">
                                        </div>
                                    </div>
                                    <div class="col-8 float-left">
                                        <input class="col-12 form-control file_name-{{ $item['id'] }}"
                                               type="text"
                                               placeholder="Выберите файл"
                                               disabled="disabled">
                                    </div>
                                    <div class="col-2 float-left"></div>
                                </div>
                            @endif
                        @endif
                    </div>
                    @break

                    @case(\App\DataType::USER)
                    <label class="col-md-3 col-form-label" for="{{ $item['id'] }}">{{ $item['name'] }}</label>
                    <div class="col-md-9">
                        <div class="form-group">
                            @php
                                $setUsers = [];
                                if(!empty($propertiesData[$item['id']]))
                                    $setUsers = collect($propertiesData[$item['id']])->keyBy('value')->toArray();
                            @endphp
                            <select class="form-control select2-material" id="{{ $item['id'] }}" disabled="disabled"
                                    {{ $item['multiple'] ? ' multiple="multiple"' : ''}}>
                                @if(empty($item['required']) && empty($item['multiple']))
                                    <option></option>
                                @endif
                                @if(isset($item['options']))
                                    @foreach($item['options'] as $option)
                                        <option value="{{ $option['id'] }}"
                                                {{ isset($setUsers[$option['id']]) ? ' selected="selected"' : ''}}
                                        >({{ $option['id'] }}) {{ $option['name'] }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    @break
                @endswitch
            </div>
        @endforeach
        @if ($access['edit'])
            <div class="col-md-12">
                <a href="{{ route('admin-site-settings.edit') }}"
                   class="btn btn-sm btn-outline-danger float-right">
                    {{trans('admin.button.edit')}}
                </a>
            </div>
        @endif
    </div>
    <script>
        function fileInputHandler(input) {
            if (input.files.length) {
                $('#file_name-' + input.id).val(input.files[0].name);
                $('input[type=hidden][name=' + input.id + ']').remove();
            }
        }

        function fileDeleteHandler(id) {
            console.log(id);
            $('#file_name-' + id).val('');
            $('input[type=file][name="files[' + id + ']"]').val('');
            $('input[type=hidden][name=' + id + ']').remove();
            $('a#link-file-' + id).remove();
        }
    </script>
@endsection
