@extends('layouts.admin')
@section('content')
    <div class="card-header">
        <h4 class="card-title">{{trans('admin.site-settings.edit')}}</h4>
    </div>
    <div class="card-body">

        @if ($errors->any())
            <ul class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif

        <form method="POST" role="form" action="{{ action('Admin\SiteSettingsController@update') }}"
              class="form-horizontal"
              enctype="multipart/form-data">
            @method('PATCH')
            @csrf
            @foreach($items as $item)
                @php
                    $item['required'] = false;
                @endphp
                <div class="row form-group">
                    @include('formHelpers.admin.properties-form')
                </div>
            @endforeach
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-sm btn-outline-success float-right">
                        {{trans('admin.button.save')}}
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection

