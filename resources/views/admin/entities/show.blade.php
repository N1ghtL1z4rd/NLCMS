@extends('layouts.admin')
@section('content')

    <link href="{{asset('libs/bootstrap-multiselect/css/bootstrap-multiselect.css')}}" rel="stylesheet"/>
    <script src="{{asset('libs/bootstrap-multiselect/js/bootstrap-multiselect.js')}}"></script>

    <div class="card-header">
        <div class="row">
            <ol class="breadcrumb bg-white py-0">
                <li class="breadcrumb-item">Контент</li>
                <li class="breadcrumb-item">
                    <a href="{{ route('admin-entity-types.index') }}">Типы сущностей</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route('admin-entity-types.show',['id' => $entity->entityType->id]) }}">{{ $entity->entityType->name }}</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">{{ $entity->name }}</li>
            </ol>
        </div>
        <h4 class="card-title float-left">
            {{trans('admin.entity-elements')}} "{{$entity->name}}"
            @if($access['edit'])
                <div class="btn-group">
                    <a href="#" class="btn btn-link btn-primary p-0 mx-3" data-toggle="dropdown">
                        <i class="fas fa-bars fa-2x"></i>
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item"
                           href="{{ route('admin-entities.edit',['edit'=>$entity->id]) }}">
                            {{trans('admin.button.edit')}} сущность
                        </a>
                    </div>
                </div>
            @endif
        </h4>
        @if($access['edit'])
            <a href="{{ route('admin-entity-elements.create',['entity_id' => $entity['id']]) }}"
               class="btn btn-outline-info btn-sm float-right">
                {{trans('admin.button.add')}}
            </a>
        @endif
    </div>
    <div class="card-body pt-0">
        <div class="row">
            <div class="col">
            </div>
            <div class="col">
                <select class="form-control" id="selectedColumns" multiple="multiple">
                    @foreach($tableColumns as $column)
                        <option value="{{$column['name']}}">({{$column['name']}}) {{$column['title']}}</option>
                    @endforeach
                    @isset($properties)
                        @foreach($properties as $property)
                            <option value="{{$property['id']}}">({{$property['code']}}) {{$property['name']}}</option>
                        @endforeach
                    @endisset
                </select>
            </div>
        </div>
        <form method="POST" action="{{ route('admin-entity-elements.destroy') }}" id="chActionsForm">
            @method('DELETE')
            @csrf
            <table class="table table-bordered dataTable-table-ajax-f"></table>
        </form>
        <hr>
        @if($access['edit'])
            <div class="dropdown d-none" id="itemsListActions">
                <button class="btn btn-sm btn-outline-warning dropdown-toggle" data-toggle="dropdown">
                    Действия
                </button>
                <div class="dropdown-menu px-1">
                    @if($access['delete'])
                        <button class="dropdown-item bg-hover-danger w-100 m-0" type="submit" form="chActionsForm">
                            Удалить
                        </button>
                    @endif
                </div>
            </div>
        @endif
    </div>
    <script>
        @isset($dtSettings)
        $('#selectedColumns').multiselect({
            includeSelectAllOption: true,
            selectAllJustVisible: false,
            numberDisplayed: 1,
            buttonContainer: '<div class="btn-group col-12 px-0 justify-content-end"/>',
            buttonClass: 'btn btn-sm',
            enableFiltering: true,
            maxHeight: 600,
            dropRight: true,
            nonSelectedText: 'Выбор колонок для отображения',
            allSelectedText: 'Выбраны все',
            nSelectedText: ' - выбрано колонок',
            selectAllText: 'Выбрать все',
            filterPlaceholder: 'Поиск',
            templates: {
                ul: '<ul class="multiselect-container dropdown-menu dropdown-menu-right"></ul>',
                filterClearBtn: '<span class="input-group-btn"><button class="btn btn-sm shadow-none btn-white multiselect-clear-filter mt-1" type="button"><i class="fa fas fa-times-circle"></i></button></span>',
                li: '<li><a href="javascript:void(0);"><label class="px-1"></label></a></li>',
            },
            onDropdownHide: function (event) {
                dtTableAjax.api().destroy();
                $('.dataTable-table-ajax-f').empty();
                initDtElements({
                    'csrf': '{{csrf_token()}}',
                    'dTable': '.dataTable-table-ajax-f',
                    'sColumns': '#selectedColumns',
                    'urlColumns': '{{$dtSettings['urlColumns']}}',
                    'urlData': '{{$dtSettings['urlData']}}'
                })
            }
        });
        /**
         * Таблица с ajax загрузкой данных
         * @type {jQuery}
         */
        $(document).ready(function () {
            initDtElements({
                'csrf': '{{csrf_token()}}',
                'dTable': '.dataTable-table-ajax-f',
                'sColumns': '#selectedColumns',
                'urlColumns': '{{$dtSettings['urlColumns']}}',
                'urlData': '{{$dtSettings['urlData']}}'
            })
        });
        @endisset
    </script>
@endsection