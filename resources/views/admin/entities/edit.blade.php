@extends('layouts.admin')
@section('content')
        <div class="card-header">
            <div class="row">
                <ol class="breadcrumb bg-white py-0">
                    <li class="breadcrumb-item">Контент</li>
                    <li class="breadcrumb-item">
                        <a href="{{ route('admin-entity-types.index') }}">Типы сущностей</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="{{ route('admin-entity-types.show',['id' => $entity->entityType->id]) }}">{{ $entity->entityType->name }}</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">{{ $entity->name }}</li>
                </ol>
            </div>
            <h4 class="card-title">{{trans('admin.edit',['name'=>'сущности'])}} "{{ $entity->name }}"</h4>
        </div>
        <div class="card-body">
            <ul class="nav nav-pills" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" href="#main" data-toggle="tab">
                        Основные
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#properties" data-toggle="tab">
                        Свойства
                    </a>
                </li>
            </ul>

            <div class="tab-content pt-4">
                <div class="tab-pane active" id="main">
                    <form method="POST" role="form" action="{{ route('admin-entities.update',['id' => $entity->id]) }}"
                          class="form-horizontal" id="entity-form"
                          enctype="multipart/form-data">
                        @method('PATCH')
                        @csrf

                        <div class="row">
                            <label class="col-md-3 col-form-label" for="name">Наименование</label>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="name" name="name" required="required"
                                           value="{{ $entity->name ?? '' }}">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-md-3 col-form-label" for="description">Описание</label>
                            <div class="col-md-9">
                                <div class="form-group">
                            <textarea class="form-control" id="description" name="description"
                                      rows="5">{!!  $entity->description ?? ''  !!}</textarea>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-md-3 col-form-label" for="code">Символьный код</label>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="code" name="code" required="required"
                                           value="{{ $entity->code ?? '' }}">
                                </div>
                            </div>
                        </div>

                        <input type="hidden" class="form-control" id="" name="entity_type_id" required="required"
                               value="{{ $entity->entityType->id }}">

                        <div class="row">
                            <label class="col-md-3"></label>
                            <div class="col-md-9">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox" value="1"
                                               name="active" {!! empty($entity->active) ? '':'checked="checked"' !!}>
                                        Активность
                                        <span class="form-check-sign">
                                            <span class="check"></span>
                                        </span>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-md-3"></label>
                            <div class="col-md-9">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox" value="1"
                                               name="system" {!! empty($entity->system) ? '':'checked="checked"' !!}>
                                        Системное
                                        <span class="form-check-sign">
                                              <span class="check"></span>
                                        </span>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-md-3"></label>
                            <div class="col-md-9">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox" value="1"
                                               name="logging" {!! empty($entity->logging) ? '':'checked="checked"' !!}>
                                        Журналирование
                                        <span class="form-check-sign">
                                            <span class="check"></span>
                                        </span>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-md-3 col-form-label" for="sort">Сортировка</label>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <input type="number" class="form-control" id="sort" name="sort" required="required"
                                           value="{{ $entity->sort ?? '500' }}">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="tab-pane" id="properties">
                    <a href="{{ route('admin-properties.create',['entity_id' => $entity->id]) }}"
                       class="btn btn-outline-info btn-sm float-right">
                        {{trans('admin.button.add')}}
                    </a>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered dataTable-table">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Наименование</th>
                                <th class="text-center">Активность</th>
                                <th class="text-center">Множественное</th>
                                <th class="text-center">Обязательность</th>
                                <th class="text-center">Сортировка</th>
                                <th class="text-center">Действия</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($properties as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>
                                        <a href="{{ route('admin-properties.edit',['id' => $item->id]) }}">
                                            {{ $item->name }}
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        @if(!empty($item->active))
                                            <i class="fas fa-check"></i>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        @if(!empty($item->multiple))
                                            <i class="fas fa-check"></i>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        @if(!empty($item->required))
                                            <i class="fas fa-check"></i>
                                        @endif
                                    </td>
                                    <td class="text-center">{{ $item->sort }}</td>

                                    <td class="td-actions text-center d-block">
                                        <div class="btn-group">
                                            <a class="btn btn-warning btn-link" rel="tooltip"
                                               href="{{ route('admin-properties.edit',['id'=>$item->id]) }}">
                                                <i class="fas fa-pencil-alt" aria-hidden="true"></i>
                                            </a>
                                            {!! Form::open(['method' => 'DELETE', 'route' => ['admin-properties.destroy', $item->id],'id' => 'property-delete']) !!}
                                            <button class="btn btn-danger btn-link" type="submit"
                                                    form="property-delete"
                                                    onclick="return confirm('{{trans('admin.message.delete-true')}}');">
                                                <i class="fas fa-trash-alt" aria-hidden="true"></i>
                                            </button>
                                            {!! Form::close() !!}
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <a href="{{ route('admin-entity-types.show',['id' => $entityType->id]) }}"
                           class="btn btn-sm btn-outline-primary float-left">{{trans('admin.button.previous')}}</a>
                        <button type="submit" form="entity-form"
                                class="btn btn-sm btn-outline-success float-right">{{trans('admin.button.save')}}</button>
                    </div>
                </div>
            </div>
        </div>
@endsection