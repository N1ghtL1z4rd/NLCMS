@extends('layouts.admin')
@section('content')
    <div class="card-header">
        <div class="row">
            <ol class="breadcrumb bg-white py-0">
                <li class="breadcrumb-item">Контент</li>
                <li class="breadcrumb-item">
                    <a href="{{ route('admin-entity-types.index') }}">Типы сущностей</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route('admin-entity-types.show',['id' => $entityType->id]) }}">{{ $entityType->name }}</a>
                </li>
            </ol>
        </div>
        <h4 class="card-title">{{trans('admin.create',['name'=>'сущности'])}} типа "{{ $entityType->name }}"</h4>
    </div>
    <div class="card-body">
        <form method="POST" role="form" action="{{ route('admin-entities.store') }}" class="form-horizontal"
              enctype="multipart/form-data">
            @csrf
            @include ('admin.entities.form')
        </form>
    </div>
@endsection
