<div class="tab-content pt-4">
    <div class="tab-pane active" id="group">
        <div class="row">
            <label class="col-md-3 col-form-label" for="name">Наименование</label>
            <div class="col-md-9">
                <div class="form-group">
                    <input type="text" class="form-control" id="name" name="name" required="required"
                           value="{{ $element['name'] ?? '' }}">
                </div>
            </div>
        </div>
        <div class="row">
            <label class="col-md-3 col-form-label" for="description">Описание</label>
            <div class="col-md-9">
                <div class="form-group">
            <textarea class="form-control" id="description" name="description"
                      rows="5">{!!  $element['description'] ?? ''  !!}</textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="col-md-3 col-form-label" for="code">Символьный код</label>
            <div class="col-md-9">
                <div class="form-group">
                    <input type="text" class="form-control" id="code" name="code" required="required"
                           value="{{ $element['code'] ?? '' }}">
                </div>
            </div>
        </div>
        <div class="row">
            <label class="col-md-3"></label>
            <div class="col-md-9">
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" value="1"
                               name="active" {!! empty($element['active']) ? '':'checked="checked"' !!}> Активность
                        <span class="form-check-sign">
                      <span class="check"></span>
                    </span>
                    </label>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="col-md-3"></label>
            <div class="col-md-9">
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" value="1"
                               name="system" {!! empty($element['system']) ? '':'checked="checked"' !!}> Системный
                        <span class="form-check-sign">
                      <span class="check"></span>
                </span>
                    </label>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="col-md-3 col-form-label" for="sort">Сортировка</label>
            <div class="col-md-9">
                <div class="form-group">
                    <input type="number" class="form-control" id="sort" name="sort" required="required"
                           value="{{ $element['sort'] ?? '500' }}">
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane" id="access">
        {{--<input type="hidden" name="access_type" value="{{ $access_type['id'] ?? ''}}">--}}
        <input type="hidden" name="access_object_type" value="{{ $access_object_type['id'] ?? ''}}">
        <input type="hidden" name="access_subject_type" value="{{ $access_subject_type['id'] ?? ''}}">
        <table class="table">
            <thead>
            <tr>
                <th class="text-center">ID</th>
                <th>URL</th>
                <th>Название</th>
                <th>Описание</th>
                <th class="text-center">Просмотр</th>
                <th class="text-center">Редактирование</th>
                <th class="text-center">Удаление</th>
            </tr>
            </thead>
            <tbody>
            @if(!empty($sections))
                @foreach($sections as $section)
                    <tr>
                        <td class="text-center">{{$section['id']}}</td>
                        <td><a href="{{$section['url']}}" target="_blank">{{$section['url']}}</a></td>
                        <td>{{$section['name']}}</td>
                        <td>{{$section['description']}}</td>
                        <td class="text-center">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" value="1"
                                           name="access[{{$section['id']}}][{{ \App\AccessType::VIEW }}]"
                                            {!! empty($access[$section['id']][\App\AccessType::VIEW]) ? '':' checked="checked"' !!}>
                                    <span class="form-check-sign">
                                        <span class="check"></span>
                                    </span>
                                </label>
                            </div>
                        </td>
                        <td class="text-center">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" value="1"
                                           name="access[{{$section['id']}}][{{ \App\AccessType::EDIT }}]"
                                            {!! empty($access[$section['id']][\App\AccessType::EDIT]) ? '':' checked="checked"' !!}>
                                    <span class="form-check-sign">
                                        <span class="check"></span>
                                    </span>
                                </label>
                            </div>
                        </td>
                        <td class="text-center">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" value="1"
                                           name="access[{{$section['id']}}][{{ \App\AccessType::DELETE }}]"
                                            {!! empty($access[$section['id']][\App\AccessType::DELETE]) ? '':' checked="checked"' !!}>
                                    <span class="form-check-sign">
                                        <span class="check"></span>
                                    </span>
                                </label>
                            </div>
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <a href="{{ route('admin-groups.index') }}"
           class="btn btn-sm btn-outline-primary float-left">{{trans('admin.button.previous')}}</a>
        <button type="submit"
                class="btn btn-sm btn-outline-success float-right">{{trans('admin.button.save')}}</button>
    </div>
</div>
<script>
    $("form input").on("invalid", function (e) {
        $('a[href="#' + $(e.target).parents('div.tab-pane')[0].id + '"]').tab('show');
    });
</script>
