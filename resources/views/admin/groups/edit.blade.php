@extends('layouts.admin')
@section('content')
    <div class="card-header">
        <h4 class="card-title">{{trans('admin.create',['name'=>'группы пользователей'])}}</h4>
    </div>
    <div class="card-body">
        <ul class="nav nav-pills" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" href="#group" data-toggle="tab">
                    Основное
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#access" data-toggle="tab">
                    Доступ
                </a>
            </li>
        </ul>
        <form method="POST" role="form" action="{{ route('admin-groups.update',['id' => $element['id']]) }}"
              class="form-horizontal"
              enctype="multipart/form-data">
            @method('PATCH')
            @csrf
            @include ('admin.groups.form')
        </form>
    </div>
@endsection