@extends('layouts.admin')
@section('content')
    <div class="card-header">
        <div class="row">
            <ol class="breadcrumb bg-white py-0">
                <li class="breadcrumb-item">Контент</li>
                <li class="breadcrumb-item">
                    <a href="{{ route('admin-entity-types.index') }}">Типы сущностей</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route('admin-entity-types.show',['id' => $entity['entity_type']['id']]) }}">{{ $entity['entity_type']['name'] }}</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route('admin-entities.show',['id' => $entity['id']]) }}">{{ $entity['name'] }}</a>
                </li>
            </ol>
        </div>
        <h4 class="card-title">{{trans('admin.edit',['name'=>'свойства сущности'])}} "{{ $entity['name'] }}"</h4>
    </div>
    <div class="card-body">
        <form method="POST" role="form" action="{{ route('admin-properties.update',['id' => $id]) }}"
              class="form-horizontal"
              enctype="multipart/form-data">
            @method('PATCH')
            @csrf
            @include ('admin.properties.form')
        </form>
    </div>
@endsection