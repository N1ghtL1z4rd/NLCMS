<div class="row">
    <label class="col-md-3 col-form-label" for="name">Наименование</label>
    <div class="col-md-9">
        <div class="form-group">
            <input type="text" class="form-control" id="name" name="name" required="required" value="{{ $name ?? '' }}">
        </div>
    </div>
</div>
<div class="row">
    <label class="col-md-3 col-form-label" for="description">Описание</label>
    <div class="col-md-9">
        <div class="form-group">
            <textarea class="form-control" id="description" name="description"
                      rows="5">{!!  $description ?? ''  !!}</textarea>
        </div>
    </div>
</div>
<div class="row">
    <label class="col-md-3 col-form-label" for="code">Символьный код</label>
    <div class="col-md-9">
        <div class="form-group">
            <input type="text" class="form-control" id="code" name="code" required="required" value="{{ $code ?? '' }}">
        </div>
    </div>
</div>

<div class="row">
    <label class="col-md-3 col-form-label" for="setting_list_object_id">Объект настройки</label>
    <div class="col-md-9">
        <div class="form-group">
            <select class="form-control select2-material" id="setting_list_object_id" name="setting_list_object_id"
                    required="required">
                @foreach($settingListObjects as $settingListObject)
                    <option value="{{ $settingListObject->id }}"
                            {{ (isset($setting_list_object_id) && $setting_list_object_id === $settingListObject->id)
                            ? 'selected="selected"':'' }}>{{ $settingListObject->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>

<div class="row">
    <label class="col-md-3 col-form-label" for="data_type_id">Тип данных</label>
    <div class="col-md-9">
        <div class="form-group">
            <select class="form-control select2-material" id="data_type_id" name="data_type_id" required="required">
                @foreach($dataTypes as $dataType)
                    <option value="{{ $dataType->id }}"
                            {{ (isset($data_type_id) && $data_type_id === $dataType->id)
                            ? 'selected="selected"':'' }}>{{ $dataType->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>

<div class="row" id="optionsBlock" style="display: none">
    <label class="col-md-3 col-form-label" for="options">Список значений</label>
    <div class="col-md-9">
        <table id="property-values-table" class="table table-bordered">
            <thead>
            <tr>
                <th class="text-center">Значение</th>
                <th class="text-center">Код</th>
                <th class="text-center">Сортировка</th>
                <th class="text-center">Действия</th>
            </tr>
            </thead>
            <tbody>
            @if(isset($options))
                @foreach($options as $option)
                    <tr>
                        <td class="w-50 p-0">
                            <input type="hidden" name="options[ids][]" value="{{ $option['id'] }}">
                            <input type="text" class="w-100" name="options[name][]" value="{{ $option['name'] }}">
                        </td>
                        <td class="w-25 p-0">
                            <input type="text" class="w-100" name="options[code][]" value="{{ $option['code'] ?? '' }}">
                        </td>
                        <td class="p-0">
                            <input type="number" class="w-100" name="options[sort][]"
                                   value="{{ $option['sort'] ?? '500' }}">
                        </td>
                        <td class="td-actions text-center">
                            <div class="btn-group">
                                <button class="btn btn-danger btn-link" rel="tooltip" type="button"
                                        onclick="delPropertyValueRow(this)">
                                    <i class="fas fa-trash-alt" aria-hidden="true"></i>
                                </button>
                            </div>
                        </td>
                    </tr>
                @endforeach
            @endif
            <tr class="d-none">
                <td class="w-50 p-0">
                    <input type="text" class="w-100" name="options[name][]" value="">
                </td>
                <td class="w-25 p-0">
                    <input type="text" class="w-100" name="options[code][]" value="">
                </td>
                <td class="p-0">
                    <input type="number" class="w-100" name="options[sort][]" value="500">
                </td>
                <td class="td-actions text-center">
                    <div class="btn-group">
                        <button class="btn btn-danger btn-link" rel="tooltip" type="button"
                                onclick="delPropertyValueRow(this)">
                            <i class="fas fa-trash-alt" aria-hidden="true"></i>
                        </button>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
        <div class="text-center">
            <button type="button" class="btn btn-sm btn-outline-info"
                    onclick="addPropertyValueRow(this)">
                {{trans('admin.button.add')}}
            </button>
        </div>
    </div>
</div>

<div class="row" id="relationsBlock" style="display: none">
    <label class="col-md-3 col-form-label" for="relations">Сущности</label>
    <div class="col-md-9">
        <div class="form-group">
            <select class="form-control select2-material" id="relations" name="relations[]" multiple="multiple">
                @if(isset($entities))
                    @foreach($entities as $entity)
                        <option value="{{ $entity->id }}"
                                {{ isset($relationsByValue[$entity->id]) ? ' selected="selected"':'' }}>{{ $entity->name }}</option>
                    @endforeach
                @endif
            </select>
        </div>
    </div>
</div>

<div class="row" id="multipleBlock">
    <label class="col-md-3"></label>
    <div class="col-md-9">
        <div class="form-check">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" value="1"
                       name="multiple" {!! empty($multiple) ? '':'checked="checked"' !!}> Множественное
                <span class="form-check-sign">
                    <span class="check"></span>
                </span>
            </label>
        </div>
    </div>
</div>

<div class="row">
    <label class="col-md-3"></label>
    <div class="col-md-9">
        <div class="form-check">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" value="1"
                       name="active" {!! empty($active) ? '':'checked="checked"' !!}> Активность
                <span class="form-check-sign">
                      <span class="check"></span>
                    </span>
            </label>
        </div>
    </div>
</div>

<div class="row">
    <label class="col-md-3"></label>
    <div class="col-md-9">
        <div class="form-check">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" value="1"
                       name="system" {!! empty($system) ? '':'checked="checked"' !!}> Системный
                <span class="form-check-sign">
                      <span class="check"></span>
                </span>
            </label>
        </div>
    </div>
</div>

<div class="row">
    <label class="col-md-3 col-form-label" for="sort">Сортировка</label>
    <div class="col-md-9">
        <div class="form-group">
            <input type="number" class="form-control" id="sort" name="sort" required="required"
                   value="{{ $sort ?? '500' }}">
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <a href="{{ route('admin-setting-lists.index') }}" class="btn btn-sm btn-outline-primary float-left">{{trans('admin.button.previous')}}</a>
        <button type="submit" class="btn btn-sm btn-outline-success float-right">{{trans('admin.button.save')}}</button>
    </div>
</div>

<script>
    $(document).ready(function (e) {
        {{--
        Тут обработка для типов данных:
         - для типа "переключатель" (4),"Дата" (5),"Дата/Время" (6) - нет множественности
         - для типа "Список" (7) - множественность есть и надо показать поле для ввода значений списка
         - для типа "Привязка к элементам сущностей" (8) - множественность есть
           и надо показать поле для выбора сущностей, к элементам которой будет привязка
         --}}
        $('#data_type_id').change(function (e) {
            var dataType = Number($(this).val());
            switch (dataType) {
                case Number('<?=\App\DataType::CHECKBOX?>'):
                case Number('<?=\App\DataType::DATE?>'):
                case Number('<?=\App\DataType::DATETIME?>'):
                    $('#multipleBlock').hide();
                    $('#relationsBlock').hide();
                    $('#optionsBlock').hide();
                    break;
                case Number('<?=\App\DataType::SELECT?>'):
                    $('#relationsBlock').hide();
                    $('#optionsBlock').show();
                    $('#multipleBlock').show();
                    break;
                case Number('<?=\App\DataType::ENTITY?>'):
                    $('#optionsBlock').hide();
                    $('#relationsBlock').show();
                    $('#multipleBlock').show();
                    break;
                default:
                    $('#relationsBlock').hide();
                    $('#optionsBlock').hide();
                    $('#multipleBlock').show();
                    break;
            }
        });

        $('#data_type_id').trigger('change');
    });

    /** Добавление новой строки в таблицу для ввода значения списка
     * @param btn
     */
    function addPropertyValueRow(btn) {
        var tbl = $(btn).parents('div').prev('table#property-values-table');
        var tr = $(tbl).find('tbody > tr:last');
        console.log(tr);
        if ($(tr).hasClass('d-none')) {
            $(tr).removeClass('d-none');
        } else {
            var copy = $(tr).clone();
            $(copy).find('input').val('');
            $(copy).insertAfter(tr);
        }
    }

    /**
     * Удаление/очистка строки в таблице для ввода значений списка
     * @param btn
     */
    function delPropertyValueRow(btn) {
        var rows = $('table#property-values-table > tbody > tr');
        var trBtn = $(btn).parents('tr');
        // если строк несколько, то удаление
        if (rows.length > 1) {
            $(trBtn).remove();
        }
        // если одна строка, то очистка полей ввода данных
        else {
            $(trBtn).find('input').val('');
        }
    }
</script>