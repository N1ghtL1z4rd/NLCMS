@extends('layouts.admin')
@section('content')
    <div class="card-header">
        <h4 class="card-title">{{trans('admin.create',['name'=>'настраиваемых параметров'])}}</h4>
    </div>
    <div class="card-body">

        <form method="POST" role="form" action="{{ route('admin-setting-lists.store') }}" class="form-horizontal"
              enctype="multipart/form-data">
            @csrf
            @include ('admin.setting-lists.form')
        </form>
    </div>
@endsection