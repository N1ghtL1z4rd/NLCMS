@extends('layouts.admin')
@section('content')
    <div class="card-header">
        <h4 class="card-title">{{trans('admin.edit',['name'=>'настраиваемых параметров'])}}</h4>
    </div>
    <div class="card-body">
        <form method="POST" role="form" action="{{ route('admin-setting-lists.update',['id' => $id]) }}"
              class="form-horizontal"
              enctype="multipart/form-data">
            @method('PATCH')
            @csrf
            @include ('admin.setting-lists.form')
        </form>
    </div>
@endsection