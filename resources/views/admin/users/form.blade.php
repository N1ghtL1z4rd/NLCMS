<div class="tab-content pt-4">
    <div class="tab-pane active" id="user">

        <div class="row form-group">
            <label class="col-md-3 col-form-label" for="name">Имя/Никнэйм</label>
            <div class="col-md-9">
                <div class="form-group">
                    <input type="text" class="form-control" id="name" name="name"
                           required="required"
                           value="{{ $user->name ?? '' }}">
                </div>
            </div>
        </div>

        <div class="row form-group">
            <label class="col-md-3 col-form-label" for="email">E-mail</label>
            <div class="col-md-9">
                <div class="form-group">
                    <input type="text" class="form-control" id="email" name="email"
                           required="required"
                           value="{{ $user->email ?? '' }}">
                </div>
            </div>
        </div>

        <div class="row form-group">
            <label class="col-md-3 col-form-label" for="password">Пароль</label>
            <div class="col-md-9">
                <div class="form-group">
                    <input type="password" class="form-control" id="password" name="password"
                            {{ Route::currentRouteName() === 'users.create' ? 'required="required"':''}}>
                </div>
            </div>
        </div>

        <div class="row form-group">
            <label class="col-md-3 col-form-label" for="password_confirmation">Подтверждение
                пароля</label>
            <div class="col-md-9">
                <div class="form-group">
                    <input type="password" class="form-control" id="password_confirmation"
                           name="password_confirmation"
                            {{ Route::currentRouteName() === 'users.create' ? 'required="required"':''}}>
                </div>
            </div>
        </div>

        <div class="row form-group">
            <label class="col-md-3 pt-3 text-right">
                Активность
            </label>
            <div class="col-md-9">
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" value="1"
                               name="active" {!! empty($user->active) ? '':'checked="checked"' !!}>
                        <span class="form-check-sign">
                            <span class="check"></span>
                         </span>
                    </label>
                </div>
            </div>
        </div>

        <div class="row form-group">
            <label class="col-md-3 pt-3 text-right">
                Системный
            </label>
            <div class="col-md-9">
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" value="1"
                               name="system" {!! empty($user->system) ? '':'checked="checked"' !!}>
                        <span class="form-check-sign">
                            <span class="check"></span>
                        </span>
                    </label>
                </div>
            </div>
        </div>

        <div class="row form-group">
            <label class="col-md-3 col-form-label" for="sort">Сортировка</label>
            <div class="col-md-9">
                <div class="form-group">
                    <input type="number" class="form-control" id="sort" name="sort"
                           required="required"
                           value="{{ $user->sort ?? '500' }}">
                </div>
            </div>
        </div>
    </div>

    <div class="tab-pane" id="groups">
        <table class="table">
            <thead>
            <tr>
                <th class="text-center">ID</th>
                <th>Название</th>
                <th>Описание</th>
                <th class="text-center">Включить</th>
            </tr>
            </thead>
            <tbody>
            @foreach($groups as $group)
                <tr>
                    <td class="text-center">{{$group['id']}}</td>
                    <td>{{$group['name']}}</td>
                    <td>{{$group['description']}}</td>
                    <td class="text-center">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="checkbox" value="{{$group['id']}}"
                                       name="groups[]" {!! !isset($userGroups[$group['id']]) ? '':'checked="checked"' !!}>
                                <span class="form-check-sign">
                                    <span class="check"></span>
                                </span>
                            </label>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    @if($settings)
        <div class="tab-pane" id="settings">
            @foreach($settings as $item)
                @php
                    $item['required'] = false;
                @endphp
                <div class="row form-group">
                    @include('formHelpers.admin.properties-form')
                </div>
            @endforeach
        </div>
    @endif

    <div class="row pt-4">
        <div class="col-md-12">
            <a href="{{ route('admin-users.index') }}"
               class="btn btn-sm btn-outline-primary float-left">{{trans('admin.button.previous')}}</a>
            <button type="submit"
                    class="btn btn-sm btn-outline-success float-right">{{trans('admin.button.save')}}</button>
        </div>
    </div>
</div>
<script>
    $("form input").on("invalid", function (e) {
        $('a[href="#' + $(e.target).parents('div.tab-pane')[0].id + '"]').tab('show');
    });
</script>