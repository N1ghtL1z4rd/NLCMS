@extends('layouts.admin')
@section('content')
    <div class="card-header">
        <h4 class="card-title">{{trans('admin.edit',['name'=>'пользователя'])}}</h4>
    </div>
    <div class="card-body">
        <div class="row px-3">
            <ul class="nav nav-pills col" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" href="#user" data-toggle="tab">
                        Основные
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#groups" data-toggle="tab">
                        Группы
                    </a>
                </li>
                @if($settings)
                    <li class="nav-item">
                        <a class="nav-link" href="#settings" data-toggle="tab">
                            Расширенные
                        </a>
                    </li>
                @endif
            </ul>
            <div class="col">
                <div class="nav-item dropdown float-right">
                    <a class="btn btn-outline-warning btn-sm" href="" data-toggle="dropdown">Действия</a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="">
                        @if(($access['delete'] && $user->id !== 1) || Auth::id() === 1)
                            <a class="dropdown-item" href="javascript:void(0);"
                               onclick="event.preventDefault();document.getElementById('delete-form').submit();"
                               title="Удалить">
                                Удалить
                            </a>
                            <form id="delete-form" action="{{ route('admin-users.destroy',['id'=>$user->id]) }}" method="POST" style="display: none;">
                                @method('DELETE')
                                @csrf
                            </form>
                        @endif
                        @if($user->id !== 1)
                            <a class="dropdown-item" href="javascript:void(0);"
                               onclick="event.preventDefault();document.getElementById('login-form').submit();"
                               title="Авторизоваться">
                                Авторизоваться
                            </a>
                            <form id="login-form" action="{{ route('admin-users.login',['id'=>$user->id]) }}"
                                  method="POST" style="display: none;">
                                @csrf
                            </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <form method="POST" role="form" action="{{ route('admin-users.update',['id' => $user->id]) }}"
              accept-charset="UTF-8"
              class="form-horizontal" enctype="multipart/form-data">
            @method('PATCH')
            @csrf
            @include ('admin.users.form')
        </form>
    </div>
@endsection

