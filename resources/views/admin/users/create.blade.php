@extends('layouts.admin')
@section('content')
    <div class="card-header">
        <h4 class="card-title">{{trans('admin.create',['name'=>'пользователя'])}}</h4>
    </div>
    <div class="card-body">
        <ul class="nav nav-pills" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" href="#user" data-toggle="tab">
                    Основные
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#groups" data-toggle="tab">
                    Группы
                </a>
            </li>
            @if($settings)
                <li class="nav-item">
                    <a class="nav-link" href="#settings" data-toggle="tab">
                        Расширенные
                    </a>
                </li>
            @endif
        </ul>
        <form method="POST" role="form" action="{{ route('admin-users.store') }}" accept-charset="UTF-8"
              class="form-horizontal" enctype="multipart/form-data">
            @csrf
            @include('admin.users.form')
        </form>
    </div>
@endsection
