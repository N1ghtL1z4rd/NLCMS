@extends('layouts.admin')
@section('content')
    <div class="card-header">
        <h4 class="card-title float-left">{{trans('admin.commands')}}</h4>
    </div>

    {{--@foreach($items as $item)--}}
    {{--@dump($item->getName())--}}
    {{--@dump($item->getDescription())--}}
    {{--@dump(array_keys($item->getDefinition()->getOptions()))--}}
    {{--@endforeach--}}
    {{--@dd($items)--}}
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-striped table-bordered dataTable-table">
                <thead>
                <tr>
                    <th class="">#</th>
                    <th class="">Команда</th>
                    <th class="text-center">Описание</th>
                    <th class="text-center">Опции</th>
                </tr>
                </thead>
                <tbody>
                @foreach($items as $item)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $item->getName() }}</td>
                        <td>{{ $item->getDescription() }}</td>
                        <td>{!!implode('<br>',array_keys($item->getDefinition()->getOptions()))!!}</td>
                        {{--<td class="td-actions text-center">--}}
                        {{--<div class="btn-group">--}}
                        {{--<a href="{{ route('admin-tables.show',['table'=>$item]) }}"--}}
                        {{--class="btn btn-primary btn-link" rel="tooltip">--}}
                        {{--<i class="far fa-eye"></i>--}}
                        {{--</a>--}}
                        {{--</div>--}}
                        {{--</td>--}}
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
