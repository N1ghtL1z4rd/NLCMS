@extends('layouts.admin')
@section('content')
    <div class="card-header">
        <h4 class="card-title float-left">Таблица {{$table}}</h4>
    </div>
    <div class="card-body">
        <table class="table table-striped table-bordered">
            @if(!empty($columns))
                <thead>
                <tr>
                    @foreach($columns as $column)
                        <th class="">{{$column}}</th>
                    @endforeach
                </tr>
                </thead>
                <tbody>
                @if(!empty($items))
                    @foreach($items as $item)
                        <tr>
                            @foreach($item as $cell)
                                <td>{{ $cell }}</td>
                            @endforeach
                        </tr>
                    @endforeach
                @endif
                </tbody>
            @endif
        </table>
        <div class="row mt-2">
            <div class="col-md-12">
                <a href="{{ route('admin-tables.index') }}" class="btn btn-sm btn-outline-primary float-left">
                    {{trans('admin.button.previous')}}
                </a>
                <div class="float-right">
                    {{ $items->render() }}
                </div>
            </div>
        </div>
    </div>
@endsection