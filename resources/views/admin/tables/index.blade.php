@extends('layouts.admin')
@section('content')
    <div class="card-header">
        <h4 class="card-title float-left">{{trans('admin.tables')}}</h4>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-striped table-bordered dataTable-table">
                <thead>
                <tr>
                    <th class="">#</th>
                    <th class="">Наименование</th>
                    <th class="text-center">Действия</th>
                </tr>
                </thead>
                <tbody>
                @foreach($items as $item)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>
                            <a href="{{ route('admin-tables.show',['table'=>$item]) }}">
                                {{ $item }}
                            </a>
                        </td>
                        <td class="td-actions text-center">
                            <div class="btn-group">
                                <a href="{{ route('admin-tables.show',['table'=>$item]) }}"
                                   class="btn btn-primary btn-link" rel="tooltip">
                                    <i class="far fa-eye"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
