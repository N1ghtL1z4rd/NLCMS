@extends('layouts.admin')
@section('content')
    <div class="card-header">
        <h4 class="card-title">{{trans('admin.edit',['name'=>'раздела'])}}</h4>
    </div>
    <div class="card-body">
        <form method="POST" role="form" action="{{ route('admin-sections.update',['id' => $id]) }}"
              class="form-horizontal"
              enctype="multipart/form-data">
            @method('PATCH')
            @csrf
            @include ('admin.sections.form')
        </form>
    </div>
@endsection