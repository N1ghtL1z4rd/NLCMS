<div class="row">
    <label class="col-md-3 col-form-label" for="name">Наименование</label>
    <div class="col-md-9">
        <div class="form-group">
            <input type="text" class="form-control" id="name" name="name" required="required" value="{{ $name ?? '' }}">
        </div>
    </div>
</div>
<div class="row">
    <label class="col-md-3 col-form-label" for="description">Описание</label>
    <div class="col-md-9">
        <div class="form-group">
            <textarea class="form-control" id="description" name="description"
                      rows="5">{!!  $description ?? ''  !!}</textarea>
        </div>
    </div>
</div>
<div class="row">
    <label class="col-md-3 col-form-label" for="url">URL</label>
    <div class="col-md-9">
        <div class="form-group">
            <input type="text" class="form-control" id="url" name="url" placeholder="/site/main-page/about"
                   required="required" value="{{ $url ?? '' }}">
        </div>
    </div>
</div>

<div class="row">
    <label class="col-md-3"></label>
    <div class="col-md-9">
        <div class="form-check">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" value="1"
                       name="active" {!! empty($active) ? '':'checked="checked"' !!}> Активность
                <span class="form-check-sign">
                      <span class="check"></span>
                    </span>
            </label>
        </div>
    </div>
</div>

<div class="row">
    <label class="col-md-3"></label>
    <div class="col-md-9">
        <div class="form-check">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" value="1"
                       name="system" {!! empty($system) ? '':'checked="checked"' !!}> Системный
                <span class="form-check-sign">
                      <span class="check"></span>
                </span>
            </label>
        </div>
    </div>
</div>

<div class="row">
    <label class="col-md-3 col-form-label" for="sort">Сортировка</label>
    <div class="col-md-9">
        <div class="form-group">
            <input type="number" class="form-control" id="sort" name="sort" required="required" value="{{ $sort ?? '500' }}">
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <a href="{{ route('admin-sections.index') }}" class="btn btn-sm btn-outline-primary float-left">{{trans('admin.button.previous')}}</a>
        <button type="submit" class="btn btn-sm btn-outline-success float-right">{{trans('admin.button.save')}}</button>
    </div>
</div>
