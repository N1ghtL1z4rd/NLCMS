@extends('layouts.admin')
@section('content')
    <div class="card-header">
        <h4 class="card-title">{{trans('admin.create',['name'=>'раздела'])}}</h4>
    </div>
    <div class="card-body">
        <form method="POST" role="form" action="{{ route('admin-sections.store') }}" class="form-horizontal"
              enctype="multipart/form-data">
            @csrf
            @include ('admin.sections.form')
        </form>
    </div>
@endsection
