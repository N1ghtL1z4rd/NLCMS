@extends('layouts.admin')
@section('content')
    <div class="card-header">
        <h4 class="card-title float-left">{{trans('admin.sections')}}</h4>
        @if($access['edit'])
            <a href="{{ route('admin-sections.create') }}" class="btn btn-outline-info btn-sm float-right">
                {{trans('admin.button.add')}}
            </a>
        @endif
    </div>
    <div class="card-body">
        <form method="POST" action="{{ route('admin-sections.destroy') }}" id="chActionsForm">
            @method('DELETE')
            @csrf
            <table class="table table-striped table-bordered dataTable-table-ajax"></table>
        </form>
        <hr>
        @if($access['edit'])
            <div class="dropdown d-none" id="itemsListActions">
                <button class="btn btn-sm btn-outline-warning dropdown-toggle" data-toggle="dropdown">
                    Действия
                </button>
                <div class="dropdown-menu px-1">
                    @if($access['delete'])
                        <button class="dropdown-item bg-hover-danger w-100 m-0" type="submit" form="chActionsForm">
                            Удалить
                        </button>
                    @endif
                </div>
            </div>
        @endif
    </div>
@endsection