<?php $__env->startSection('title', trans('errors.404')); ?>

<?php $__env->startSection('message', $exception->getMessage()); ?>

<?php echo $__env->make('errors::layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>