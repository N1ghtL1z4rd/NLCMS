<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{-- CSRF Token --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('favicon.ico') }}">
    <title>{{ trans('auth.authorization') }}</title>
    {{-- CSS Files --}}
    <link href="{{ asset('libs/material-dashboard/assets/css/material-dashboard.min.css') }}" rel="stylesheet"/>
    {{--Fonts --}}
    <link href="{{ asset('libs/fontawesome/css/all.min.css') }}" rel="stylesheet"/>
</head>
<body class="">
<div class="page-header d-flex align-items-center h-100"
     style="background-image: url('{{asset('img/login-background.png')}}');background-size: cover;">
    @yield('content')
</div>
</body>
</html>