<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{-- CSRF Token --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('favicon.ico') }}">
    <title>Административная панель</title>

    {{-- CSS Files --}}
    <link href="{{ asset('libs/material-dashboard/assets/css/material-dashboard.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('libs/material-dashboard/assets/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet"/>
    {{-- Core JS Files --}}
    <script src="{{ asset('libs/material-dashboard/assets/js/core/jquery.min.js') }}"></script>
    <script src="{{ asset('libs/material-dashboard/assets/js/core/popper.min.js') }}"></script>
    <script src="{{ asset('libs/material-dashboard/assets/js/core/bootstrap-material-design.min.js') }}"></script>
    <script src="{{ asset('libs/material-dashboard/assets/js/plugins/perfect-scrollbar.jquery.min.js') }}"></script>
    {{-- Chartist JS --}}
    {{--<script src="{{ asset('libs/material-dashboard/assets/js/plugins/chartist.min.js') }}"></script>--}}
    {{--  Notifications Plugin --}}
    <script src="{{ asset('libs/material-dashboard/assets/js/plugins/bootstrap-notify.js') }}"></script>
    {{-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc --}}
    <script src="{{ asset('libs/material-dashboard/assets/js/material-dashboard.min.js?v=2.1.0') }}" defer></script>
    {{-- Fonts --}}
    <link href="{{ asset('libs/fontawesome/css/all.min.css') }}" rel="stylesheet"/>
    {{-- Select2 --}}
    <link href="{{ asset('libs/select2/select2-materialize.min.css') }}" rel="stylesheet"/>
    <script src="{{ asset('libs/select2/select2.min.js') }}"></script>
    {{-- Datetimepicker--}}
    <script src="{{ asset('libs/moment/moment-with-locales.min.js') }}"></script>
    <script src="{{ asset('libs/material-dashboard/assets/js/plugins/bootstrap-datetimepicker.min.js') }}"></script>
    {{-- Admin template files --}}
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet"/>
    <script src="{{ asset('js/admin.js') }}"></script>
    {{-- Datatables --}}
    {{--<link href="{{ asset('libs/datatables/css/jquery.dataTables.min.css') }}" rel="stylesheet"/>--}}
    <link href="{{ asset('libs/datatables/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet"/>
    <script src="{{ asset('libs/datatables/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('libs/datatables/js/dataTables.bootstrap4.min.js') }}"></script>

</head>

<body>
<div class="wrapper">
    <div class="sidebar" data-color="purple" data-background-color="black">
        <div class="sidebar-wrapper ps-container ps-theme-default ps-active-y">
            <div class="logo text-center text-white">
                <div class="col-md-12 display-4">
                    <span>NLCMS</span>
                </div>
            </div>
            <div id="accordionMenu" role="tablist" aria-multiselectable="true">
                @include('layouts.admin.sidebar',
                        [
                            'menu' => Cms\Helpers::menu('ADMIN_MAIN'),
                            'icon' => 'fas fa-tachometer-alt'
                        ])
                @include('layouts.admin.sidebar',
                        [
                            'menu' => Cms\Helpers::menu('ADMIN_CONTENT'),
                            'icon' => 'fas fa-book media-left media-middle sidebar-nav-icon',
                            'entityTypes' => \App\EntityType::with('entities')->orderBy('sort')->get()
                        ])
                @include('layouts.admin.sidebar',
                        [
                            'menu' => Cms\Helpers::menu('ADMIN_SETTINGS'),
                            'icon' => 'fas fa-cogs media-left media-middle sidebar-nav-icon'
                        ])
            </div>
        </div>
        <a class="nav-link text-center position-absolute fixed-bottom text-white border-top" target="_blank"
           href="https://gitlab.com/N1ghtL1z4rd/NLCMS">
            NLCMS
        </a>
    </div>
    <div class="admin-main-panel">
        <nav class="navbar navbar-expand-lg bg-white rounded-0" id="navigation-example">
            <div class="container-fluid">
                <div class="navbar-wrapper">
                    <a class="navbar-brand" href="javascript:void(0)"></a>
                </div>
                <ul class="navbar-nav">
                    <li class="nav-item dropdown show">
                        <a class="nav-link" href="javascript:void(0)" id="navbarDropdownUserLink" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="true">
                            {{ Auth::user()->name }}
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownUserLink">
                            <a class="dropdown-item" href="/">На сайт</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();document.getElementById('logout-form').submit();"
                               title="Выход">
                                Выход
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="content">
            <div class="container-fluid">
                <div class="col-md-12">
                    <div class="mt-3">
                        @if(!empty(session()->get('flash_message')))
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <i class="fas fa-times" aria-hidden="true"></i>
                                </button>
                                <span>{{ session()->get('flash_message') }}</span>
                            </div>
                        @endif
                            @if(!empty(session()->get('warning')))
                                <div class="alert alert-warning">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <i class="fas fa-times" aria-hidden="true"></i>
                                    </button>
                                    <span>{{ session()->get('warning') }}</span>
                                </div>
                            @endif
                        @if($errors->any())
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <i class="fas fa-times" aria-hidden="true"></i>
                                </button>
                                @foreach ($errors->all() as $error)
                                    <span>{{ $error }}</span>
                                @endforeach
                            </div>
                        @endif
                    </div>
                    <div class="card">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@php
    $url = null;
    $routeNameArr = explode('.',Route::current()->getName());
    if(count($routeNameArr) === 1) {
        $url = Request::getPathInfo();
    } else {
        if(in_array('admin-entity-types',$routeNameArr)) {
            if(in_array('show',$routeNameArr)) {
                $url = Request::getPathInfo();
            } else {
                $url = Route::current()->action['prefix'];
            }
        } elseif(in_array('admin-entities',$routeNameArr)) {
            if(in_array('show',$routeNameArr)) {
                $url = Request::getPathInfo();
            } elseif(in_array('edit',$routeNameArr)) {
                $urlFullArr = explode('/',Request::getPathInfo());
                array_pop($urlFullArr);
                $url = implode('/',$urlFullArr);
            } else {
                $url = Route::current()->action['prefix'];
            }
        } elseif(in_array('admin-properties',$routeNameArr)) {
            if(!empty($entity_id))
                $url = route('admin-entities.show',['id' => $entity_id]);
        } elseif(in_array('admin-entity-elements',$routeNameArr)) {
            if(!empty($entity->id))
                $url = route('admin-entities.show',['id' => $entity->id]);
        } else {
            $url = Route::current()->action['prefix'];
        }
    }
@endphp
<script>
    $(document).ready(function () {
        /**
         * Установка активного пункта меню
         * @type {string}
         */
        setActiveMenuItem('{{ $url }}');

        $(".select2-material").select2({
            width: '100%'
        });

        moment.locale('ru');
        $('.datepicker').datetimepicker({
            format: 'DD-MM-YYYY',
            locale: 'ru',
            icons: {
                time: 'far fa-arrow-alt-circle-down',
                date: 'far fa-arrow-alt-circle-up',
                up: 'fas fa-caret-up',
                down: 'fas fa-caret-down',
                previous: 'fas fa-caret-left',
                next: 'fas fa-caret-right',
                today: 'fas fa-crosshairs',
                clear: 'fas fa-trash-alt',
                close: 'fas fa-times'
            }
        });

        $('.datetimepicker').datetimepicker({
            format: 'DD-MM-YYYY HH:mm',
            locale: 'ru',
            icons: {
                time: 'far fa-arrow-alt-circle-down',
                date: 'far fa-arrow-alt-circle-up',
                up: 'fas fa-caret-up',
                down: 'fas fa-caret-down',
                previous: 'fas fa-caret-left',
                next: 'fas fa-caret-right',
                today: 'fas fa-crosshairs',
                clear: 'fas fa-trash-alt',
                close: 'fas fa-times'
            }
        });

        dtTable = $('.dataTable-table').dataTable({
            "language": {
                url: "{{ asset('libs/datatables/lang/ru_RU.json') }}"
            },
            "lengthMenu": [[10, 20, 50, 100, -1], [10, 20, 50, 100, "Все"]],
            "pageLength": 20,
            columnDefs: [
                {
                    targets: [$('th.dt-sorting-none').index()],
                    orderable: false,
                },
            ]
        });
        if ($('th.dt-sorting-default-asc').index() >= 0)
            dtTable.api().order([$('th.dt-sorting-default-asc').index(), "asc"]).draw();

        if ($('th.dt-sorting-default-desc').index() >= 0)
            dtTable.api().order([$('th.dt-sorting-default-desc').index(), "desc"]).draw();
    });
    @isset($dtAjaxSettings)
    /**
     * Таблица с ajax загрузкой данных
     * @type {jQuery}
     */
    dtTableAjax = $('.dataTable-table-ajax').dataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "{{ $dtAjaxSettings['url'] ?? '' }}",
            "type": "POST",
            "headers": {
                'X-CSRF-TOKEN': '{{csrf_token()}}'
            }
        },
        "language": {
            "url": "{{ asset('libs/datatables/lang/ru_RU.json') }}"
        },
        "lengthMenu": [[10, 20, 50, 100, 500, -1], [10, 20, 50, 100, 500, "Все"]],
        "pageLength": 20,
        "order": @json($dtAjaxSettings['order']),
        "columns": @json($dtAjaxSettings['columns']),
    });

    // $('#myInput').on('keyup', function () {
    //     dtTableAjax.api().search(this.value).draw();
    // });
    @endisset
</script>
</body>
</html>