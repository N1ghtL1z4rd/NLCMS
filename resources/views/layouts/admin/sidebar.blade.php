<div class="card bg-transparent rounded-0 m-0">
    <div class="card-header p-0" id="">
        <h5 class="my-2 nav">
            <p class="nav-link w-100 text-white" data-toggle="collapse"
               data-target="#sidebar-panel-{{ $menu['menu']['id'] }}"
               aria-expanded="true">
                <i class="{{ $icon }} text-white"></i>
                {{ $menu['menu']['name'] }}
                <b class="caret mr-3"></b>
            </p>
        </h5>
    </div>

    <div id="sidebar-panel-{{ $menu['menu']['id'] }}" aria-labelledby="" data-parent="#accordion" class="collapse">
        <div class="card-body p-0">
            <ul class="nav pl-3 py-0 mt-0 mb-2">
                @foreach($menu['items'] as $item)
                    @if(!empty($item['sub']))
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="collapse" href="#{{$item['id']}}" aria-expanded="true">
                                <i class="{{$item['icon']}}"></i>
                                <p>{{$item['name']}}
                                    <b class="caret"></b>
                                </p>
                            </a>
                            <div class="collapse" id="{{$item['id']}}" style="">
                                <ul class="nav pl-3">
                                    @foreach($item['sub'] as $subItem)
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{$subItem['section']['url']}}">
                                                <span class="sidebar-normal">{{$subItem['name']}}</span>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </li>
                    @else
                        <li class="nav-item">
                            <a class="nav-link" href="{{ $item['section']['url'] }}">
                                <i class="{{$item['icon']}}"></i>
                                <p>{{$item['name']}}</p>
                            </a>
                        </li>
                    @endif
                @endforeach

                @if(!empty($entityTypes) && $entityTypes->count() > 0)
                    @foreach($entityTypes->toArray() as $entityType)
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="collapse" aria-expanded="true"
                               href="#admin-sidebar-entityType-{{$entityType['id']}}">
                                <i class="fas fa-folder"></i>
                                <p>
                                    {{$entityType['name']}}
                                    <b class="caret"></b>
                                </p>
                            </a>

                            <div class="collapse"
                                 id="admin-sidebar-entityType-{{$entityType['id']}}" style="">
                                <ul class="nav pl-3">
                                    <li class="nav-item">
                                        <a class="nav-link"
                                           href="{{ route('admin-entity-types.show',['id' => $entityType['id']]) }}">
                                            <i class="fas fa-cog media-left media-middle"></i>
                                            Управление сущностями
                                        </a>
                                    </li>
                                    @if(!empty($entityType['entities']) && count($entityType['entities']) > 0)
                                        @foreach($entityType['entities'] as $entity)
                                            <li class="nav-item">
                                                <a class="nav-link"
                                                   href="{{ route('admin-entities.show',['id' => $entity['id']]) }}">
                                                    <i class="fas fa-clone"></i>
                                                    <p>{{$entity['name']}}</p>
                                                </a>
                                            </li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                        </li>
                    @endforeach
                @endif
            </ul>
        </div>
    </div>
</div>