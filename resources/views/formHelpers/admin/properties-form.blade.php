@switch($item['data_type_id'])
    @case(\App\DataType::STRING)
    <label class="col-md-3 col-form-label" for="{{ $item['id'] }}">{{ $item['name'] }}</label>
    <div class="col-md-9">
        {{-- Если множественное --}}
        @if($item['multiple'])
            {{--Если есть данные по свойству, то вывести в цикле --}}
            @if(!empty($propertiesData[$item['id']]))
                @foreach($propertiesData[$item['id']] as $key => $val)
                    <div class="form-group">
                        <input type="text" class="form-control" id="{{ $item['id'] }}"
                               {{ $item['required'] ? ' required="required"' : ''}}
                               name="properties[{{ $item['id'] }}][]"
                               value="{{ $val['value'] ?? '' }}">
                    </div>
                @endforeach
                {{--Если нет данные, то пустой input--}}
            @else
                <div class="form-group">
                    <input type="text" class="form-control" id="{{ $item['id'] }}"
                           {{ $item['required'] ? ' required="required"' : ''}}
                           name="properties[{{ $item['id'] }}][]">
                </div>
            @endif
            <div class="text-center">
                <button type="button" class="btn btn-sm btn-outline-info"
                        data-property_id="{{ $item['id'] }}"
                        data-data_type_id="{{$item['data_type_id']}}"
                        onclick="addPropertyValueRow(this)">
                    {{trans('admin.button.add')}}
                </button>
            </div>
            {{--Если не множественное, то вывести первый элемент из массива--}}
        @else
            <div class="form-group">
                <input type="text" class="form-control" id="{{ $item['id'] }}"
                       {{ $item['required'] ? ' required="required"' : ''}}
                       name="properties[{{ $item['id'] }}]"
                       value="{{ $propertiesData[$item['id']][0]['value'] ?? '' }}">
            </div>
        @endif
    </div>
    @break

    @case(\App\DataType::INTEGER)
    <label class="col-md-3 col-form-label" for="{{ $item['id'] }}">{{ $item['name'] }}</label>
    <div class="col-md-9">
        {{-- Если множественное --}}
        @if($item['multiple'])
            {{--Если есть данные по свойству, то вывести в цикле --}}
            @if(!empty($propertiesData[$item['id']]))
                @foreach($propertiesData[$item['id']] as $key => $val)
                    <div class="form-group">
                        <input type="number" class="form-control" id="{{ $item['id'] }}"
                               {{ $item['required'] ? ' required="required"' : ''}}
                               name="properties[{{ $item['id'] }}][]"
                               value="{{ $val['value'] ?? '' }}">
                    </div>
                @endforeach
                {{--Если нет данные, то пустой input--}}
            @else
                <div class="form-group">
                    <input type="number" class="form-control" id="{{ $item['id'] }}"
                           {{ $item['required'] ? ' required="required"' : ''}}
                           name="properties[{{ $item['id'] }}][]">
                </div>
            @endif
            <div class="text-center">
                <button type="button" class="btn btn-sm btn-outline-info"
                        data-property_id="{{ $item['id'] }}"
                        data-data_type_id="{{$item['data_type_id']}}"
                        onclick="addPropertyValueRow(this)">
                    {{trans('admin.button.add')}}
                </button>
            </div>
            {{--Если не множественное, то вывести первый элемент из массива--}}
        @else
            <div class="form-group">
                <input type="number" class="form-control" id="{{ $item['id'] }}"
                       {{ $item['required'] ? ' required="required"' : ''}}
                       name="properties[{{ $item['id'] }}]"
                       value="{{ $propertiesData[$item['id']][0]['value'] ?? '' }}">
            </div>
        @endif
    </div>
    @break

    @case(\App\DataType::TEXT)
    <label class="col-md-3 col-form-label" for="{{ $item['id'] }}">{{ $item['name'] }}</label>
    <div class="col-md-9">
        {{-- Если множественное --}}
        @if($item['multiple'])
            {{--Если есть данные по свойству, то вывести в цикле --}}
            @if(!empty($propertiesData[$item['id']]))
                @foreach($propertiesData[$item['id']] as $key => $val)
                    <div class="form-group">
                        <textarea class="form-control" id="{{ $item['id'] }}" rows="5"
                                  {{ $item['required'] ? ' required="required"' : ''}}
                                  name="properties[{{ $item['id'] }}][]">{{ $val['value'] ?? '' }}</textarea>
                    </div>
                @endforeach
                {{--Если нет данные, то пустой input--}}
            @else
                <div class="form-group">
                    <textarea class="form-control" id="{{ $item['id'] }}" rows="5"
                              {{ $item['required'] ? ' required="required"' : ''}}
                              name="properties[{{ $item['id'] }}][]"></textarea>
                </div>
            @endif
            <div class="text-center">
                <button type="button" class="btn btn-sm btn-outline-info"
                        data-property_id="{{ $item['id'] }}"
                        data-data_type_id="{{$item['data_type_id']}}"
                        onclick="addPropertyValueRow(this)">
                    {{trans('admin.button.add')}}
                </button>
            </div>
            {{--Если не множественное, то вывести первый элемент из массива--}}
        @else
            <div class="form-group">
                <textarea class="form-control" id="{{ $item['id'] }}" rows="5"
                          {{ $item['required'] ? ' required="required"' : ''}}
                          name="properties[{{ $item['id'] }}]">{{ $propertiesData[$item['id']][0]['value'] ?? '' }}</textarea>
            </div>
        @endif
    </div>
    @break

    @case(\App\DataType::CHECKBOX)
    <label class="col-md-3"></label>
    <div class="col-md-9">
        <div class="form-check">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" value="1"
                       {{ $item['required'] ? ' required="required"' : ''}}
                       {{ !empty($propertiesData[$item['id']][0]['value']) ? ' checked="checked"' : ''}}
                       name="properties[{{ $item['id'] }}]">
                {{ $item['name'] }}
                <span class="form-check-sign">
                    <span class="check"></span>
                </span>
            </label>
        </div>
    </div>
    @break

    @case(\App\DataType::DATE)
    <label class="col-md-3 col-form-label" for="{{ $item['id'] }}">{{ $item['name'] }}</label>
    <div class="col-md-9">
        <div class="form-group">
            <input type="text" class="form-control datepicker" id="{{ $item['id'] }}"
                   {{ $item['required'] ? ' required="required"' : ''}}
                   name="properties[{{ $item['id'] }}]"
                   value="{{ $propertiesData[$item['id']][0]['value'] ?? '' }}">
        </div>
    </div>
    @break

    @case(\App\DataType::DATETIME)
    <label class="col-md-3 col-form-label" for="{{ $item['id'] }}">{{ $item['name'] }}</label>
    <div class="col-md-9">
        <div class="form-group">
            <input type="text" class="form-control datetimepicker" id="{{ $item['id'] }}"
                   {{ $item['required'] ? ' required="required"' : ''}}
                   name="properties[{{ $item['id'] }}]"
                   value="{{ $propertiesData[$item['id']][0]['value'] ?? '' }}">
        </div>
    </div>
    @break

    @case(\App\DataType::SELECT)
    <label class="col-md-3 col-form-label" for="{{ $item['id'] }}">{{ $item['name'] }}</label>
    <div class="col-md-9">
        <div class="form-group">
            @php
                $setOptions = [];
                if(!empty($propertiesData[$item['id']]))
                    $setOptions = collect($propertiesData[$item['id']])->keyBy('value')->toArray();
            @endphp
            <select class="form-control select2-material" id="{{ $item['id'] }}"
                    {{ $item['multiple'] ? ' multiple="multiple"' : ''}}
                    {{ $item['required'] ? ' required="required"' : ''}}
                    name="properties[{{ $item['id'] }}]{{ $item['multiple'] ? '[]' : ''}}">
                @if(empty($item['required']) && empty($item['multiple']))
                    <option></option>
                @endif
                @if(isset($item['options']))
                    @foreach($item['options'] as $option)
                        <option value="{{ $option['id'] }}"
                                {{ isset($setOptions[$option['id']]) ? ' selected="selected"' : ''}}
                        >{{ $option['name'] }}</option>
                    @endforeach
                @endif
            </select>
        </div>
    </div>
    @break

    @case(\App\DataType::ENTITY)
    <label class="col-md-3 col-form-label" for="{{ $item['id'] }}">{{ $item['name'] }}</label>
    <div class="col-md-9">
        <div class="form-group">
            @php
                $setEntities = [];
                if(!empty($propertiesData[$item['id']]))
                    $setEntities = collect($propertiesData[$item['id']])->keyBy('value')->toArray();
            @endphp
            <select class="form-control select2-material" id="{{ $item['id'] }}"
                    {{ $item['multiple'] ? ' multiple="multiple"' : ''}}
                    {{ $item['required'] ? ' required="required"' : ''}}
                    name="properties[{{ $item['id'] }}]{{ $item['multiple'] ? '[]' : ''}}">
                @if(empty($item['required']) && empty($item['multiple']))
                    <option></option>
                @endif
                @if(isset($item['relations']))
                    @foreach($item['relations'] as $entity)
                        @if(isset($entity['elements']))
                            @foreach($entity['elements'] as $eElement)
                                <option value="{{ $eElement['id'] }}"
                                        {{ isset($setEntities[$eElement['id']]) ? ' selected="selected"' : ''}}>
                                    ({{ $eElement['id'] }}) {{ $eElement['name'] }}</option>
                            @endforeach
                        @endif
                    @endforeach
                @endif
            </select>
        </div>
    </div>
    @break

    @case(\App\DataType::FILE)
    <label class="col-md-3 col-form-label">{{ $item['name'] }}</label>
    <div class="col-md-9">
        {{-- Если множественное --}}
        @if($item['multiple'])
            {{--Если есть данные по свойству, то вывести в цикле --}}
            @if(!empty($propertiesData[$item['id']]))
                @foreach($propertiesData[$item['id']] as $key => $val)
                    <div class="form-group clearfix">
                        <div class="col-2 float-left">
                            <div class="col-12 btn btn-outline-primary btn-sm float-left">
                                <span>Выберите файл</span>
                                <input type="hidden"
                                       name="properties[{{ $item['id'] }}][]"
                                       value="{{$val['value'] ?? ''}}">
                                <input type="file" id="{{ $item['id'] }}"
                                       name="properties[files][{{ $item['id'] }}][]"
                                       value="{{ $val['file']['name'] ?? '' }}"
                                       oninput="fileInputHandler(this)">
                            </div>
                        </div>
                        <div class="col-8 float-left">
                            <input class="col-12 form-control file_name-{{ $item['id'] }}" type="text"
                                   value="{{ $val['file']['name'] ?? '' }}"
                                   placeholder="Выберите файл"
                                   disabled="disabled">
                        </div>
                        <div class="col-2 float-left">
                            <div class="float-right input-group-btn">
                                @if(!empty($val['file']))
                                    <a href="{{ Storage::url($val['file']['path']) }}"
                                       class="btn btn-sm btn-link btn-primary"
                                       id="link-file-{{$item['id']}}">
                                        <i class="fas fa-download"></i>
                                    </a>
                                @endif
                                <button type="button" class="btn btn-sm btn-link btn-danger"
                                        data-property_id="{{ $item['id'] }}"
                                        onclick="fileDeleteHandler(this)">
                                    <i class="fas fa-trash-alt"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                @endforeach
                {{--Если нет данные, то пустой input--}}
            @else
                <div class="form-group clearfix">
                    <div class="col-2 float-left">
                        <div class="col-12 btn btn-outline-primary btn-sm float-left">
                            <span>Выберите файл</span>
                            <input type="file" id="{{ $item['id'] }}"
                                   {{ $item['required'] ? ' required="required"' : ''}}
                                   name="properties[files][{{ $item['id'] }}][]"
                                   oninput="fileInputHandler(this)">
                        </div>
                    </div>
                    <div class="col-8 float-left">
                        <input class="col-12 form-control file_name-{{ $item['id'] }}" type="text"
                               placeholder="Выберите файл" disabled="disabled">
                    </div>
                    <div class="col-2 float-left">
                        <div class="float-right input-group-btn">
                            <button type="button" class="btn btn-sm btn-link btn-danger"
                                    data-property_id="{{ $item['id'] }}"
                                    onclick="fileDeleteHandler(this)">
                                <i class="fas fa-trash-alt"></i>
                            </button>
                        </div>
                    </div>
                </div>
            @endif
            <div class="text-center">
                <button type="button" class="btn btn-sm btn-outline-info"
                        data-property_id="{{ $item['id'] }}"
                        data-data_type_id="{{$item['data_type_id']}}"
                        onclick="addPropertyValueRow(this)">
                    {{trans('admin.button.add')}}
                </button>
            </div>
            {{--Если не множественное--}}
        @else
            {{--Если есть данные по свойству, то вывести первый элемент из массива --}}
            @if(!empty($propertiesData[$item['id']]))
                <div class="form-group clearfix">
                    <div class="col-2 float-left">
                        <div class="col-12 btn btn-outline-primary btn-sm float-left">
                            <span>Выберите файл</span>
                            <input type="hidden"
                                   name="properties[{{ $item['id'] }}]"
                                   value="{{head($propertiesData[$item['id']])['value'] ?? ''}}">
                            <input type="file" id="{{ $item['id'] }}"
                                   name="properties[files][{{ $item['id'] }}]"
                                   oninput="fileInputHandler(this)">
                        </div>
                    </div>
                    <div class="col-8 float-left">
                        <input class="col-12 form-control file_name-{{ $item['id'] }}" type="text"
                               placeholder="Выберите файл" disabled="disabled"
                               value="{{!empty(head($propertiesData[$item['id']])['file']['name']) ? head($propertiesData[$item['id']])['file']['name'] : '' }}">
                    </div>
                    <div class="col-2 float-left">
                        <div class="float-right input-group-btn">
                            @if(!empty($propertiesData[$item['id']][0]['file']))
                                <a href="{{ Storage::url(head($propertiesData[$item['id']])['file']['path']) }}"
                                   class="btn btn-sm btn-link btn-primary" id="link-file-{{$item['id']}}">
                                    <i class="fas fa-download"></i>
                                </a>
                            @endif
                            <button type="button" class="btn btn-sm btn-link btn-danger"
                                    data-property_id="{{ $item['id'] }}"
                                    onclick="fileDeleteHandler(this)">
                                <i class="fas fa-trash-alt"></i>
                            </button>
                        </div>
                    </div>
                </div>
                {{--Если нет данные, то пустой input--}}
            @else
                <div class="form-group clearfix">
                    <div class="col-2 float-left">
                        <div class="col-12 btn btn-outline-primary btn-sm float-left">
                            <span>Выберите файл</span>
                            <input type="file" id="{{ $item['id'] }}"
                                   {{ $item['required'] ? ' required="required"' : ''}}
                                   name="properties[files][{{ $item['id'] }}]"
                                   oninput="fileInputHandler(this)">
                        </div>
                    </div>
                    <div class="col-8 float-left">
                        <input class="col-12 form-control file_name-{{ $item['id'] }}" type="text"
                               placeholder="Выберите файл" disabled="disabled">
                    </div>
                    <div class="col-2 float-left">
                        <div class="float-right input-group-btn">
                            <button type="button" class="btn btn-sm btn-link btn-danger"
                                    data-property_id="{{ $item['id'] }}"
                                    onclick="fileDeleteHandler(this)">
                                <i class="fas fa-trash-alt"></i>
                            </button>
                        </div>
                    </div>
                </div>
            @endif
        @endif
    </div>
    @break

    @case(\App\DataType::USER)
    <label class="col-md-3 col-form-label" for="{{ $item['id'] }}">{{ $item['name'] }}</label>
    <div class="col-md-9">
        <div class="form-group">
            @php
                $setUsers = [];
                if(!empty($propertiesData[$item['id']]))
                    $setUsers = collect($propertiesData[$item['id']])->keyBy('value')->toArray();
            @endphp
            <select class="form-control select2-material" id="{{ $item['id'] }}"
                    {{ $item['multiple'] ? ' multiple="multiple"' : ''}}
                    {{ $item['required'] ? ' required="required"' : ''}}
                    name="properties[{{ $item['id'] }}]{{ $item['multiple'] ? '[]' : ''}}">
                @if(empty($item['required']) && empty($item['multiple']))
                    <option></option>
                @endif
                @if(isset($item['options']))
                    @foreach($item['options'] as $option)
                        <option value="{{ $option['id'] }}"
                                {{ isset($setUsers[$option['id']]) ? ' selected="selected"' : ''}}
                        >({{ $option['id'] }}) {{ $option['name'] }}</option>
                    @endforeach
                @endif
            </select>
        </div>
    </div>
    @break
@endswitch
<script>
    /** Добавление новой строки в таблицу для ввода значения списка
     * @param btn
     */
    function addPropertyValueRow(btn) {
        var dataType = $(btn).data('data_type_id');
        switch (dataType) {
            case Number('<?=\App\DataType::STRING?>'):
            case Number('<?=\App\DataType::INTEGER?>'):
            case Number('<?=\App\DataType::TEXT?>'):
            case Number('<?=\App\DataType::FILE?>'):
                var propertyId = $(btn).data('property_id');
                var divForm = $(btn).parent().prev();
                var copy = $(divForm).clone();
                $(copy).find('input,textarea,select').val('');
                $(copy).insertAfter(divForm);
                break;
        }
    }

    function fileInputHandler(input) {
        if (input.files.length) {
            $(input)
                .parents('.col-2.float-left')
                .next()
                .find('input.file_name-' + input.id)
                .val(input.files[0].name);
        }
    }

    function fileDeleteHandler(btn) {
        var nameDiv = $(btn).parents('.col-2.float-left').prev();
        var fileDiv = $(nameDiv).prev();
        $(nameDiv).find('input').val('');
        $(fileDiv).find('input').val('');
    }
</script>