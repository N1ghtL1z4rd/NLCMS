<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingListValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setting_list_values', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('setting_list_id')->unsigned();
            $table->string('name');
            $table->string('code');
//            $table->boolean('default')->default(false); // значение по умолчанию
            $table->boolean('active')->default(true);
            $table->bigInteger('sort')->nullable()->default(500);
            $table->bigInteger('created_by')->unsigned()->nullable()->comment('Кто создал');
            $table->bigInteger('modified_by')->unsigned()->nullable()->comment('Кто изменил');
            $table->timestamps();
            $table->softDeletes()->comment('Когда удалено');
            // FK
            $table->foreign('setting_list_id')->references('id')->on('setting_lists')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setting_list_values');
    }
}
