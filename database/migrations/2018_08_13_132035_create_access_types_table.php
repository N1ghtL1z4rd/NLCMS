<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccessTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('access_types', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('code');
            $table->boolean('active')->default(true);
            $table->boolean('system')->default(false);
            $table->bigInteger('sort')->nullable()->default(500);
            $table->bigInteger('created_by')->unsigned()->nullable()->comment('Кто создал');
            $table->bigInteger('modified_by')->unsigned()->nullable()->comment('Кто изменил');
            $table->timestamps();
            $table->softDeletes()->comment('Когда удалено');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('access_types');
    }
}
