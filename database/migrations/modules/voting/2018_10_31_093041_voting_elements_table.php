<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class VotingElementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voting_elements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('voting_type_id');
            $table->bigInteger('element_id');
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('modified_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
            // FK
            $table->foreign('voting_type_id')->references('id')->on('voting_types');
            $table->foreign('element_id')->references('id')->on('entity_elements');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voting_elements');
    }
}
