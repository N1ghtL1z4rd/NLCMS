<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkflowElementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workflow_elements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('workflow_status_id');
            $table->integer('element_id');
            $table->boolean('active')->default(false)->comment('Активный статус');
            $table->integer('created_by')->nullable();
            $table->integer('modified_by')->nullable();
            $table->timestamps();
            // FK
            $table->foreign('workflow_status_id')->references('id')->on('workflow_statuses');
            $table->foreign('element_id')->references('id')->on('entity_elements');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workflow_elements');
    }
}
