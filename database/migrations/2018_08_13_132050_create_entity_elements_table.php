<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntityElementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entity_elements', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned()->comment('ID');
            $table->string('name')->comment('Наименование');
            $table->string('code')->nullable()->comment('Символьный код');
            $table->bigInteger('extended_id')->nullable()->comment('Расширенный ID');
            $table->bigInteger('preview_img')->nullable()->comment('Картинка для анонса');
            $table->text('preview')->nullable()->comment('Анонс');
            $table->bigInteger('detail_img')->nullable()->comment('Картинка детальная');
            $table->text('detail')->nullable()->comment('Текст детально');
            $table->bigInteger('entity_id')->unsigned()->comment('ID сущности');
            $table->boolean('active')->default(true)->comment('Активность');
            $table->bigInteger('sort')->unsigned()->nullable()->default(500)->comment('Сортировка');
            $table->bigInteger('created_by')->unsigned()->nullable()->comment('Кто создал');
            $table->bigInteger('modified_by')->unsigned()->nullable()->comment('Кто изменил');
            $table->timestamps();
            $table->softDeletes();
            // FK
            $table->foreign('entity_id')->references('id')->on('entities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entity_elements');
    }
}
