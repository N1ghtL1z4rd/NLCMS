<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingListObjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setting_list_objects', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('code');
            $table->string('model');
            $table->string('table');
            $table->boolean('active')->default(true);
            $table->boolean('system')->default(false);
            $table->bigInteger('sort')->unsigned()->nullable()->default(500);
            $table->bigInteger('created_by')->unsigned()->nullable()->comment('Кто создал');
            $table->bigInteger('modified_by')->unsigned()->nullable()->comment('Кто изменил');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setting_list_objects');
    }
}
