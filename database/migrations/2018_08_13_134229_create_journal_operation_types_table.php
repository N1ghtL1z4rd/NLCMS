<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJournalOperationTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('journal_operation_types', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('code')->unique();
            $table->boolean('active')->default(true);
            $table->boolean('system')->default(false);
            $table->bigInteger('sort')->unsigned()->nullable()->default(500);
            $table->bigInteger('created_by')->unsigned()->nullable()->comment('Кто создал');
            $table->bigInteger('modified_by')->unsigned()->nullable()->comment('Кто изменил');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('journal_operation_types');
    }
}
