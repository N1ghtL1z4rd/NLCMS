<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingListRelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setting_list_relations', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('setting_list_id')->unsigned();
            $table->bigInteger('value');
            $table->timestamps();
            // FK
            $table->foreign('setting_list_id')->references('id')->on('setting_lists')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setting_list_relations');
    }
}
