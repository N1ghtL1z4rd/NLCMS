<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entities', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('code');
            $table->bigInteger('entity_type_id')->unsigned();
            $table->boolean('active')->default(true);
            $table->boolean('system')->default(false);
            $table->boolean('logging')->default(true);
            $table->bigInteger('sort')->unsigned()->nullable()->default(500);
            $table->bigInteger('created_by')->unsigned()->nullable()->comment('Кто создал');
            $table->bigInteger('modified_by')->unsigned()->nullable()->comment('Кто изменил');
            $table->timestamps();
            $table->softDeletes()->comment('Когда удалено');
            // FK
            $table->foreign('entity_type_id')->references('id')->on('entity_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entities');
    }
}
