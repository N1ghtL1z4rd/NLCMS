<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('name')->comment('Имя/Никнэйм');
            $table->string('email')->comment('Электронная почта');
            $table->string('password')->comment('Хэш пароля');
            $table->boolean('active')->default(true)->comment('Активность');
            $table->boolean('system')->default(false)->comment('Системное');
            $table->bigInteger('sort')->unsigned()->nullable()->default(500)->comment('Сортировка');
            $table->rememberToken();
            $table->bigInteger('created_by')->unsigned()->nullable()->comment('Кто создал');
            $table->bigInteger('modified_by')->unsigned()->nullable()->comment('Кто изменил');
            $table->timestamps();
            $table->softDeletes()->comment('Когда удалено');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
