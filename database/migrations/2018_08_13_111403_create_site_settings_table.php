<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_settings', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('setting_list_id')->unsigned();
            $table->text('value')->nullable();
            $table->bigInteger('sort')->unsigned()->nullable()->default(500);
            $table->bigInteger('modified_by')->unsigned();
            $table->timestamps();
            // FK
            $table->foreign('setting_list_id')->references('id')->on('setting_lists');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_settings');
    }
}
