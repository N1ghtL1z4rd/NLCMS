<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateElementPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('element_properties', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('element_id')->unsigned();
            $table->bigInteger('property_id')->unsigned();
            $table->text('value');
            $table->bigInteger('sort')->unsigned()->nullable()->default(100);
            $table->timestamps();
            $table->softDeletes();
            // FK
            $table->foreign('element_id')->references('id')->on('entity_elements')->onDelete('cascade');
            $table->foreign('property_id')->references('id')->on('properties')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('element_properties');
    }
}
