<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_settings', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('setting_list_id')->unsigned();
            $table->text('value')->nullable();
            $table->bigInteger('sort')->unsigned()->nullable()->default(500);
            $table->timestamps();
            // FK
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('setting_list_id')->references('id')->on('setting_lists');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_settings');
    }
}
