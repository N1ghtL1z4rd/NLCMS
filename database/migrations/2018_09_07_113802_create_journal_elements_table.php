<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJournalElementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('journal_elements', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('operation_type_id')->unsigned();
            $table->bigInteger('element_id')->unsigned();
            $table->longText('condition')->nullable();
            $table->bigInteger('created_by')->unsigned()->nullable()->comment('Кто инициировал действие (запись в журнал)');
            $table->bigInteger('modified_by')->unsigned()->nullable()->comment('Кто изменил запись журнала (отредактировал запись/удалил)');
            $table->timestamps();
            $table->softDeletes();
            // FK
            $table->foreign('operation_type_id')->references('id')->on('journal_operation_types');
            $table->foreign('element_id')->references('id')->on('entity_elements');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('journal_elements');
    }
}
