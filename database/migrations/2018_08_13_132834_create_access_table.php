<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('access', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('access_type_id')->unsigned();
            $table->bigInteger('object_type_id')->unsigned();
            $table->bigInteger('object_id')->unsigned();
            $table->bigInteger('subject_type_id')->unsigned();
            $table->bigInteger('subject_id')->unsigned();
            $table->bigInteger('created_by')->unsigned()->nullable()->comment('Кто создал');
            $table->bigInteger('modified_by')->unsigned()->nullable()->comment('Кто изменил');
            $table->timestamps();
            $table->softDeletes();
            //FK
            $table->foreign('access_type_id')->references('id')->on('access_types');
            $table->foreign('object_type_id')->references('id')->on('access_object_types');
            $table->foreign('subject_type_id')->references('id')->on('access_subject_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('access');
    }
}
