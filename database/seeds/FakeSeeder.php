<?php

use Illuminate\Database\Seeder;

class FakeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('ru_RU');
//        for ($i = 0; $i < 10000; $i++) {
//            DB::table('sections')->insert([
//                'name' => 'test' . str_random(),
//                'url' => 'test' . '/' . str_random(),
//                'sort' => $i * 10000,
//            ]);
//        }

//        for ($i = 0; $i < 10000; $i++) {
//            DB::table('setting_lists')->insert([
//                'name' => 'test' . str_random(),
//                'code' => 'test' . '/' . str_random() . $i,
//                'setting_list_object_id' => rand(1, 2),
//                'data_type_id' => rand(1, 10),
//                'sort' => 10000 + ($i * 10)
//            ]);
//        }

        for ($i = 0; $i < 200; $i++) {
            DB::table('users')->insert([
                'name' => $faker->name,
                'email' => $faker->unique()->safeEmail,
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                'active' => true,
                'system' => (bool)rand(0, 1),
                'sort' => 100 + $i,
                'remember_token' => \Illuminate\Support\Str::random(10),
            ]);
        }

        for ($i = 0; $i < 3; $i++) {
            DB::table('entity_types')->insert([
                'name' => $faker->country(),
                'code' => 'ENTITY_TYPE_' . $i,
                'sort' => 100 + $i
            ]);
        }

        for ($i = 0; $i < 5; $i++) {
            DB::table('entities')->insert([
                'name' => $faker->city(),
                'code' => 'ENTITY_' . $i,
                'entity_type_id' => 1,
                'sort' => 100 + $i
            ]);
        }

        for ($i = 0; $i < 400; $i++) {
            DB::table('entity_elements')->insert([
                'name' => $faker->company(),
                'code' => 'ELEMENT_CODE_' . $i,
                'extended_id' => $faker->numberBetween(3),
                'preview' => $faker->text(30),
                'detail' => $faker->realText(),
                'entity_id' => 1,
                'sort' => 100 + $i
            ]);
        }
        for ($i = 0; $i < 1400; $i++) {
            DB::table('entity_elements')->insert([
                'name' => $faker->company(),
                'code' => 'ELEMENT_CODE_' . $i,
                'extended_id' => $faker->numberBetween(3),
                'preview' => $faker->text(30),
                'detail' => $faker->realText(),
                'entity_id' => 2,
                'sort' => 100 + $i
            ]);
        }

        for ($i = 0; $i < 100; $i++) {
            $dataTypeId = rand(1, 10);
            $propertyId = DB::table('properties')->insertGetId([
                'name' => 'Свойство_' . $faker->realText(10),
                'code' => 'property_code_' . $i,
                'entity_id' => 3,
                'multiple' => in_array($dataTypeId, [4, 5, 6]) ? 0 : (bool)rand(0, 1),
                'data_type_id' => $dataTypeId,
                'sort' => 100 + $i
            ]);
            switch ($dataTypeId) {
                case \App\DataType::SELECT:
                    for ($j = 1; $j < 101; $j++) {
                        DB::table('property_values')->insertGetId([
                            'property_id' => $propertyId,
                            'name' => 'Значение_свойства_' . $propertyId . '_№-' . $j,
                            'code' => 'property_value_' . $propertyId . '_' . $j,
                            'sort' => 100 + ($j * 10)
                        ]);
                    }
                    break;
                case \App\DataType::ENTITY:
                    DB::table('property_relations')->insertGetId([
                        'property_id' => $propertyId,
                        'value' => 1
                    ]);
                    break;
            }
        }


        $properties = \App\Property::where('entity_id', 3)->get()->toArray();
        for ($i = 1; $i < 201; $i++) {
            $elementId = DB::table('entity_elements')->insertGetId([
                'name' => $faker->jobTitle(),
                'code' => 'ELEMENT_code_' . $i,
                'extended_id' => $faker->numberBetween(),
                'preview' => $faker->text(30),
                'detail' => $faker->realText(),
                'entity_id' => 3,
                'sort' => 100 + $i
            ]);
            /**
             * 1 ;Строка;Текст ограниченный длиной в 256 символов;STRING
             * 2 ;Число;Целочисленные значения;INTEGER
             * 3 ;Текст;Текст без форматирования;TEXTAREA
             * 4 ;Переключатель;Переключатель (чекбокс);CHECKBOX
             * 5 ;Дата;Поле для ввода даты без времени;DATE
             * 6 ;Дата/Время;Поде для ввода даты и времени;DATETIME
             * 7 ;Список;Выпадающий список;SELECT
             * 8 ;Привязка к элементам сущностей;Привязка к элементам сущностей;ENTITY
             * 9 ;Файл;Файл;FILE
             * 10 ;Привязка к пользователю;Привязка к пользователю;USER
             */
            foreach ($properties as $property) {
                switch ($property['data_type_id']) {
                    case \App\DataType::STRING:
                        if ((bool)$property['multiple'] === true) {
                            $value = [];
                            for ($a = 0; $a < 3; $a++) {
                                $value[] = $faker->text(15);
                            }
                        } else {
                            $value = $faker->text(15);
                        }
                        break;
                    case \App\DataType::INTEGER:
                        if ((bool)$property['multiple'] === true) {
                            $value = [];
                            for ($b = 0; $b < 3; $b++) {
                                $value[] = $faker->numberBetween();
                            }
                        } else {
                            $value = $faker->numberBetween();
                        }
                        break;
                    case \App\DataType::TEXT:
                        if ((bool)$property['multiple'] === true) {
                            $value = [];
                            for ($c = 0; $c < 3; $c++) {
                                $value[] = $faker->realText();
                            }
                        } else {
                            $value = $faker->realText();
                        }
                        break;
                    case \App\DataType::DATE:
                        $value = $faker->date();
                        break;
                    case \App\DataType::DATETIME:
                        $value = $faker->dateTimeBetween();
                        break;
                    case \App\DataType::SELECT:
                        if ((bool)$property['multiple'] === true) {
                            $value = [];
                            for ($d = 0; $d < 15; $d++) {
                                $rndId = \App\PropertyValue::where('property_id', $property['id'])->inRandomOrder()->first()->id;
                                $value[$rndId] = $rndId;
                            }
                        } else {
                            $value = \App\PropertyValue::where('property_id', $property['id'])->inRandomOrder()->first()->id;
                        }
                        break;
                    case \App\DataType::CHECKBOX:
                        $value = 1;
                        break;
                    case \App\DataType::ENTITY:
                        if ((bool)$property['multiple'] === true) {
                            $value = [];
                            for ($e = 0; $e < 10; $e++) {
                                $rndIdE = rand(7, 406);
                                $value[$rndIdE] = $rndIdE;
                            }
                        } else {
                            $value = rand(1, 400);
                        }
                        break;
                    case \App\DataType::USER:
                        if ((bool)$property['multiple'] === true) {
                            $value = [];
                            for ($f = 0; $f < 5; $f++) {
                                $rndIdE = rand(1, 201);
                                $value[$rndIdE] = $rndIdE;
                            }
                        } else {
                            $value = rand(1, 201);
                        }
                        break;
                }

                if (!empty($value)) {
                    if (is_array($value)) {
                        $srt = 10;
                        foreach ($value as $item) {
                            DB::table('element_properties')->insert([
                                'element_id' => $elementId,
                                'property_id' => $property['id'],
                                'value' => $item,
                                'sort' => $srt * 10
                            ]);
                        }
                    } else {
                        DB::table('element_properties')->insert([
                            'element_id' => $elementId,
                            'property_id' => $property['id'],
                            'value' => $value,
                            'sort' => 10
                        ]);
                    }
                }
            }
        }
    }
}
