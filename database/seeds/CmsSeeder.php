<?php

use Illuminate\Database\Seeder;

class CmsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $globalData = [
            'active' => true,
            'system' => true,
            'created_by' => 1,
            'modified_by' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ];
        /**
         * Пользователи
         */
        DB::table('users')
            ->insert(array_merge([
                'name' => 'Администратор сайта',
                'email' => 'admin@admin.com',
                'password' => bcrypt('secret'),
                'sort' => 1
            ], $globalData));
        /**
         * Группы пользователей
         */
        DB::table('groups')
            ->insert(array_merge([
                'name' => 'Администраторы',
                'description' => 'Группа администраторов сайта',
                'code' => 'ADMIN',
                'sort' => 1
            ], $globalData));
        /**
         * Привязать админа к группе "Администраторы"
         */
        DB::table('user_groups')
            ->insert([
                'user_id' => 1,
                'group_id' => 1
            ]);

        /**
         * Типы данных
         */
        $dataTypes = [
            [
                'name' => 'Строка',
                'description' => 'Текст ограниченный длиной в 256 символов',
                'code' => 'STRING',
            ],
            [
                'name' => 'Число',
                'description' => 'Целочисленные значения',
                'code' => 'INTEGER',
            ],
            [
                'name' => 'Текст',
                'description' => 'Текст без форматирования',
                'code' => 'TEXTAREA',
            ],
            [
                'name' => 'Переключатель',
                'description' => 'Переключатель (чекбокс)',
                'code' => 'CHECKBOX',
            ],
            [
                'name' => 'Дата',
                'description' => 'Поле для ввода даты без времени',
                'code' => 'DATE',
            ],
            [
                'name' => 'Дата/Время',
                'description' => 'Поде для ввода даты и времени',
                'code' => 'DATETIME',
            ],
            [
                'name' => 'Список',
                'description' => 'Выпадающий список',
                'code' => 'SELECT',
            ],
            [
                'name' => 'Привязка к элементам сущностей',
                'description' => 'Привязка к элементам сущностей',
                'code' => 'ENTITY',
            ],
            [
                'name' => 'Файл',
                'description' => 'Файл',
                'code' => 'FILE',
            ],
            [
                'name' => 'Привязка к пользователю',
                'description' => 'Привязка к пользователю',
                'code' => 'USER',
            ]
        ];
        foreach ($dataTypes as $key => $dataType) {
            DB::table('data_types')
                ->insert(array_merge($dataType, $globalData, ['sort' => ($key + 1) * 10]));
        }

        /**
         * Меню сайта
         */
        DB::table('menus')
            ->insert(array_merge([
                'name' => 'Главная',
                'description' => 'Меню административной панели: главная',
                'code' => 'ADMIN_MAIN',
                'sort' => 1,
            ], $globalData));
        DB::table('menus')
            ->insert(array_merge([
                'name' => 'Контент',
                'description' => 'Меню административной панели: контент',
                'code' => 'ADMIN_CONTENT',
                'sort' => 2,
            ], $globalData));
        DB::table('menus')
            ->insert(array_merge([
                'name' => 'Настройки',
                'description' => 'Меню административной панели: настройки',
                'code' => 'ADMIN_SETTINGS',
                'sort' => 3,
            ], $globalData));

        /**
         * Разделы сайта
         */
        $sections = [
            [
                'name' => 'Главная',
                'url' => '/admin'
            ],
            [
                'name' => 'Настройки сайта',
                'url' => '/admin/site-settings'
            ],
            [
                'name' => 'Разделы',
                'url' => '/admin/sections'
            ],
            [
                'name' => 'Пользователи',
                'url' => '/admin/users'
            ],
            [
                'name' => 'Группы',
                'url' => '/admin/groups'
            ],
            [
                'name' => 'Настраиваемые параметры',
                'url' => '/admin/setting-lists'
            ],
            [
                'name' => 'Типы сущностей',
                'url' => '/admin/entity-types'
            ],
            [
                'name' => 'Сущности',
                'url' => '/admin/entities'
            ],
            [
                'name' => 'Элементы сущностей',
                'url' => '/admin/entity-elements'
            ],
            [
                'name' => 'Свойства сущностей',
                'url' => '/admin/properties'
            ],
            [
                'name' => 'Журнал',
                'url' => '/admin/journal'
            ],
//            [
//                'name' => 'Команды Artisan',
//                'url' => '/admin/commands'
//            ],
//            [
//                'name' => 'Конструктор меню',
//                'url' => '/admin/menus'
//            ],
            [
                'name' => 'Таблицы БД',
                'url' => '/admin/tables'
            ],
            [
                'name' => 'Резервные копии',
                'url' => '/admin/backups'
            ],
            [
                'name' => 'Информация о PHP',
                'url' => '/admin/php-info'
            ],
        ];

        foreach ($sections as $key => $section) {
            DB::table('sections')->insert(array_merge($section, $globalData, ['sort' => ($key + 1) * 10]));
        }

        /**
         * Меню админки
         */
        $menuItems = [
            [
                'name' => 'Рабочий стол',
                'icon' => 'fas fa-th-large',
                'menu_id' => 1,
                'whereSection' => ['url' => '/admin']
            ],
            [
                'name' => 'Типы сущностей',
                'menu_id' => 2,
                'icon' => 'fas fa-object-ungroup',
                'whereSection' => ['url' => '/admin/entity-types'],
            ],
            [
                'name' => 'Настройки сайта',
                'icon' => 'fas fa-cog',
                'menu_id' => 3,
                'whereSection' => ['url' => '/admin/site-settings']
            ],
            [
                'name' => 'Разделы',
                'icon' => 'far fa-copy',
                'menu_id' => 3,
                'whereSection' => ['url' => '/admin/sections']
            ],
            [
                'name' => 'Пользователи',
                'menu_id' => 3,
                'icon' => 'fas fa-user',
                'whereSection' => ['url' => '/admin/users'],
            ],
            [
                'name' => 'Группы',
                'menu_id' => 3,
                'icon' => 'fas fa-users',
                'whereSection' => ['url' => '/admin/groups'],
            ],
            [
                'name' => 'Настраиваемые параметры',
                'icon' => 'fas fa-sliders-h',
                'menu_id' => 3,
                'whereSection' => ['url' => '/admin/setting-lists'],
            ],
            [
                'name' => 'Журнал',
                'icon' => 'fas fa-history',
                'menu_id' => 3,
                'whereSection' => ['url' => '/admin/journal']
            ],
            [
                'name' => 'Инструменты',
                'icon' => 'fas fa-wrench',
                'menu_id' => 3,
                'children' => [
//                    [
//                        'name' => 'Конструктор меню',
//                        'menu_id' => 3,
//                        'whereSection' => ['url' => '/admin/menus']
//                    ],
                    [
                        'name' => 'Таблицы БД',
                        'menu_id' => 3,
                        'whereSection' => ['url' => '/admin/tables']
                    ],
                    [
                        'name' => 'Резервные копии',
                        'menu_id' => 3,
                        'whereSection' => ['url' => '/admin/backups']
                    ],
//                    [
//                        'name' => 'Команды Artisan',
//                        'menu_id' => 3,
//                        'whereSection' => ['url' => '/admin/commands']
//                    ],
                ]
            ],
            [
                'name' => 'Информация',
                'menu_id' => 3,
                'icon' => 'fas fa-info-circle',
                'children' => [
                    [
                        'name' => 'Информация o PHP',
                        'menu_id' => 3,
                        'whereSection' => ['url' => '/admin/php-info']
                    ]
                ]
            ],
        ];

        foreach ($menuItems as $key => $menuItem) {
            if (!empty($menuItem['children'])) {
                $children = $menuItem['children'];
                unset($menuItem['children']);
                $parentId = DB::table('menu_items')
                    ->insertGetId(array_merge($menuItem, $globalData, ['sort' => ($key + 1) * 10]));
                if (!empty($children)) {
                    foreach ($children as $childKey => $child) {
                        $sectionId = DB::table('sections')->where($child['whereSection'])->get()->first()->id;
                        unset($child['whereSection']);
                        DB::table('menu_items')
                            ->insert(array_merge($child, $globalData,
                                ['sort' => ($childKey + 1) * 10, 'section_id' => $sectionId, 'parent_id' => $parentId]
                            ));
                    }
                }
            } else {
                $sectionId = DB::table('sections')->where($menuItem['whereSection'])->get()->first()->id;
                unset($menuItem['whereSection']);
                DB::table('menu_items')
                    ->insert(array_merge($menuItem, $globalData,
                        ['sort' => ($key + 1) * 10, 'section_id' => $sectionId]
                    ));
            }
        }

        /**
         * Объекты настройки
         */
        $settingListObjects = [
            [
                'name' => 'Сайт',
                'code' => 'SITE',
                'model' => '\App\SiteSetting',
                'table' => 'site_settings'
            ],
//            [
//                'name' => 'Разделы',
//                'code' => 'sections'
//            ],
            [
                'name' => 'Пользователь',
                'code' => 'USER',
                'model' => '\App\UserSetting',
                'table' => 'user_settings'
            ],
//            [
//                'name' => 'Группы пользователей',
//                'code' => 'groups'
//            ],
//            [
//                'name' => 'Свойства сущностей',
//                'code' => 'properties'
//            ],
        ];
        foreach ($settingListObjects as $key => $settingListObject) {
            DB::table('setting_list_objects')
                ->insert(array_merge($settingListObject, $globalData, ['sort' => ($key + 1) * 10]));
        }


        /**
         * Настройки сайта по code = site-settings
         */
        $settingLists = [
            'SITE' => [
                [
                    'name' => 'Название сайта',
                    'code' => 'SITE_NAME',
                    'data_type_id_where' => [
                        'code' => 'STRING'
                    ],
                ],
            ],
            'USER' => [
                [
                    'name' => 'Фамилия',
                    'code' => 'USER_SECOND_NAME',
                    'data_type_id_where' => [
                        'code' => 'STRING'
                    ]
                ],
                [
                    'name' => 'Имя',
                    'code' => 'USER_FIRST_NAME',
                    'data_type_id_where' => [
                        'code' => 'STRING'
                    ]
                ],
                [
                    'name' => 'Отчество',
                    'code' => 'USER_MIDDLE_NAME',
                    'data_type_id_where' => [
                        'code' => 'STRING'
                    ]
                ],
//                [
//                    'name' => 'Телефон',
//                    'code' => 'USER_PHONE',
//                    'data_type_id_where' => [
//                        'code' => 'string'
//                    ]
//                ],
//                [
//                    'name' => 'Дата рождения',
//                    'code' => 'USER_BIRTHDAY',
//                    'data_type_id_where' => [
//                        'code' => 'date'
//                    ]
//                ],
            ],
        ];

        foreach ($settingLists as $code => $settingList) {
            foreach ($settingList as $item) {
                $currentData = [
                    'setting_list_object_id' => DB::table('setting_list_objects')->where(['code' => $code])->get()->first()->id,
                    'data_type_id' => DB::table('data_types')->where($item['data_type_id_where'])->get()->first()->id,
                ];
                unset($item['data_type_id_where']);
                DB::table('setting_lists')
                    ->insert(array_merge($item, $globalData, ['sort' => ($key + 1) * 10], $currentData));
            }

        }

        /**
         * Типы операций журнала
         */
        DB::table('journal_operation_types')
            ->insert(array_merge([
                'name' => 'Добавление',
                'description' => 'Добавление элемента',
                'code' => 'ADD',
                'sort' => 10,
            ], $globalData));

        DB::table('journal_operation_types')
            ->insert(array_merge([
                'name' => 'Изменение',
                'description' => 'Редактирование элемента',
                'code' => 'EDIT',
                'sort' => 20,
            ], $globalData));

        DB::table('journal_operation_types')
            ->insert(array_merge([
                'name' => 'Удаление',
                'description' => 'Удаление элемента',
                'code' => 'DELETE',
                'sort' => 30,
            ], $globalData));

        /**
         * Типы доступа
         */
        DB::table('access_types')
            ->insert(array_merge([
                'name' => 'Просмотр',
                'code' => 'VIEW',
                'sort' => 10,
            ], $globalData));

        DB::table('access_types')
            ->insert(array_merge([
                'name' => 'Редактирование',
                'code' => 'EDIT',
                'sort' => 20,
            ], $globalData));

        DB::table('access_types')
            ->insert(array_merge([
                'name' => 'Удаление',
                'code' => 'DELETE',
                'sort' => 30,
            ], $globalData));

        /**
         * Субъекты доступа
         */
        DB::table('access_subject_types')
            ->insert(array_merge([
                'name' => 'Пользователь',
                'description' => '',
                'code' => 'USER',
                'sort' => 10,
            ], $globalData));

        DB::table('access_subject_types')
            ->insert(array_merge([
                'name' => 'Группа пользователей',
                'description' => '',
                'code' => 'GROUP',
                'sort' => 20,
            ], $globalData));

        /**
         * Объекты доступа
         */
        DB::table('access_object_types')
            ->insert(array_merge([
                'name' => 'Раздел сайта',
                'description' => '',
                'code' => 'SECTION',
                'sort' => 10,
            ], $globalData));
    }
}
