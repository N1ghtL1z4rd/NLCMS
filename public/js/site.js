function dump(data) {
    console.log(data);
}

$(document).ready(function (e) {
    $('#mainCallbackModalForm').submit(function (e) {
        e.preventDefault();
        let form = $('#mainCallbackModalForm').serializeArray();
        $.ajax({
            type: "POST",
            url: "/ajax/callback",
            data: form,
            success: function (msg) {
                $('#mainCallbackModal').modal('hide');
            }
        });
    });


    // $('#mainCallbackModalFormSubmit').on('click', function (e) {
    //     let form = $('#mainCallbackModalForm').serializeArray();
    //     $.ajax({
    //         type: "POST",
    //         url: "/ajax/callback",
    //         data: form,
    //         success: function (msg) {
    //             $('#mainCallbackModal').modal('hide');
    //         }
    //     });
    // });

    // $('#mainCostCalculationFormSubmitBtn').on('click', function (e) {
    //     let form = $('#mainCostCalculationForm').serializeArray();
    //     $.ajax({
    //         type: "POST",
    //         url: '/local/ajax/addSample.php',
    //         data: form,
    //         success: function (msg) {
    //             $('#mainCostCalculationModal').modal('show');
    //         }
    //     });
    // });
    //
    // $('#mainNewQuestionModalFormSubmit').on('click', function (e) {
    //     let form = $('#mainNewQuestionModalForm').serializeArray();
    //     $.ajax({
    //         type: "POST",
    //         url: '/local/ajax/addQuestion.php',
    //         data: form,
    //         success: function (msg) {
    //             $('#mainNewQuestionModal').modal('hide');
    //         }
    //     });
    // });
    //
    // $('#mainSubscribeFormSubmit').on('click', function (e) {
    //     let form = $('#mainSubscribeForm').serializeArray();
    //     $.ajax({
    //         type: "POST",
    //         url: '/local/ajax/addSubscribe.php',
    //         data: form,
    //         success: function (msg) {
    //             $('#mainSubscribeModal').modal('show');
    //         }
    //     });
    // });
});
