function _pre(data) {
    console.log(data);
}

/**
 * Обработка активности пунктов меню в админке
 * @param url
 */
function setActiveMenuItem(url) {
    let findLink = $('#accordionMenu').find('a.nav-link[href $="' + url + '"]');
    if (findLink.length === 1) {
        let target = findLink[0];
        $(target).parents('li.nav-item').addClass('active');
        $(target).parents('div.collapse').addClass('show');
        $(target).parents('div[id ^= "sidebar-panel-"]').addClass('show');
    }
}

/**
 * Обработка чекбокса выбора/снятия выбора всех записей на странице
 * @param el
 */
function selectAllRowsForActions(el) {
    if (el.checked === true) {
        $('table td input[type=checkbox][name="setDelete[]"]').prop('checked', true);
        let checkedRows = $('table td').find('input[type=checkbox][name="setDelete[]"]:checked');
        if (checkedRows.length > 0)
            $('#itemsListActions').removeClass('d-none');
    }

    if (el.checked === false) {
        $('table td input[type=checkbox][name="setDelete[]"]').prop('checked', false);
        $('#itemsListActions').addClass('d-none');
    }
}

/**
 * Обработка чекбокса выбора/снятия выбора одной записи на странице
 * @param el
 */
function selectRowForActions(el) {
    let allRows = $('table td').find('input[type=checkbox][name="setDelete[]"]');
    let checkedRows = $('table td').find('input[type=checkbox][name="setDelete[]"]:checked');
    if (allRows.length === checkedRows.length) {
        $('table #chSelectAllRowsForActions').prop('checked', true);
    } else if (allRows.length > checkedRows.length) {
        $('table #chSelectAllRowsForActions').prop('checked', false);
    }
    if (el.checked === true) {
        $('#itemsListActions').removeClass('d-none');
    }
    if (el.checked === false && checkedRows.length === 0) {
        $('#itemsListActions').addClass('d-none');
    }
}

/**
 * Формирование таблицы dt для элементов сущностей
 * @param settings.csrf
 * @param settings.dTable
 * @param settings.sColumns
 * @param settings.urlColumns
 * @param settings.urlData
 */
function initDtElements(settings) {
    // var selected = [];
    tHead = $.ajax({
        "headers": {
            'X-CSRF-TOKEN': settings.csrf
        },
        "url": settings.urlColumns,
        "type": "POST",
        "data": {selectedColumns: $(settings.sColumns).val()}
    }).done(function () {
        dtTableAjax = $(settings.dTable)
            .dataTable({
                'dom': '<"row"<"col"l><"col"i><"col"p>>rt<"row"<"col"l><"col"i><"col"p>>r',
                "scrollX": true,
                "autoWidth": false,
                "processing": true,
                "serverSide": true,
                "ajax": {
                    "url": settings.urlData,
                    "type": "POST",
                    "headers": {
                        'X-CSRF-TOKEN': settings.csrf
                    },
                    /**
                     * Функция отрабатывает перед отправкой ajax, тут можно собирать данные для отправки
                     */
                    "data": function (d) {
                        d.selectedColumns = $(settings.sColumns).val();
                    }
                },
                // "initComplete": function () {
                //     console.log(this);
                //     var api = this.api();
                //     api.$('td').click( function () {
                //         api.search( this.innerHTML ).draw();
                //     } );
                // },
                // "rowCallback": function( row, data ) {
                //     if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                //         $(row).addClass('selected');
                //     }
                // },
                "language": {
                    "url": '/libs/datatables/lang/ru_RU.json'
                },
                "lengthMenu": [[10, 20, 50, 100, 500, -1], [10, 20, 50, 100, 500, "Все"]],
                "pageLength": 20,
                "order": tHead.responseJSON.order,
                "columns": tHead.responseJSON.columns,
            });
        /**
         * Выделение строк таблицы
         */
        // $('.dataTable-table-ajax-f tbody').on('click', 'tr', function () {
        //     var id = this.id;
        //     var index = $.inArray(id, selected);
        //
        //     // if (index === -1) {
        //     //     selected.push(id);
        //     // } else {
        //     //     selected.splice(index, 1);
        //     // }
        //
        //     $(this).toggleClass('bg-info');
        // });
        /**
         * скрыть колонки https://datatables.net/examples/api/show_hide.html
         */
        $('a.toggle-vis').on('click', function (e) {
            e.preventDefault();

            // Get the column API object
            var column = table.column($(this).attr('data-column'));

            // Toggle the visibility
            column.visible(!column.visible());
        });
        // $('#myInput').on('keyup', function () {
        //     dtTableAjax.api().search(this.value).draw();
        // });
        //     data = JSON.parse(jqxhr.responseText);
        //     // Iterate each column and print table headers for Datatables
        //     $.each(data.columns, function (k, colObj) {
        //         str = '<th>' + colObj.name + '</th>';
        //         $(str).appendTo(tableName + '>thead>tr');
        //     })
    });
}