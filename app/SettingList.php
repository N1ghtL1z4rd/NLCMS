<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SettingList
 *
 * @package App
 * @property integer id
 * @property string name
 * @property string description
 * @property string code
 * @property integer setting_list_object_id
 * @property integer data_type_id
 * @property bool multiple
 * @property bool active
 * @property mixed sort
 * @property mixed created_by
 * @property mixed modified_by
 * @property mixed created_at
 * @property mixed updated_at
 * @property mixed deleted_at
 * @property-read \App\User $author
 * @property-read \App\DataType $dataType
 * @property-read \App\User $editor
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\SettingListValue[] $options
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\SettingListRelation[] $relations
 * @property-read \App\SettingListObject $settingListObject
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SettingList active()
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\SettingList onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\SettingList withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\SettingList withoutTrashed()
 * @mixin \Eloquent
 */
class SettingList extends Model
{
    use SoftDeletes;
    /**
     * Атрибуты, которые должны быть преобразованы в даты.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('active', '=', true);
    }

    public function author()
    {
        return $this->hasOne('App\User', 'id', 'created_by');
    }

    public function editor()
    {
        return $this->hasOne('App\User', 'id', 'modified_by');
    }

    public function settingListObject()
    {
        return $this->hasOne('App\SettingListObject', 'id', 'setting_list_object_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function dataType()
    {
        return $this->hasOne('App\DataType', 'id', 'data_type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function options()
    {
        return $this->hasMany('App\SettingListValue', 'setting_list_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function relations()
    {
        return $this
            ->hasMany('App\SettingListRelation', 'setting_list_id', 'id')
            ->with('elements');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function values()
    {
        /**
         * Получить связанную модель (которая отвечает за значения настраиваемого параметра для конкретного объекта настройки)
         */
        $model = SettingListObject::find($this->setting_list_object_id)['model'];
        return $this->hasMany($model, 'setting_list_id', 'id');
    }
}
