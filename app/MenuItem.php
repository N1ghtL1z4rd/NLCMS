<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class MenuItem
 *
 * @package App
 * @property integer id
 * @property string name
 * @property string description
 * @property string icon
 * @property integer menu_id
 * @property integer parent_id
 * @property integer section_id
 * @property boolean active
 * @property boolean system
 * @property integer sort
 * @property integer created_by
 * @property integer modified_by
 * @property mixed created_at
 * @property mixed updated_at
 * @property mixed deleted_at
 * @property-read \App\Menu $menu
 * @property-read \App\Section $section
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MenuItem active()
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\MenuItem onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\MenuItem withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\MenuItem withoutTrashed()
 * @mixin \Eloquent
 */
class MenuItem extends Model
{
    use SoftDeletes;

    /**
     * Атрибуты, которые должны быть преобразованы в даты.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('active', '=', true);
    }

    public function section()
    {
        return $this->hasOne('App\Section', 'id', 'section_id')
            ->active()
            ->with('accessByGroup');
    }

    public function menu()
    {
        return $this->belongsTo('App\Menu');
    }
}
