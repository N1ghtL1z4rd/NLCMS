<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserSetting
 *
 * @package App
 * @property integer id
 * @property integer user_id
 * @property integer setting_list_id
 * @property mixed value
 * @property mixed created_at
 * @property mixed updated_at
 * @mixin \Eloquent
 */
class UserSetting extends Model
{
    /**
     * Атрибуты, которые должны быть преобразованы в даты.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'setting_list_id',
        'value',
    ];
}
