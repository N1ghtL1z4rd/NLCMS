<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SettingListValue
 *
 * @package App
 * @property integer id
 * @property integer setting_list_id
 * @property string name
 * @property string code
 * @property boolean active
 * @property boolean system
 * @property integer sort
 * @property integer created_by
 * @property integer modified_by
 * @property mixed created_at
 * @property mixed updated_at
 * @property mixed delete_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SettingListValue active()
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\SettingListValue onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\SettingListValue withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\SettingListValue withoutTrashed()
 * @mixin \Eloquent
 */
class SettingListValue extends Model
{
    use SoftDeletes;

    /**
     * Атрибуты, которые должны быть преобразованы в даты.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'setting_list_id',
        'name',
        'code',
        'sort'
    ];

    /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('active', '=', true);
    }
}
