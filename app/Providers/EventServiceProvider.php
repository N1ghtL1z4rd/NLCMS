<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        /**
         * Обработка событий для пользователей
         */
        'App\Events\UserUpdate' => [
            'App\Listeners\UserUpdateHandler',
        ],
        'App\Events\UserDestroy' => [
            'App\Listeners\UserDestroyHandler',
        ],
        /**
         * Обработка событий для групп пользователей
         */
        'App\Events\GroupStore' => [
            'App\Listeners\GroupStoreHandler',
        ],
        'App\Events\GroupUpdate' => [
            'App\Listeners\GroupUpdateHandler',
        ],
        'App\Events\GroupDestroy' => [
            'App\Listeners\GroupDestroyHandler',
        ],
        /**
         * Обработка событий для настраиваемых параметров
         */
        'App\Events\SettingListDestroy' => [
            'App\Listeners\SettingListDestroyHandler',
        ],
        /**
         * Обработка событий для настраиваемых параметров
         */
        'App\Events\SectionDestroy' => [
            'App\Listeners\SectionDestroyHandler',
        ],
        /**
         * Обработка событий для свойств сущности
         */
        'App\Events\PropertyDestroy' => [
            'App\Listeners\PropertyDestroyHandler',
        ],
        /**
         * Обработка событий для элементов сущностей
         */
        'App\Events\EntityElementStore' => [
            'App\Listeners\EntityElementStoreHandler',
        ],
        'App\Events\EntityElementUpdate' => [
            'App\Listeners\EntityElementUpdateHandler',
        ],
        'App\Events\EntityElementDestroy' => [
            'App\Listeners\EntityElementDestroyHandler',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }
}
