<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PropertyValue
 *
 * @package App
 * @property integer id
 * @property integer property_id
 * @property string name
 * @property string code
 * @property boolean active
 * @property boolean system
 * @property integer sort
 * @property integer created_by
 * @property integer modified_by
 * @property mixed created_at
 * @property mixed updated_at
 * @property mixed delete_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\PropertyValue onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\PropertyValue withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\PropertyValue withoutTrashed()
 * @mixin \Eloquent
 */
class PropertyValue extends Model
{
    use SoftDeletes;
    /**
     * Атрибуты, которые должны быть преобразованы в даты.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'property_id', 'name', 'code', 'sort', 'created_by', 'modified_by'
    ];
    //
}
