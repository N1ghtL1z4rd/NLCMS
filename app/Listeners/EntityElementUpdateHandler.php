<?php

namespace App\Listeners;

use App\Entity;
use App\EntityElement;
use App\Events\EntityElementUpdate;
use App\JournalElement;
use App\JournalOperationType;

class EntityElementUpdateHandler
{
    /**
     * Обработчик события
     *
     * @param EntityElementUpdate $event
     * @return void
     */
    public function handle(EntityElementUpdate $event)
    {
        if (Entity::where(['id' => $event->element->entity_id, 'logging' => true])->count() > 0)
            JournalElement::create([
                'operation_type_id' => JournalOperationType::EDIT,
                'element_id' => $event->element->id,
                'condition' => EntityElement::with('preview_img','detail_img','properties')->find($event->element->id)->toJson(),
                /**
                 * Инициатор записи в журнал (тот кто изменил элемент)
                 */
                'created_by' => $event->element->modified_by
            ]);
    }
}
