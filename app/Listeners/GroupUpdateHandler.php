<?php

namespace App\Listeners;

use App\AccessObjectType;
use App\Events\GroupUpdate;
use App\MenuItem;
use Illuminate\Support\Facades\Cache;

class GroupUpdateHandler
{
    /**
     * Обработчик события
     *
     * @param GroupUpdate $event
     * @return bool
     */
    public function handle(GroupUpdate $event)
    {
        /**
         * Получить доступы для группы (ключами будут id разделов)
         */
        $access = $event->group
            ->access()
            ->where('object_type_id', AccessObjectType::SECTION)
            ->get(['object_id', 'object_type_id'])
            ->keyBy('object_id')
            ->toArray();
        if (empty($access))
            return true;
        /**
         * Получить записи меню, к которым относятся разделы
         */
        $sectionIds = array_keys($access);
        $menuItems = MenuItem::with('menu')
            ->whereIn('section_id', $sectionIds)
            ->get();
        if (empty($menuItems))
            return true;
        /**
         * Поиск уникальных меню (для записей меню)
         */
        $menus = $menuItems->keyBy('menu_id')->toArray();
        if (empty($menus))
            return true;
        /**
         * Поиск пользователей группы
         */
        $users = $event->group->users()->get()->toArray();
        if (empty($users))
            return true;
        foreach ($users as $user) {
            foreach ($menus as $menu) {
                if (!empty($menu['menu'])) {
                    $cacheKey = $menu['menu']['code'] . '_MENU_' . $user['user_id'];
                    if (Cache::has($cacheKey))
                        Cache::forget($cacheKey);
                }
            }
        }
    }
}
