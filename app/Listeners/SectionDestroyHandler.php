<?php

namespace App\Listeners;

use App\Events\SectionDestroy;
use Illuminate\Support\Facades\Auth;

class SectionDestroyHandler
{
    /**
     * Handle the event.
     *
     * @param  SectionDestroy $event
     * @return void
     */
    public function handle(SectionDestroy $event)
    {
        /**
         * Обновление (кто удалил)
         */
        $event->section->modified_by = Auth::id();
        $event->section->save();
        /**
         * Удалить пункты меню связанные с разделом
         */
        $event->section->menuItems()->delete();
        /**
         * Удаление доступов для группы из таблицы access
         */
        $event->section->access()->delete();
    }
}
