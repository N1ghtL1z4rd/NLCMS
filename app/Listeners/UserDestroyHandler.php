<?php

namespace App\Listeners;

use App\Events\UserDestroy;
use App\Menu;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class UserDestroyHandler
{
    /**
     * Handle the event.
     *
     * @param  UserDestroy $event
     * @return void
     */
    public function handle(UserDestroy $event)
    {
        /**
         * Удаление кеша меню пользователя после обновления пользователя
         */
        $menus = Menu::all()->keyBy('id');
        foreach ($menus as $menu) {
            $cacheKey = $menu['code'] . '_MENU_' . $event->user->id;
            if (Cache::has($cacheKey))
                Cache::forget($cacheKey);
        }
        /**
         * Обновление пользователя (кто удалил)
         */
        $event->user->modified_by = Auth::id();
        $event->user->save();
        /**
         * Удаление связей пользователя с группами
         */
        $event->user->groups()->delete();
        /**
         * Удаление значений настраиваемых параметров пользователя
         */
        $event->user->settings()->delete();
    }
}
