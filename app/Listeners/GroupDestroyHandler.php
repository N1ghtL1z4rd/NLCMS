<?php

namespace App\Listeners;

use App\AccessObjectType;
use App\Events\GroupDestroy;
use App\MenuItem;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class GroupDestroyHandler
{
    /**
     * Handle the event.
     *
     * @param  GroupDestroy $event
     * @return bool
     */
    public function handle(GroupDestroy $event)
    {
        /**
         * Получить доступы для группы (ключами будут id разделов)
         */
        $access = $event->group
            ->access()
            ->where('object_type_id', AccessObjectType::SECTION)
            ->get(['object_id', 'object_type_id'])
            ->keyBy('object_id')
            ->toArray();
        $users = $event->group->users();
        if (!empty($access)) {
            /**
             * Получить записи меню, к которым относятся разделы
             */
            $sectionIds = array_keys($access);
            $menuItems = MenuItem::with('menu')
                ->whereIn('section_id', $sectionIds)
                ->get();
            if (!empty($menuItems)) {
                /**
                 * Поиск уникальных меню (для записей меню)
                 */
                $menus = $menuItems->keyBy('menu_id')->toArray();
                if (!empty($menus)) {
                    /**
                     * Поиск пользователей группы
                     */
                    $users = $event->group->users();
                    if (empty($users->get()->toArray()))
                        return true;
                    foreach ($users->get()->toArray() as $user) {
                        foreach ($menus as $menu) {
                            if (!empty($menu['menu'])) {
                                $cacheKey = $menu['menu']['code'] . '_MENU_' . $user['user_id'];
                                if (Cache::has($cacheKey))
                                    Cache::forget($cacheKey);
                            }
                        }
                    }
                }
            }
        }
        /**
         * Обновление группы (кто удалил)
         */
        $event->group->modified_by = Auth::id();
        $event->group->save();
        /**
         * Удаление связей группы с пользователями
         */
        $users->delete();
        /**
         * Удаление доступов для группы из таблицы access
         */
        $event->group->access()->delete();
    }
}
