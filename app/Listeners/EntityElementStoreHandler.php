<?php

namespace App\Listeners;

use App\Entity;
use App\EntityElement;
use App\Events\EntityElementStore;
use App\JournalElement;
use App\JournalOperationType;

class EntityElementStoreHandler
{
    /**
     * Обработчик события
     *
     * @param EntityElementStore $event
     * @return void
     */
    public function handle(EntityElementStore $event)
    {
        if (Entity::where(['id' => $event->element->entity_id, 'logging' => true])->count() > 0)
            JournalElement::create([
                'operation_type_id' => JournalOperationType::ADD,
                'element_id' => $event->element->id,
                'condition' => EntityElement::with('preview_img','detail_img','properties')->find($event->element->id)->toJson(),
                /**
                 * Инициатор записи в журнал (тот кто создал элемент)
                 */
                'created_by' => $event->element->created_by
            ]);
    }
}
