<?php

namespace App\Listeners;

use App\Events\UserUpdate;
use App\Menu;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Cache;

class UserUpdateHandler
{
    /**
     * Handle the event.
     *
     * @param  UserUpdate $event
     * @return void
     */
    public function handle(UserUpdate $event)
    {
        /**
         * Удаление кеша меню пользователя после обновления пользователя
         */
        $menus = Menu::all()->keyBy('id');
        foreach ($menus as $menu) {
            $cacheKey = $menu['code'] . '_MENU_' . $event->user->id;
            if (Cache::has($cacheKey))
                Cache::forget($cacheKey);
        }
    }
}
