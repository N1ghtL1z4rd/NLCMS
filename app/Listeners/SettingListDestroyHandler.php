<?php

namespace App\Listeners;

use App\Events\SettingListDestroy;
use Illuminate\Support\Facades\Auth;

class SettingListDestroyHandler
{
    /**
     * Handle the event.
     *
     * @param  SettingListDestroy $event
     * @return void
     */
    public function handle(SettingListDestroy $event)
    {
        /**
         * Обновление настраиваемого параметра (кто удалил)
         */
        $event->setting->modified_by = Auth::id();
        $event->setting->save();
        /**
         * Удалить все значения настраиваемого параметра для типа "Список"
         */
        if ($event->setting->options()->get()->isNotEmpty())
            $event->setting->options()->delete();
        /**
         * Удалить все значения настраиваемого параметра для типа "Привязка к элементам сущностей"
         */
        if ($event->setting->relations()->get()->isNotEmpty())
            $event->setting->relations()->delete();
        /**
         * Удалить значения настраиваемого параметра у объектов настройки
         */
        if ($event->setting->values()->get()->isNotEmpty())
            $event->setting->values()->delete();
    }
}
