<?php

namespace App\Listeners;

use App\Entity;
use App\EntityElement;
use App\Events\EntityElementDestroy;
use App\JournalElement;
use App\JournalOperationType;
use Illuminate\Support\Facades\Auth;

class EntityElementDestroyHandler
{
    /**
     * Обработчик события
     *
     * @param EntityElementDestroy $event
     * @return void
     */
    public function handle(EntityElementDestroy $event)
    {
        /**
         * Обновление элемента (кто удалил)
         */
        $event->element->modified_by = Auth::id();
        $event->element->save();
        /**
         * Удаление связанных значений свойств элемента
         */
        $event->element->properties()->delete();
        /**
         * Запись в журнал
         */
        if (Entity::where(['id' => $event->element->entity_id, 'logging' => true])->count() > 0)
            JournalElement::create([
                'operation_type_id' => JournalOperationType::DELETE,
                'element_id' => $event->element->id,
                'condition' => EntityElement::with('preview_img', 'detail_img', 'properties_trashed')->withTrashed()->find($event->element->id)->toJson(),
                /**
                 * Инициатор записи в журнал (тот кто удалил элемент)
                 */
                'created_by' => $event->element->modified_by
            ]);
    }
}
