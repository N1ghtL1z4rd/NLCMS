<?php

namespace App\Listeners;

use App\Events\PropertyDestroy;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;

class PropertyDestroyHandler
{
    /**
     * Handle the event.
     *
     * @param  PropertyDestroy $event
     * @return void
     */
    public function handle(PropertyDestroy $event)
    {
        /**
         * Обновление (кто удалил)
         */
        $event->property->modified_by = Auth::id();
        $event->property->save();
        /**
         * Удалить все значения свойства для типа "Список"
         */
        $event->property->options()->delete();
        /**
         * Удалить все значения свойства для типа "Привязка к элементам сущностей"
         */
        $event->property->relations()->delete();
        /**
         * Удалить значения свойства для элементов сущности
         */
        $event->property->values()->delete();
    }
}
