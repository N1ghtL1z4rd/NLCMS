<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ElementProperty
 *
 * @package App
 * @property integer id
 * @property integer element_id
 * @property integer property_id
 * @property string value
 * @property integer sort
 * @property mixed created_at
 * @property mixed updated_at
 * @property mixed deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\ElementProperty onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\ElementProperty withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\ElementProperty withoutTrashed()
 * @mixin \Eloquent
 */

class ElementProperty extends Model
{
    use SoftDeletes;

    /**
     * Атрибуты, которые должны быть преобразованы в даты.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * Атрибуты, для которых разрешено массовое назначение.
     *
     * @var array
     */
    protected $fillable = [
        'element_id',
        'property_id',
        'value',
        'sort'
    ];

    public function property()
    {
        return $this->hasOne('App\Property', 'id', 'property_id');
    }
}
