<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Group
 *
 * @package App
 * @property integer id
 * @property string name
 * @property string description
 * @property string code
 * @property boolean active
 * @property boolean system
 * @property integer sort
 * @property integer created_by
 * @property integer modified_by
 * @property mixed created_at
 * @property mixed updated_at
 * @property mixed deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Access[] $access
 * @property-read \App\User $author
 * @property-read \App\User $editor
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\UserGroup[] $users
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Group active()
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Group onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Group withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Group withoutTrashed()
 * @mixin \Eloquent
 */
class Group extends Model
{
    use SoftDeletes;

    /**
     * Атрибуты, которые должны быть преобразованы в даты.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('active', '=', true);
    }

    public function author()
    {
        return $this->hasOne('App\User', 'id', 'created_by');
    }

    public function editor()
    {
        return $this->hasOne('App\User', 'id', 'modified_by');
    }

    public function users()
    {
        return $this->hasMany('App\UserGroup', 'group_id', 'id');
    }

    public function access()
    {
        return $this->hasMany('App\Access', 'subject_id', 'id')
            ->where('subject_type_id', AccessSubjectType::GROUP);
    }
}
