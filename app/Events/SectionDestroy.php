<?php

namespace App\Events;

use App\Section;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class SectionDestroy
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $section;

    /**
     * Создать новый экземпляр события.
     *
     * @param Section $section
     */
    public function __construct(Section $section)
    {
        $this->section = $section;
    }
}
