<?php

namespace App\Events;

use App\Group;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class GroupUpdate
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $group;

    /**
     * Создать новый экземпляр события.
     *
     * @param  Group $group
     * @return void
     */
    public function __construct(Group $group)
    {
        $this->group = $group;
    }
}
