<?php

namespace App\Events;

use App\EntityElement;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class EntityElementUpdate
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $element;

    /**
     * Создать новый экземпляр события.
     *
     * @param  EntityElement $element
     * @return void
     */
    public function __construct(EntityElement $element)
    {
        $this->element = $element;
    }
}
