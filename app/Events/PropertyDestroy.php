<?php

namespace App\Events;

use App\Property;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class PropertyDestroy
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $property;

    /**
     * Создать новый экземпляр события.
     *
     * @param Property $property
     */
    public function __construct(Property $property)
    {
        $this->property = $property;
    }
}
