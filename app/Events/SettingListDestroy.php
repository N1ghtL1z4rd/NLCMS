<?php

namespace App\Events;

use App\SettingList;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class SettingListDestroy
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $setting;

    /**
     * Создать новый экземпляр события.
     *
     * @param SettingList $setting
     */
    public function __construct(SettingList $setting)
    {
        $this->setting = $setting;
    }
}
