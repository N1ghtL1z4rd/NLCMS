<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class EntityType
 *
 * @package App
 * @property integer id
 * @property string name
 * @property string description
 * @property string code
 * @property boolean active
 * @property boolean system
 * @property integer sort
 * @property integer created_by
 * @property integer modified_by
 * @property mixed created_at
 * @property mixed updated_at
 * @property mixed delete_at
 * @property-read \App\User $author
 * @property-read \App\User $editor
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity[] $entities
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EntityType active()
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\EntityType onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\EntityType withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\EntityType withoutTrashed()
 * @mixin \Eloquent
 */

class EntityType extends Model
{
    use SoftDeletes;

    /**
     * Атрибуты, которые должны быть преобразованы в даты.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('active', '=', true);
    }

    public function author()
    {
        return $this->hasOne('App\User', 'id', 'created_by');
    }

    public function editor()
    {
        return $this->hasOne('App\User', 'id', 'modified_by');
    }

    public function entities()
    {
        return $this->hasMany('App\Entity', 'entity_type_id', 'id');
    }
}
