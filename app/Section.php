<?php

namespace App;

use App\UserGroup;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

/**
 * Class Section
 *
 * @package App
 * @property string id
 * @property string name
 * @property string description
 * @property string url
 * @property boolean active
 * @property boolean system
 * @property integer sort
 * @property integer created_by
 * @property integer modified_by
 * @property mixed created_at
 * @property mixed updated_at
 * @property mixed delete_at
 * @property-read \App\Access $access
 * @property-read \App\User $author
 * @property-read \App\User $editor
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\MenuItem[] $menuItems
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Section active()
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Section onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Section withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Section withoutTrashed()
 * @mixin \Eloquent
 */
class Section extends Model
{
    use SoftDeletes;

    /**
     * Атрибуты, которые должны быть преобразованы в даты.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('active', '=', true);
    }

    public function author()
    {
        return $this->hasOne('App\User', 'id', 'created_by');
    }

    public function editor()
    {
        return $this->hasOne('App\User', 'id', 'modified_by');
    }

    public function accessByGroup()
    {
        /**
         * Вытащить все записи о группах пользователя с самой группой
         */
        $uGroups = UserGroup::where('user_id', Auth::id())->with('group')->get()->keyBy('group_id')->toArray();
        $groupIds = [];
        /**
         * Проверить наличие активной группы (группы могут быть деактивированы)
         */
        foreach ($uGroups as $id => $uGroup) {
            if (!empty($uGroup['group']))
                $groupIds[] = $id;
        }
        /**
         * Если нет активных групп, вернуть null
         */
        if (empty($groupIds))
            return null;
        /**
         * Если есть активные группы, вернуть доступ (если есть)
         */
        return $this->hasOne('App\Access', 'object_id', 'id')
            ->where('object_type_id', AccessObjectType::SECTION)
            ->where('subject_type_id', AccessSubjectType::GROUP)
            ->whereIn('subject_id', $groupIds);
    }

    public function menuItems()
    {
        return $this->hasMany('App\MenuItem', 'section_id', 'id');
    }

    public function access()
    {
        return $this->hasOne('App\Access', 'object_id', 'id')
            ->where('object_type_id', AccessObjectType::SECTION);
    }
}
