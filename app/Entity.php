<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Entity
 *
 * @package App
 * @property integer id
 * @property string name
 * @property string description
 * @property string code
 * @property integer entity_type_id
 * @property boolean active
 * @property boolean system
 * @property boolean logging
 * @property integer sort
 * @property integer created_by
 * @property integer modified_by
 * @property mixed created_at
 * @property mixed updated_at
 * @property mixed delete_at
 * @property-read \App\User $author
 * @property-read \App\User $editor
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\EntityElement[] $elements
 * @property-read \App\EntityType $entityType
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity active()
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Entity onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Entity withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Entity withoutTrashed()
 * @mixin \Eloquent
 */
class Entity extends Model
{
    use SoftDeletes;

    /**
     * Атрибуты, которые должны быть преобразованы в даты.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * Определение вызова глобального условия для выборки данных
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();
        /**
         * метод, отрабатывающий при обращении к классу
         */
        static::created(function ($item) {
        });
    }

    /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('active', '=', true);
    }

    public function author()
    {
        return $this->hasOne('App\User', 'id', 'created_by');
    }

    public function editor()
    {
        return $this->hasOne('App\User', 'id', 'modified_by');
    }

    public function entityType()
    {
        return $this->belongsTo('App\EntityType', 'entity_type_id', 'id');
    }

    public function elements()
    {
        return $this->hasMany('App\EntityElement', 'entity_id', 'id');
    }
}
