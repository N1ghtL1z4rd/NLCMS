<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Property
 *
 * @package App
 * @property integer id
 * @property string name
 * @property string description
 * @property string code
 * @property integer entity_id
 * @property integer data_type_id
 * @property boolean multiple
 * @property boolean required
 * @property boolean active
 * @property boolean system
 * @property integer sort
 * @property integer created_by
 * @property integer modified_by
 * @property mixed created_at
 * @property mixed updated_at
 * @property mixed delete_at
 * @property-read \App\User $author
 * @property-read \App\DataType $dataType
 * @property-read \App\User $editor
 * @property-read \App\Entity $entity
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\PropertyValue[] $options
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\PropertyRelation[] $relations
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ElementProperty[] $values
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Property active()
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Property onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Property withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Property withoutTrashed()
 * @mixin \Eloquent
 */
class Property extends Model
{
    use SoftDeletes;
    /**
     * Атрибуты, которые должны быть преобразованы в даты.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('active', '=', true);
    }

    public function author()
    {
        return $this->hasOne('App\User', 'id', 'created_by');
    }

    public function editor()
    {
        return $this->hasOne('App\User', 'id', 'modified_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function dataType()
    {
        return $this->hasOne('App\DataType', 'id', 'data_type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function entity()
    {
        return $this->hasOne('App\Entity', 'id', 'entity_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function options()
    {
        return $this->hasMany('App\PropertyValue', 'property_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function relations()
    {
        return $this
            ->hasMany('App\PropertyRelation', 'property_id', 'id')
            ->with('elements');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function values()
    {
        return $this->hasMany('App\ElementProperty', 'property_id', 'id');
    }
}
