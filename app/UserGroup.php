<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserGroup
 *
 * @package App
 * @property integer user_id
 * @property integer group_id
 * @property-read \App\Group $group
 * @property-read \App\User $user
 * @mixin \Eloquent
 */
class UserGroup extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id')
            ->active();
    }

    public function group()
    {
        return $this->belongsTo('App\Group', 'group_id', 'id')
            ->active();
    }
}
