<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class DataType
 *
 * @package App
 * @property integer id
 * @property string name
 * @property string description
 * @property string code
 * @property boolean active
 * @property boolean system
 * @property integer sort
 * @property integer created_by
 * @property integer modified_by
 * @property mixed created_at
 * @property mixed updated_at
 * @property mixed deleted_at
 * @property-read \App\User $author
 * @property-read \App\User $editor
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DataType active()
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\DataType onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\DataType withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\DataType withoutTrashed()
 * @mixin \Eloquent
 */
class DataType extends Model
{
    const STRING = 1;
    const INTEGER = 2;
    const TEXT = 3;
    const CHECKBOX = 4;
    const DATE = 5;
    const DATETIME = 6;
    const SELECT = 7;
    const ENTITY = 8;
    const FILE = 9;
    const USER = 10;

    use SoftDeletes;

    /**
     * Атрибуты, которые должны быть преобразованы в даты.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('active', '=', true);
    }

    public function author()
    {
        return $this->hasOne('App\User', 'id', 'created_by');
    }

    public function editor()
    {
        return $this->hasOne('App\User', 'id', 'modified_by');
    }
}
