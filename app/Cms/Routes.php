<?php

namespace App\Cms;

use Illuminate\Support\Facades\Route;

class Routes
{
    /**
     * Роуты административной панели
     * @return void
     */
    public static function all()
    {
        Route::prefix('admin')->group(function () {
            /**
             * 1. Главная
             */
            Route::get('/', 'Admin\MainController@index')->name('admin');

            /***********************************************************************************************************
             *
             *
             * Контент
             *
             *
             **********************************************************************************************************/

            /**
             * Типы сущностей
             */
            Route::prefix('entity-types')->group(function () {
                Route::get('/', 'Admin\EntityTypesController@index')->name('admin-entity-types.index');
                Route::post('/', 'Admin\EntityTypesController@ajax')->name('admin-entity-types.ajax');
                Route::get('create', 'Admin\EntityTypesController@create')->name('admin-entity-types.create');
                Route::post('store', 'Admin\EntityTypesController@store')->name('admin-entity-types.store');
                Route::get('{entity_type}', 'Admin\EntityTypesController@show')->name('admin-entity-types.show');
                Route::get('{entity_type}/edit', 'Admin\EntityTypesController@edit')->name('admin-entity-types.edit');
                Route::patch('{entity_type}', 'Admin\EntityTypesController@update')->name('admin-entity-types.update');
                Route::delete('/', 'Admin\EntityTypesController@destroy')->name('admin-entity-types.destroy');
            });
            /**
             * Сущности
             */
            Route::prefix('entities')->group(function () {
                Route::post('{entity_type}', 'Admin\EntitiesController@ajax')->where('entity_type', '[0-9]+')->name('admin-entities.ajax');
                Route::get('create/{entity_type}', 'Admin\EntitiesController@create')->name('admin-entities.create');
                Route::post('store', 'Admin\EntitiesController@store')->where('type', '[A-Za-z]+')->name('admin-entities.store');
                Route::get('{entity}', 'Admin\EntitiesController@show')->name('admin-entities.show');
                Route::get('{entity}/edit', 'Admin\EntitiesController@edit')->name('admin-entities.edit');
                Route::patch('{entity}', 'Admin\EntitiesController@update')->name('admin-entities.update');
                Route::delete('/', 'Admin\EntitiesController@destroy')->name('admin-entities.destroy');
            });
            /**
             * Элементы
             */
            Route::prefix('entity-elements')->group(function () {
                Route::post('{entity}/columns', 'Admin\EntityElementsController@ajaxColumns')->where('entity', '[0-9]+')->name('admin-entity-elements.ajaxColumns');
                Route::post('{entity}/data', 'Admin\EntityElementsController@ajaxData')->where('entity', '[0-9]+')->name('admin-entity-elements.ajaxData');
                Route::get('create/{entity}', 'Admin\EntityElementsController@create')->name('admin-entity-elements.create');
                Route::post('store', 'Admin\EntityElementsController@store')->where('type', '[A-Za-z]+')->name('admin-entity-elements.store');
                Route::get('{entity_element}/edit', 'Admin\EntityElementsController@edit')->name('admin-entity-elements.edit');
                Route::patch('{entity_element}', 'Admin\EntityElementsController@update')->name('admin-entity-elements.update');
                Route::delete('/', 'Admin\EntityElementsController@destroy')->name('admin-entity-elements.destroy');
            });
            /**
             * Свойства сущностей
             */
            Route::prefix('properties')->group(function () {
                Route::get('create/{entity}', 'Admin\PropertiesController@create')->name('admin-properties.create');
                Route::post('store', 'Admin\PropertiesController@store')->name('admin-properties.store');
                Route::get('{property}/edit', 'Admin\PropertiesController@edit')->name('admin-properties.edit');
                Route::patch('{property}', 'Admin\PropertiesController@update')->name('admin-properties.update');
                Route::delete('{property}', 'Admin\PropertiesController@destroy')->name('admin-properties.destroy');
            });

            /***********************************************************************************************************
             *
             *
             * Настройки
             *
             *
             **********************************************************************************************************/

            /**
             * Настройки сайта site-settings
             */
            Route::prefix('site-settings')->group(function () {
                Route::get('/', 'Admin\SiteSettingsController@show')->name('admin-site-settings.show');
                Route::get('edit', 'Admin\SiteSettingsController@edit')->name('admin-site-settings.edit');
                Route::patch('/', 'Admin\SiteSettingsController@update')->name('admin-site-settings.update');
            });
            /**
             * Разделы
             */
            Route::prefix('sections')->group(function () {
                Route::get('/', 'Admin\SectionsController@index')->name('admin-sections.index');
                Route::post('/', 'Admin\SectionsController@ajax')->name('admin-sections.ajax');
                Route::get('create', 'Admin\SectionsController@create')->name('admin-sections.create');
                Route::post('store', 'Admin\SectionsController@store')->name('admin-sections.store');
                Route::get('{section}/edit', 'Admin\SectionsController@edit')->name('admin-sections.edit');
                Route::patch('{section}', 'Admin\SectionsController@update')->name('admin-sections.update');
                Route::delete('/', 'Admin\SectionsController@destroy')->name('admin-sections.destroy');
            });

            /**
             * Пользователи
             */
            Route::prefix('users')->group(function () {
                Route::get('/', 'Admin\UsersController@index')->name('admin-users.index');
                Route::post('/', 'Admin\UsersController@ajax')->name('admin-users.ajax');
                Route::post('{user}/login', 'Admin\UsersController@login')->name('admin-users.login');
                Route::get('create', 'Admin\UsersController@create')->name('admin-users.create');
                Route::post('store', 'Admin\UsersController@store')->name('admin-users.store');
                Route::get('{user}/edit', 'Admin\UsersController@edit')->name('admin-users.edit');
                Route::patch('{user}', 'Admin\UsersController@update')->name('admin-users.update');
                Route::delete('/', 'Admin\UsersController@destroy')->name('admin-users.destroy');
            });

            /**
             * Группы
             */
            Route::prefix('groups')->group(function () {
                Route::get('/', 'Admin\GroupsController@index')->name('admin-groups.index');
                Route::post('/', 'Admin\GroupsController@ajax')->name('admin-groups.ajax');
                Route::get('create', 'Admin\GroupsController@create')->name('admin-groups.create');
                Route::post('store', 'Admin\GroupsController@store')->name('admin-groups.store');
                Route::get('{group}/edit', 'Admin\GroupsController@edit')->name('admin-groups.edit');
                Route::patch('{group}', 'Admin\GroupsController@update')->name('admin-groups.update');
                Route::delete('/', 'Admin\GroupsController@destroy')->name('admin-groups.destroy');
            });
            /**
             * Настраиваемые параметры
             */
            Route::prefix('setting-lists')->group(function () {
                Route::get('/', 'Admin\SettingListsController@index')->name('admin-setting-lists.index');
                Route::post('/', 'Admin\SettingListsController@ajax')->name('admin-setting-lists.ajax');
                Route::get('create', 'Admin\SettingListsController@create')->name('admin-setting-lists.create');
                Route::post('store', 'Admin\SettingListsController@store')->name('admin-setting-lists.store');
                Route::get('{setting_list}/edit', 'Admin\SettingListsController@edit')->name('admin-setting-lists.edit');
                Route::patch('{setting_list}', 'Admin\SettingListsController@update')->name('admin-setting-lists.update');
                Route::delete('/', 'Admin\SettingListsController@destroy')->name('admin-setting-lists.destroy');
            });
            /**
             * Журнал
             */
            Route::prefix('journal')->group(function () {
                Route::get('/', 'Admin\JournalController@index')->name('admin-journal.index');
                Route::post('/', 'Admin\JournalController@ajax')->name('admin-journal.ajax');
                Route::get('{journal_element}', 'Admin\JournalController@show')->name('admin-journal.show');
            });
            /**
             * Информация
             */
            Route::get('php-info', 'Admin\MainController@phpInfo');
            /**
             * Таблицы БД
             */
            Route::prefix('tables')->group(function () {
                Route::get('/', 'Admin\TablesController@index')->name('admin-tables.index');
                Route::get('{table}', 'Admin\TablesController@show')->name('admin-tables.show');
            });
            /**
             * Резервные копии БД
             */
            Route::prefix('backups')->group(function () {
                Route::get('/', 'Admin\BackupsController@index')->name('admin-backups.index');
                Route::get('create', 'Admin\BackupsController@create')->name('admin-backups.create');
                Route::get('/{backup}', 'Admin\BackupsController@show')->name('admin-backups.show');
                Route::delete('{backup}', 'Admin\BackupsController@destroy')->name('admin-backups.destroy');
            });
            /**
             * Команды artisan
             */
            Route::prefix('commands')->group(function () {
                Route::get('/', 'Admin\CommandsController@index')->name('admin-commands.index');
                Route::post('/', 'Admin\CommandsController@run')->name('admin-commands.run');
            });
        });
    }
}
