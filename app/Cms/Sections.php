<?php

namespace App\Cms;

use App\Section;
use Illuminate\Support\Facades\Request;


class Sections
{
    /**
     * Формирование строки для поиска раздела по роуту
     *
     * Роуты с ресурсами должны быть поименованы и объединены в группы роутов (пример в @property \App\Cms\Routes),
     * так как по префиксу проверяется доступ к действию:
     *  - GET: admin/sections/create проверяется по префиксу admin/sections, действие create -
     *      доступ к нему есть, если для раздела установлено разрешение для группы пользователя на "Редактирование"
     *  - DELETE: admin/sections/{section} проверяется по префиксу admin/sections, действие destroy -
     *      доступ к нему есть, если для раздела установлено разрешение для группы пользователя "Удаление"
     *
     * - Если роут не поименован, то доступ проверяется по getStaticPrefix() на "Просмотр"
     * - Если роут поименован, но в наименовании отсутствует action[1] (например: ->name('test')), то
     *      доступ проверяется по getStaticPrefix() на "Просмотр"
     * - Если роут поименован и в наименовании есть action[1] (например: ->name('test.create')), то
     *      доступ проверяется по $routeGroup
     *
     * @todo надо продумать условие для тех роутов, которые не сгруппированы и поименованы
     */
    public static function getCurrentSection()
    {
        $routeGroup = Request::route()->getAction()['prefix'];
        $routeName = Request::route()->getName();

        $urlCheck = Request::route()->getCompiled()->getStaticPrefix();
        if (!empty($routeName)) {
            $action = explode('.', $routeName);
            if (!empty($action[1]))
                if (in_array($action[1], ['ajax', 'create', 'store', 'edit', 'update', 'destroy']))
                    $urlCheck = '/' . $routeGroup;
        }
        return Section::where('url', $urlCheck)->active()->first();
    }
}
