<?php

namespace App\Cms;

use App\UserGroup;
use App\Access as AccessModel;
use App\AccessType;
use App\AccessObjectType;
use App\AccessSubjectType;
use Auth;
use Illuminate\Support\Facades\Request;


class Access
{
    /**
     * Проверка на администратора
     */
    public static function isAdmin()
    {
        /**
         * Если это пользователь с id = 1 (администратор сайта по умолчанию), то доступ разрешен
         */
        if (Auth::id() === 1)
            return true;
        /**
         * Если пользователь состоит в группе администраторов, то доступ разрешен
         */
        if (UserGroup::where('user_id', Auth::id())->where('group_id', 1)->count() > 0)
            return true;
    }

    /**
     * Первичная проверка доступов для пользователя
     * @return bool
     */
    private static function checkFirst()
    {
        /**
         * Если пользователь не авторизован, то доступ запрещен
         */
        if (!Auth::id())
            return false;
        /**
         * Проверка на администратора
         */
        if (self::isAdmin())
            return true;
    }

    /**
     * Получить атрибуты для проверки доступа
     * @return array
     */
    private static function getAttributes()
    {
        return [
            'object_type_id' => AccessObjectType::SECTION,
            'subject_type_id' => AccessSubjectType::GROUP
        ];
    }

    /**
     * Проверка доступа к разделу/странице по запрашиваемому URI
     * @param $q
     * @return bool
     */
    public static function check()
    {
        if (self::checkFirst())
            return true;
        /**
         * Получить запрашиваемый раздел
         */
        $section = \App\Cms\Sections::getCurrentSection();
        /**
         * если не найдено то 404
         */
        if (empty($section))
            abort(404, 'Не найдено');
        /**
         * Вытащить все записи о группах пользователя с самой группой
         */
        $uGroups = UserGroup::where('user_id', Auth::id())->with('group')->get()->keyBy('group_id')->toArray();
        /**
         * Если нет групп, вернуть false
         */
        if (empty($uGroups))
            return false;

        $groupIds = [];
        /**
         * Проверить наличие активной группы (группы могут быть деактивированы)
         */
        foreach ($uGroups as $id => $uGroup) {
            if (!empty($uGroup['group']))
                $groupIds[] = $id;
        }

        /**
         * Если нет активных групп, вернуть false
         */
        if (empty($groupIds))
            return false;
        /**
         * Если пользователь состоит в группе (группах), то проверить доступ группы (групп) к запрашиваемому разделу/странице
         */
        $access_type_id = AccessType::VIEW;
        if (!empty($routeName)) {
            $action = explode('.', $routeName);
            if (!empty($action[1])) {
                if (in_array($action[1], ['create', 'store', 'edit', 'update']))
                    $access_type_id = AccessType::EDIT;
                if (in_array($action[1], ['destroy']))
                    $access_type_id = AccessType::DELETE;
            }
        }
        $attributes = self::getAttributes();
        $result = AccessModel::where('access_type_id', '>=', $access_type_id)
            ->where('object_type_id', $attributes['object_type_id'])
            ->where('subject_type_id', $attributes['subject_type_id'])
            ->whereIn('subject_id', $groupIds)
            ->where('object_id', $section->id)
            ->get();
        if ($result->count() > 0)
            return true;

        return false;
    }

    /**
     * Проверка доступа к разделу/странице по типу доступа
     * @param $access
     * @return bool
     */
    public static function checkByAccess($access)
    {
        if (self::checkFirst())
            return true;
        /**
         * Получить запрашиваемый раздел
         */
        $section = \App\Cms\Sections::getCurrentSection();
        /**
         * если не найдено то 404
         */
        if (empty($section))
            return false;
        /**
         * Вытащить все записи о группах пользователя с самой группой
         */
        $uGroups = UserGroup::where('user_id', Auth::id())->with('group')->get()->keyBy('group_id')->toArray();
        /**
         * Если нет групп, вернуть false
         */
        if (empty($uGroups))
            return false;

        $groupIds = [];
        /**
         * Проверить наличие активной группы (группы могут быть деактивированы)
         */
        foreach ($uGroups as $id => $uGroup) {
            if (!empty($uGroup['group']))
                $groupIds[] = $id;
        }
        /**
         * Если нет активных групп, вернуть false
         */
        if (empty($groupIds))
            return false;
        /**
         * Если пользователь состоит в группе (группах), то проверить доступ группы (групп) к запрашиваемому  разделу по ID
         */
        $attributes = self::getAttributes();
        $result = AccessModel::where('access_type_id', '>=', $access)
            ->where('object_type_id', $attributes['object_type_id'])
            ->where('subject_type_id', $attributes['subject_type_id'])
            ->whereIn('subject_id', $groupIds)
            ->where('object_id', $section->id)
            ->get();
        if ($result->count() > 0)
            return true;

        return false;
    }
}
