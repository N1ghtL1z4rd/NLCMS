<?php

namespace App\Cms;

use App\Menu;

class Helpers
{
    /**
     * @param $code
     * @return array
     */
    public static function menu($code)
    {
        $menuList = [];
        $menu = Menu::active()
            ->where('code', $code)
            ->with('menuItems')
            ->first();
        $menu = $menu->toArray();
        $menuList['menu'] = $menu;

        $admin = false;
        if (Access::isAdmin())
            $admin = true;
        $itemsList = [];
        if (!empty($menu)) {
            foreach ($menu['menu_items'] as $menuItem) {
                if (is_null($menuItem['parent_id'])) {
                    if ((!empty($menuItem['section']) && !empty($menuItem['section']['access_by_group'])) || $admin) {
                        $itemsList[$menuItem['id']] = $menuItem;
                    }
                } else {
                    if ((!empty($menuItem['section']) && !empty($menuItem['section']['access_by_group'])) || $admin) {
                        if (empty($itemsList[$menuItem['parent_id']])) {
                            $keySearch = array_search($menuItem['parent_id'], array_column($menu['menu_items'], 'id'));
                            $itemsList[$menuItem['parent_id']] = $menu['menu_items'][$keySearch];
                        }
                        $itemsList[$menuItem['parent_id']]['sub'][$menuItem['sort']] = $menuItem;
                        ksort($itemsList[$menuItem['parent_id']]['sub']);
                    }
                }
            }
            $menuList['items'] = collect($itemsList)->sortBy('sort')->toArray();
        }

        return $menuList;
    }

    public static function bytesToHuman($bytes)
    {
        $units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB'];

        for ($i = 0; $bytes > 1024; $i++) {
            $bytes /= 1024;
        }

        return round($bytes, 2) . ' ' . $units[$i];
    }
}
