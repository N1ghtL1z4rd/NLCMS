<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class JournalOperationType
 *
 * @package App
 * @property integer id
 * @property string name
 * @property string description
 * @property string code
 * @property boolean active
 * @property boolean system
 * @property integer sort
 * @property integer created_by
 * @property integer modified_by
 * @property mixed created_at
 * @property mixed updated_at
 * @property mixed deleted_at
 * @property-read \App\User $author
 * @property-read \App\User $editor
 * @method static \Illuminate\Database\Eloquent\Builder|\App\JournalOperationType active()
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\JournalOperationType onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\JournalOperationType withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\JournalOperationType withoutTrashed()
 * @mixin \Eloquent
 */
class JournalOperationType extends Model
{
    const ADD = 1;
    const EDIT = 2;
    const DELETE = 3;

    use SoftDeletes;

    /**
     * Атрибуты, которые должны быть преобразованы в даты.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('active', '=', true);
    }

    public function author()
    {
        return $this->hasOne('App\User', 'id', 'created_by');
    }

    public function editor()
    {
        return $this->hasOne('App\User', 'id', 'modified_by');
    }
}
