<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SettingListRelation
 *
 * @package App
 * @property integer id
 * @property integer setting_list_id
 * @property integer value
 * @property mixed created_at
 * @property mixed updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\EntityElement[] $elements
 * @property-read \App\Entity $entity
 * @mixin \Eloquent
 */
class SettingListRelation extends Model
{
    /**
     * Атрибуты, которые должны быть преобразованы в даты.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'value'
    ];

    public function entity()
    {
        return $this
            ->hasOne('App\Entity', 'id', 'value');
    }

    public function elements()
    {
        return $this
            ->hasMany('App\EntityElement', 'entity_id', 'value')
            ->active();
    }
}
