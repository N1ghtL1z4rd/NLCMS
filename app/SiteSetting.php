<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SiteSetting
 *
 * @package App
 * @property integer id
 * @property integer setting_list_id
 * @property mixed value
 * @property integer modified_by
 * @property mixed created_at
 * @property mixed updated_at
 * @property-read \App\SettingList $settingList
 * @mixin \Eloquent
 */
class SiteSetting extends Model
{
    /**
     * Атрибуты, которые должны быть преобразованы в даты.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'setting_list_id',
        'value',
        'modified_by'
    ];

    public function settingList()
    {
        return $this->hasOne('App\SettingList', 'id', 'setting_list_id');
    }
}
