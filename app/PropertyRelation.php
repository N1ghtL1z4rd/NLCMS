<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PropertyRelation
 *
 * @package App
 * @property integer id
 * @property integer property_id
 * @property integer value
 * @property mixed created_at
 * @property mixed updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\EntityElement[] $elements
 * @mixin \Eloquent
 */
class PropertyRelation extends Model
{
    /**
     * Атрибуты, которые должны быть преобразованы в даты.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];


    public $incrementing = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'property_id', 'value'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function elements()
    {
        return $this->hasMany('App\EntityElement', 'entity_id', 'value')->active();
    }

}
