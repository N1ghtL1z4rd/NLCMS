<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class JournalElement
 *
 * @package App
 * @property integer id
 * @property integer operation_type_id
 * @property integer element_id
 * @property mixed condition
 * @property integer created_by
 * @property integer modified_by
 * @property mixed created_at
 * @property mixed updated_at
 * @property mixed deleted_at
 * @property-read \App\User $author
 * @property-read \App\EntityElement $element
 * @property-read \App\JournalOperationType $operation
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\JournalElement onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\JournalElement withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\JournalElement withoutTrashed()
 * @mixin \Eloquent
 */
class JournalElement extends Model
{
    use SoftDeletes;

    /**
     * Атрибуты, которые должны быть преобразованы в даты.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * Атрибуты, для которых разрешено массовое назначение.
     *
     * @var array
     */
    protected $fillable = [
        'operation_type_id',
        'element_id',
        'condition',
        'created_by',
    ];

    public function element()
    {
        return $this->belongsTo('App\EntityElement', 'element_id')
            ->with('properties')
            ->withTrashed();
    }

    public function operation()
    {
        return $this->belongsTo('App\JournalOperationType', 'operation_type_id');
    }

    public function author()
    {
        return $this->belongsTo('App\User', 'created_by');
    }
    public function editor()
    {
        return $this->belongsTo('App\User', 'id', 'modified_by');
    }
}
