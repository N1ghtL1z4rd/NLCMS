<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EntityElementStore extends FormRequest
{
    /**
     * Определить, разрешено ли пользователю выполнить этот запрос
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Правила проверки, которые применяются к запросу
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:1|max:255',
            //@todo здесь подумать над проверкой на необязательность
//            'code' => 'string|max:255',
//            'extended_id' => 'integer',
            'entity_id' => 'required|integer',
            'sort' => 'integer',
        ];
    }
}
