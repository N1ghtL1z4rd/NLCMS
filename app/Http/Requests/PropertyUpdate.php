<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PropertyUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:1|max:255',
            'code' => [
                'required',
                'max:255',
                Rule::unique('properties', 'code')
                    ->whereNull('deleted_at')
                    ->whereNot('id', $this->route()->parameter('property'))
            ],
            'entity_id' => 'required|integer',
            'data_type_id' => 'required|integer',
            'sort' => 'integer',
        ];
    }
}
