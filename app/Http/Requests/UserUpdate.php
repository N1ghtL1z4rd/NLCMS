<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserUpdate extends FormRequest
{
    /**
     * Определить, разрешено ли пользователю выполнить этот запрос
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Правила проверки, которые применяются к запросу
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route()->parameter('user')->id;
        $password = $this->request->get('password');
        $fields = [
            'name' => 'required|string|min:1|max:255',
            'email' => [
                'required',
                'max:255',
                'email',
                Rule::unique('users', 'email')
                    ->whereNull('deleted_at')
                    ->whereNot('id', $id)
            ],
            'sort' => 'integer',
        ];

        if (empty($password))
            return $fields;

        return array_merge($fields, ['password' => 'required|string|min:8|confirmed',]);
    }
}
