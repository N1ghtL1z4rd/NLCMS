<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserStore extends FormRequest
{
    /**
     * Определить, разрешено ли пользователю выполнить этот запрос
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Правила проверки, которые применяются к запросу
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:1|max:255',
            'email' => [
                'required',
                'max:255',
                'email',
                Rule::unique('users', 'email')
                    ->whereNull('deleted_at')
            ],
            'password' => 'required|string|min:8|confirmed',
            'sort' => 'integer',
        ];
    }
}
