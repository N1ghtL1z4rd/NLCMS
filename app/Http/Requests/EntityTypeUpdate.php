<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class EntityTypeUpdate extends FormRequest
{
    /**
     * Определить, разрешено ли пользователю выполнить этот запрос
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Правила проверки, которые применяются к запросу
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:1|max:255',
            'code' => [
                'required',
                'max:255',
                Rule::unique('entity_types', 'code')
                    ->whereNull('deleted_at')
                    ->whereNot('id', $this->route()->parameter('entity_type'))
            ],
            'sort' => 'integer',
        ];
    }
}
