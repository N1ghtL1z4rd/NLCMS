<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class GroupUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:1|max:255',
            'code' => [
                'required',
                'max:255',
                Rule::unique('groups', 'code')
                    ->whereNull('deleted_at')
                    ->whereNot('id', $this->route()->parameter('group'))
            ],
            'sort' => 'integer',
        ];
    }
}
