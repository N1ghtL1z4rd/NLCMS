<?php

namespace App\Http\Controllers\Admin;

use App\AccessType;
use App\Cms\Access;
use App\Entity;
use App\EntityType;
use App\EntityElement;
use App\Http\Requests\EntityStore;
use App\Http\Requests\EntityUpdate;
use App\Property;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

/**
 * Class EntitiesController
 * Админка -> Данные -> Сущности
 * @package App\Http\Controllers\Admin
 */
class EntitiesController extends MainController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    public function ajax($entityTypeId)
    {
        if (\Request::ajax()) {
            //@todo тут надо дергать инфу о доступах и после этого формирорвать ссылки и чекбоксы для таблицы и тп
            $access = [
                'edit' => Access::checkByAccess(AccessType::EDIT),
                'delete' => Access::checkByAccess(AccessType::DELETE)
            ];
            $select = [
                'id',
                'name',
                'code',
                'active',
                'system',
                'logging',
                'sort',
            ];
            /**
             * Номер страницы
             */
            $page = \Request::get('draw');
            /**
             * Количество записей на странице
             */
            $length = (int)\Request::get('length');
            /**
             * Смещение
             */
            $offset = \Request::get('start');
            $query = Entity::select($select);
            /**
             * Если -1, то выводить все записи
             */
            $recordsFiltered = Entity::where('entity_type_id', $entityTypeId)->count();
            if ($length !== -1)
                $query = $query->limit($length)->offset($offset);
            /**
             * Поисковый запрос
             */
            $search = \Request::get('search');
            /**
             * Найти все подходящие записи
             */
            if (isset($search['value']) && strlen($search['value']) > 0) {
                $querySearch = Entity::select($select);
                foreach ($select as $sItem) {
                    $querySearch->orWhere($sItem, 'like', '%' . $search['value'] . '%');
                }
                $itemsSearchDB = $querySearch->where('entity_type_id', $entityTypeId)->get();
                $query->whereIn('id', $itemsSearchDB->pluck('id'));
                $recordsFiltered = $itemsSearchDB->count();
            }
            /**
             * Сортировка
             */
            $order = \Request::get('order');
            $columns = \Request::get('columns');
            if (!empty($order)) {
                foreach ($order as $oItem) {
                    if (isset($oItem['column']) && !empty($columns[$oItem['column']]['name']) && $columns[$oItem['column']]['name'] !== 'chActions')
                        $query->orderBy($columns[$oItem['column']]['name'], empty($oItem['dir']) ? 'asc' : $oItem['dir']);
                }
            }

            $itemsDB = $query->where('entity_type_id', $entityTypeId)->get();
            $items = collect([]);
            if ($itemsDB->isNotEmpty()) {
                $collection = collect($itemsDB);
                $items = $collection->map(function ($item) use ($access) {
                    $item->chActions = '';
                    if ((bool)$item->system === false) {
                        $item->chActions = '<input type="checkbox" value="' . $item->id . '" name="setDelete[]"  onclick="selectRowForActions(this)"/>';
                    }
                    $item->name = '<a href="' . route('admin-entities.show', ['id' => $item->id]) . '">' . $item->name . '</a>';
                    return $item;
                });
            }
            return \Response::json(
                [
                    "draw" => $page,
                    "recordsTotal" => Entity::where('entity_type_id', $entityTypeId)->count(),
                    "recordsFiltered" => $recordsFiltered,
                    "data" => $items->isNotEmpty() ? $items->toArray() : []
                ]
            );
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($entityTypeId)
    {
        $entityType = EntityType::active()->find($entityTypeId);
        if (empty($entityType))
            abort(404, trans('admin.message.entity-type-not-active'));
        $data['entityType'] = $entityType;
        return view('admin.entities.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param EntityStore $request
     * @return RedirectResponse
     */
    public function store(EntityStore $request)
    {
        $userId = Auth::id();
        $item = new Entity;
        $item->name = $request->name;
        $item->description = $request->description;
        $item->code = $request->code;
        $item->entity_type_id = $request->entity_type_id;
        $item->active = (boolean)$request->active;
        $item->system = (boolean)$request->system;
        $item->logging = (boolean)$request->logging;
        $item->sort = $request->sort;
        $item->created_by = $userId;
        $item->save();
        return Redirect::route('admin-entity-types.show', ['id' => $item->entity_type_id])->with('flash_message', trans('admin.message.record-added'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $entityElement = new EntityElement;
        $properties = Property::where('entity_id', $id)->get();
        $data = [
            'entity' => Entity::find($id),
            'dtSettings' => [
                'urlColumns' => route('admin-entity-elements.ajaxColumns', ['id' => $id]),
                'urlData' => route('admin-entity-elements.ajaxData', ['id' => $id]),
            ],
            'tableColumns' => $entityElement->getDtTableColumns(),
            'properties' => $properties->isNotEmpty() ? $properties->toArray() : [],
            'access' => [
                'edit' => Access::checkByAccess(AccessType::EDIT),
                'delete' => Access::checkByAccess(AccessType::DELETE)
            ]
        ];
        return view('admin.entities.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['entity'] = Entity::find($id);
        $data['entityType'] = EntityType::find($data['entity']['entity_type_id']);
        $data['properties'] = Property::where('entity_id', $data['entity']['id'])->get();
        return view('admin.entities.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param EntityUpdate $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(EntityUpdate $request, $id)
    {
        $item = Entity::find($id);
        $userId = Auth::id();
        $item->name = $request->name;
        $item->description = $request->description;
        $item->code = $request->code;
        $item->entity_type_id = $request->entity_type_id;
        $item->active = (boolean)$request->active;
        $item->system = (boolean)$request->system;
        $item->logging = (boolean)$request->logging;
        $item->sort = $request->sort;
        $item->modified_by = $userId;
        $item->save();
        return Redirect::route('admin-entities.edit', $id)->with('flash_message', trans('admin.message.changes-saved'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return RedirectResponse
     */
    public function destroy()
    {
        $itemsForDelete = \Request::get('setDelete');
        $withElements = false;
        if (!empty($itemsForDelete)) {
            foreach ($itemsForDelete as $id) {
                /**
                 * Проверка существования элементов данной сущности
                 */
                $itemsCnt = EntityElement::where('entity_id', $id)->count();
                if ($itemsCnt > 0) {
                    $withElements = true;
                } else {
                    Entity::destroy($id);
                }
            }
        }
        $message = trans('admin.message.records-deleted');
        if ($withElements)
            $message = trans('admin.message.not-all-remove');
        return redirect(\URL::previous())->with('warning', $message);
    }
}
