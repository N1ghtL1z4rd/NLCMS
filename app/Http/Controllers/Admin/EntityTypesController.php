<?php

namespace App\Http\Controllers\Admin;

use App\AccessType;
use App\Cms\Access;
use App\EntityType;
use App\Entity;
use App\Http\Requests\EntityTypeStore;
use App\Http\Requests\EntityTypeUpdate;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

/**
 * Class EntityTypesController
 * Админка -> Данные -> Типы сущностей
 * @package App\Http\Controllers\Admin
 */
class EntityTypesController extends MainController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'dtAjaxSettings' => [
                'url' => route('admin-entity-types.ajax'),
                'columns' => [
                    [
                        'data' => 'chActions',
                        'name' => 'chActions',
                        'title' => '<input type="checkbox" id="chSelectAllRowsForActions" onclick="selectAllRowsForActions(this)">',
                        'orderable' => false,
                        'class' => 'text-center'
                    ],
                    [
                        'data' => 'id',
                        'name' => 'id',
                        'title' => 'ID',
                    ],
                    [
                        'data' => 'name',
                        'name' => 'name',
                        'title' => 'Наименование',
                    ],
                    [
                        'data' => 'code',
                        'name' => 'code',
                        'title' => 'Символьный код',
                    ],
                    [
                        'data' => 'active',
                        'name' => 'active',
                        'title' => 'Активность',
                        'class' => 'text-center'
                    ],
                    [
                        'data' => 'system',
                        'name' => 'system',
                        'title' => 'Системный',
                        'class' => 'text-center'
                    ],
                    [
                        'data' => 'sort',
                        'name' => 'sort',
                        'title' => 'Сортировка',
                        'class' => 'text-center'
                    ]
                ],
                'order' => [
                    [1, 'asc']
                ]
            ],
            'access' => [
                'edit' => Access::checkByAccess(AccessType::EDIT),
                'delete' => Access::checkByAccess(AccessType::DELETE)
            ]
        ];
        return view('admin.entity-types.index', $data);
    }

    public function ajax()
    {
        if (\Request::ajax()) {
            //@todo тут надо дергать инфу о доступах и после этого формирорвать ссылки и чекбоксы для таблицы и тп
            $access = [
                'edit' => Access::checkByAccess(AccessType::EDIT),
                'delete' => Access::checkByAccess(AccessType::DELETE)
            ];
            $select = [
                'id',
                'name',
                'code',
                'active',
                'system',
                'sort',
            ];
            /**
             * Номер страницы
             */
            $page = \Request::get('draw');
            /**
             * Количество записей на странице
             */
            $length = (int)\Request::get('length');
            /**
             * Смещение
             */
            $offset = \Request::get('start');
            $query = EntityType::select($select);
            /**
             * Если -1, то выводить все записи
             */
            $recordsFiltered = EntityType::count();
            if ($length !== -1)
                $query = $query->limit($length)->offset($offset);
            /**
             * Поисковый запрос
             */
            $search = \Request::get('search');
            /**
             * Найти все подходящие записи
             */
            if (isset($search['value']) && strlen($search['value']) > 0) {
                $querySearch = EntityType::select($select);
                foreach ($select as $sItem) {
                    $querySearch->orWhere($sItem, 'like', '%' . $search['value'] . '%');
                }
                $itemsSearchDB = $querySearch->get();
                $query->whereIn('id', $itemsSearchDB->pluck('id'));
                $recordsFiltered = $itemsSearchDB->count();
            }
            /**
             * Сортировка
             */
            $order = \Request::get('order');
            $columns = \Request::get('columns');
            if (!empty($order)) {
                foreach ($order as $oItem) {
                    if (isset($oItem['column']) && !empty($columns[$oItem['column']]['name']) && $columns[$oItem['column']]['name'] !== 'chActions')
                        $query->orderBy($columns[$oItem['column']]['name'], empty($oItem['dir']) ? 'asc' : $oItem['dir']);
                }
            }

            $itemsDB = $query->get();
            $items = collect([]);
            if ($itemsDB->isNotEmpty()) {
                $collection = collect($itemsDB);
                $items = $collection->map(function ($item) use ($access) {
                    $item->chActions = '';
                    if ((bool)$item->system === false) {
                        $item->chActions = '<input type="checkbox" value="' . $item->id . '" name="setDelete[]"  onclick="selectRowForActions(this)"/>';
                    }
                    $item->name = '<a href="' . route('admin-entity-types.show', ['id' => $item->id]) . '">' . $item->name . '</a>';
                    return $item;
                });
            }
            return \Response::json(
                [
                    "draw" => $page,
                    "recordsTotal" => EntityType::count(),
                    "recordsFiltered" => $recordsFiltered,
                    "data" => $items->isNotEmpty() ? $items->toArray() : []
                ]
            );
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.entity-types.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param EntityTypeStore $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(EntityTypeStore $request)
    {
        $userId = Auth::id();
        $item = new EntityType;
        $item->name = $request->name;
        $item->description = $request->description;
        $item->code = $request->code;
        $item->active = (boolean)$request->active;
        $item->system = (boolean)$request->system;
        $item->sort = (integer)$request->sort;
        $item->created_by = $userId;
        $item->save();
        return Redirect::route('admin-entity-types.index')->with('flash_message', trans('admin.message.record-added'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [
            'entityType' => EntityType::find($id),
            'dtAjaxSettings' => [
                'url' => route('admin-entities.ajax', ['id' => $id]),
                'columns' => [
                    [
                        'data' => 'chActions',
                        'name' => 'chActions',
                        'title' => '<input type="checkbox" id="chSelectAllRowsForActions" onclick="selectAllRowsForActions(this)">',
                        'orderable' => false,
                        'class' => 'text-center'
                    ],
                    [
                        'data' => 'id',
                        'name' => 'id',
                        'title' => 'ID',
                    ],
                    [
                        'data' => 'name',
                        'name' => 'name',
                        'title' => 'Наименование',
                    ],
                    [
                        'data' => 'code',
                        'name' => 'code',
                        'title' => 'Символьный код',
                    ],
                    [
                        'data' => 'active',
                        'name' => 'active',
                        'title' => 'Активность',
                        'class' => 'text-center'
                    ],
                    [
                        'data' => 'system',
                        'name' => 'system',
                        'title' => 'Системный',
                        'class' => 'text-center'
                    ],
                    [
                        'data' => 'logging',
                        'name' => 'logging',
                        'title' => 'Журналирование',
                        'class' => 'text-center'
                    ],
                    [
                        'data' => 'sort',
                        'name' => 'sort',
                        'title' => 'Сортировка',
                        'class' => 'text-center'
                    ]
                ],
                'order' => [
                    [1, 'asc']
                ]
            ],
            'access' => [
                'edit' => Access::checkByAccess(AccessType::EDIT),
                'delete' => Access::checkByAccess(AccessType::DELETE)
            ]
        ];

        return view('admin.entity-types.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = EntityType::find($id);
        return view('admin.entity-types.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param EntityTypeUpdate $request
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(EntityTypeUpdate $request, $id)
    {
        $item = EntityType::find($id);
        $userId = Auth::id();
        $item->name = $request->name;
        $item->description = $request->description;
        $item->code = $request->code;
        $item->active = (boolean)$request->active;
        $item->system = (boolean)$request->system;
        $item->sort = (integer)$request->sort;
        $item->modified_by = $userId;
        $item->save();
        return Redirect::route('admin-entity-types.edit', $id)->with('flash_message', trans('admin.message.changes-saved'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy()
    {
        $itemsForDelete = \Request::get('setDelete');
        $withElements = false;
        if (!empty($itemsForDelete)) {
            foreach ($itemsForDelete as $id) {
                /**
                 * Проверка существования сущностей данного типа
                 */
                $itemsCnt = Entity::where('entity_type_id', $id)->count();
                if ($itemsCnt > 0) {
                    $withElements = true;
                } else {
                    EntityType::destroy($id);
                }
            }
        }
        $message = trans('admin.message.records-deleted');
        if ($withElements)
            $message = trans('admin.message.not-all-remove');
        return redirect(\URL::previous())->with('warning', $message);
    }
}
