<?php

namespace App\Http\Controllers\Admin;

use App\Access;
use App\AccessObjectType;
use App\AccessSubjectType;
use App\AccessType;
use App\Events\GroupStore as EventStore;
use App\Events\GroupUpdate as EventUpdate;
use App\Events\GroupDestroy as EventDestroy;
use App\Group;
use App\Http\Requests\GroupStore;
use App\Http\Requests\GroupUpdate;
use App\Section;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Redirect;

/**
 * Class GroupsController
 * Админка -> Пользователи -> Группы пользователей
 * @package App\Http\Controllers\Admin
 */
class GroupsController extends MainController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'dtAjaxSettings' => [
                'url' => route('admin-groups.ajax'),
                'columns' => [
                    [
                        'data' => 'chActions',
                        'name' => 'chActions',
                        'title' => '<input type="checkbox" id="chSelectAllRowsForActions" onclick="selectAllRowsForActions(this)">',
                        'orderable' => false,
                        'class' => 'text-center'
                    ],
                    [
                        'data' => 'id',
                        'name' => 'id',
                        'title' => 'ID',
                    ],
                    [
                        'data' => 'name',
                        'name' => 'name',
                        'title' => 'Наименование',
                    ],
                    [
                        'data' => 'code',
                        'name' => 'code',
                        'title' => 'Символьный код',
                    ],
                    [
                        'data' => 'active',
                        'name' => 'active',
                        'title' => 'Активность',
                        'class' => 'text-center'
                    ],
                    [
                        'data' => 'system',
                        'name' => 'system',
                        'title' => 'Системный',
                        'class' => 'text-center'
                    ],
                    [
                        'data' => 'sort',
                        'name' => 'sort',
                        'title' => 'Сортировка',
                        'class' => 'text-center'
                    ]
                ],
                'order' => [
                    [1, 'asc']
                ]
            ],
            'access' => [
                'edit' => \App\Cms\Access::checkByAccess(AccessType::EDIT),
                'delete' => \App\Cms\Access::checkByAccess(AccessType::DELETE)
            ]
        ];
        return view('admin.groups.index', $data);
    }

    public function ajax()
    {
        if (\Request::ajax()) {
            $select = [
                'id',
                'name',
                'code',
                'active',
                'system',
                'sort',
            ];
            /**
             * Номер страницы
             */
            $page = \Request::get('draw');
            /**
             * Количество записей на странице
             */
            $length = (int)\Request::get('length');
            /**
             * Смещение
             */
            $offset = \Request::get('start');
            $query = Group::select($select);
            /**
             * Если -1, то выводить все записи
             */
            $recordsFiltered = Group::count();
            if ($length !== -1)
                $query = $query->limit($length)->offset($offset);
            /**
             * Поисковый запрос
             */
            $search = \Request::get('search');
            /**
             * Найти все подходящие записи
             */
            if (isset($search['value']) && strlen($search['value']) > 0) {
                $querySearch = Group::select($select);
                foreach ($select as $sItem) {
                    $querySearch->orWhere($sItem, 'like', '%' . $search['value'] . '%');
                }
                $itemsSearchDB = $querySearch->get();
                $query->whereIn('id', $itemsSearchDB->pluck('id'));
                $recordsFiltered = $itemsSearchDB->count();
            }
            /**
             * Сортировка
             */
            $order = \Request::get('order');
            $columns = \Request::get('columns');
            if (!empty($order)) {
                foreach ($order as $oItem) {
                    if (isset($oItem['column']) && !empty($columns[$oItem['column']]['name']) && $columns[$oItem['column']]['name'] !== 'chActions')
                        $query->orderBy($columns[$oItem['column']]['name'], empty($oItem['dir']) ? 'asc' : $oItem['dir']);
                }
            }

            $itemsDB = $query->get();
            $items = collect([]);
            if ($itemsDB->isNotEmpty()) {
                $collection = collect($itemsDB);
                $items = $collection->map(function ($item) {
                    $item->chActions = '';
                    if ((bool)$item->system === false) {
                        $item->chActions = '<input type="checkbox" value="' . $item->id . '" name="setDelete[]"  onclick="selectRowForActions(this)"/>';
                        $item->name = '<a href="' . route('admin-groups.edit', ['id' => $item->id]) . '">' . $item->name . '</a>';
                    }
                    return $item;
                });
            }
            return \Response::json(
                [
                    "draw" => $page,
                    "recordsTotal" => Group::count(),
                    "recordsFiltered" => $recordsFiltered,
                    "data" => $items->isNotEmpty() ? $items->toArray() : []
                ]
            );
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['access_object_type'] = AccessObjectType::where('code', 'SECTION')
            ->first()
            ->toArray();
        $data['access_subject_type'] = AccessSubjectType::where('code', 'GROUP')
            ->first()
            ->toArray();
        $data['sections'] = Section::active()
            ->orderBy('sort')
            ->orderBy('url')
            ->get()
            ->toArray();
        return view('admin.groups.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Throwable
     */
    public function store(GroupStore $request)
    {
        $userId = Auth::id();
        $item = new Group;
        $item->name = $request->name;
        $item->description = $request->description;
        $item->code = $request->code;
        $item->active = (boolean)$request->active;
        $item->system = (boolean)$request->system;
        $item->sort = (integer)$request->sort;
        $item->created_by = $userId;
        $item->modified_by = $userId;
        $item->save();

        if ($item->id) {
            $access = $request->get('access');
            $access_object_type = (int)$request->get('access_object_type');
            $access_subject_type = (int)$request->get('access_subject_type');
            if (!empty($access) && !empty($access_object_type) && !empty($access_subject_type)) {
                foreach ($access as $sId => $sAccess) {
                    foreach ($sAccess as $access_type => $flag)
                        if ((boolean)$flag === true)
                            Access::create([
                                'access_type_id' => $access_type,
                                'object_type_id' => $access_object_type,
                                'object_id' => $sId,
                                'subject_type_id' => $access_subject_type,
                                'subject_id' => $item->id,
                                'created_by' => $userId
                            ]);
                }
            }
        }
        /**
         *  Вызов события обновления группы
         */
        Event::dispatch(new EventStore($item));
        return Redirect::route('admin-groups.index')->with('flash_message', trans('admin.message.record-added'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ($id == 1 && Auth::id() != 1)
            abort(403);
        $data['element'] = Group::find($id)->toArray();
        $data['access_type'] = AccessType::where('code', 'VIEW')
            ->first()
            ->toArray();
        $data['access_object_type'] = AccessObjectType::where('code', 'SECTION')
            ->first()
            ->toArray();
        $data['access_subject_type'] = AccessSubjectType::where('code', 'GROUP')
            ->first()
            ->toArray();
        $data['sections'] = Section::active()
            ->orderBy('sort')
            ->orderBy('url')
            ->get()
            ->toArray();
        $accessDB = Access::where(
            [
                'object_type_id' => $data['access_object_type']['id'],
                'subject_type_id' => $data['access_subject_type']['id'],
                'subject_id' => $id,
            ]
        )
            ->get()
            ->toArray();
        $access = [];
        foreach ($accessDB as $k => $item) {
            $access[$item['object_id']][$item['access_type_id']] = $item;
        }
        $data['access'] = $access;
        return view('admin.groups.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param GroupUpdate $request
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Throwable
     */
    public function update(GroupUpdate $request, $id)
    {
        $item = Group::find($id);
        $userId = Auth::id();
        $item->name = $request->name;
        $item->description = $request->description;
        $item->code = $request->code;
        $item->active = (boolean)$request->active;
        $item->system = (boolean)$request->system;
        $item->sort = (integer)$request->sort;
        $item->modified_by = $userId;

        if ($item->save()) {
            $access = $request->get('access');
            $access_object_type = (int)$request->get('access_object_type');
            $access_subject_type = (int)$request->get('access_subject_type');
            Access::where(
                [
                    'object_type_id' => $access_object_type,
                    'subject_type_id' => $access_subject_type,
                    'subject_id' => $id,
                ])
                ->delete();
            if (!empty($access) && !empty($access_object_type) && !empty($access_subject_type)) {
                foreach ($access as $sId => $sAccess) {
                    foreach ($sAccess as $access_type => $flag)
                        if ((boolean)$flag === true)
                            Access::create([
                                'access_type_id' => $access_type,
                                'object_type_id' => $access_object_type,
                                'object_id' => $sId,
                                'subject_type_id' => $access_subject_type,
                                'subject_id' => $item->id,
                                'created_by' => $userId
                            ]);
                }
            }
        }
        /**
         *  Вызов события обновления группы
         */
        Event::dispatch(new EventUpdate($item));
        return Redirect::route('admin-groups.edit', $id)->with('flash_message', trans('admin.message.changes-saved'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy()
    {
        $itemsForDelete = \Request::get('setDelete');
        if (!empty($itemsForDelete)) {
            foreach ($itemsForDelete as $id) {
                if ((int)$id !== 1) {
                    Group::destroy($id);
                    event(new EventDestroy(Group::withTrashed()->find($id)));
                }
            }
        }
        return \Redirect::route('admin-groups.index')->with('flash_message', trans('admin.message.records-deleted'));
    }
}
