<?php

namespace App\Http\Controllers\Admin;

use App\AccessType;
use App\Cms\Access;
use App\JournalElement;
use Illuminate\Http\Request;

/**
 * Class JournalController
 * Админка -> Журналирование -> Журнал
 * @package App\Http\Controllers\Admin
 */
class JournalController extends MainController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'dtAjaxSettings' => [
                'url' => route('admin-journal.ajax'),
                'columns' => [
//                    [
//                        'data' => 'chActions',
//                        'name' => 'chActions',
//                        'title' => '<input type="checkbox" id="chSelectAllRowsForActions" onclick="selectAllRowsForActions(this)">',
//                        'orderable' => false,
//                        'class' => 'text-center'
//                    ],
                    [
                        'data' => 'id',
                        'name' => 'id',
                        'title' => 'ID',
                    ],
                    [
                        'data' => 'created_at',
                        'name' => 'created_at',
                        'title' => 'Дата',
                    ],
                    [
                        'data' => 'elementName',
                        'name' => 'elementName',
                        'title' => 'Наименование',
                    ],
                    [
                        'data' => 'operationName',
                        'name' => 'operationName',
                        'title' => 'Операция',
                    ],
                    [
                        'data' => 'authorName',
                        'name' => 'authorName',
                        'title' => 'Кем изменено',
                    ],
                ],
                'order' => [
                    [1, 'desc']
                ]
            ],
            'access' => [
                'edit' => Access::checkByAccess(AccessType::EDIT),
                'delete' => Access::checkByAccess(AccessType::DELETE)
            ]
        ];
        return view('admin.journal.index', $data);
    }

    public function ajax()
    {
        if (\Request::ajax()) {
            $select = [
                'created_at',
                'id',
                'operation_type_id',
                'element_id',
                'created_by',
            ];
            /**
             * Номер страницы
             */
            $draw = \Request::get('draw');
            /**
             * Количество записей на странице
             */
            $length = (int)\Request::get('length');
            /**
             * Смещение
             */
            $offset = \Request::get('start');
            $query = JournalElement::select();
            /**
             * Если -1, то выводить все записи
             */
            $recordsFiltered = JournalElement::count();
            if ($length !== -1)
                $query = $query->limit($length)->offset($offset);
            /**
             * Поисковый запрос
             */
            $search = \Request::get('search');
            /**
             * Найти все подходящие записи
             */
            if (isset($search['value']) && strlen($search['value']) > 0) {
                $querySearch = JournalElement::select($select);
                foreach ($select as $sItem) {
                    $querySearch->orWhere($sItem, 'like', "%{$search['value']}%");
                }
                $itemsSearchDB = $querySearch
                    ->orWhereHas('operation', function ($sFirst) use ($search) {
                        $sFirst->where('name', 'like', "%{$search['value']}%");
                    })
                    ->orWhereHas('element', function ($sSecond) use ($search) {
                        $sSecond->where('name', 'like', "%{$search['value']}%");
                    })->get();

                $query->whereIn('journal_elements.id', $itemsSearchDB->pluck('id'));
                $recordsFiltered = $itemsSearchDB->count();
            }
            /**
             * Сортировка
             */
            $order = array_first(\Request::get('order'));
            $columns = \Request::get('columns');
            if (!empty($order)) {
                if (isset($order['column']) && !empty($columns[$order['column']]['name'])) {
                    if (!in_array($columns[$order['column']]['name'], ['chActions', 'elementName', 'operationName', 'authorName'])) {
                        $query->orderBy($columns[$order['column']]['name'], empty($order['dir']) ? 'asc' : $order['dir']);
                    } /**
                     * Сортировка по наименованию элемента (столбец "Наименование")
                     */
                    elseif ($columns[$order['column']]['name'] === 'elementName') {
                        $query
                            ->join('entity_elements', 'journal_elements.element_id', '=', 'entity_elements.id')
                            ->orderBy('entity_elements.name', empty($order['dir']) ? 'asc' : $order['dir']);
                    } /**
                     * Сортировка по наименованию операции (столбец "Операция")
                     */
                    elseif ($columns[$order['column']]['name'] === 'operationName') {
                        $query
                            ->join('journal_operation_types', 'journal_elements.operation_type_id', '=', 'journal_operation_types.id')
                            ->orderBy('journal_operation_types.name', empty($order['dir']) ? 'asc' : $order['dir']);
                    } /**
                     * Сортировка по имени/никнэйму пользователя (столбец "Кем изменено")
                     */
                    elseif ($columns[$order['column']]['name'] === 'authorName') {
                        $query
                            ->join('users', 'journal_elements.created_by', '=', 'users.id')
                            ->orderBy('users.name', empty($order['dir']) ? 'asc' : $order['dir']);
                    }
                }
            }
            $itemsDB = $query->with(['element', 'operation', 'author'])->get();
            $items = collect([]);
            if ($itemsDB->isNotEmpty()) {
                $collection = collect($itemsDB);
                $items = $collection->map(function ($item) {
//                    $item->chActions = \Auth::id() === 1 ?
//                        '<input type="checkbox" value="' . $item->id . '" name="setDelete[]"/>' : '';
                    $item->elementName = '<a href="' . route('admin-journal.show', ['id' => $item->id]) . '">' . $item->element->name . '</a>';
                    $item->operationName = $item->operation->name;
                    $item->authorName = $item->author->name;
                    return $item;
                });
            }
            return \Response::json(
                [
                    "draw" => $draw,
                    "recordsTotal" => JournalElement::count(),
                    "recordsFiltered" => $recordsFiltered,
                    "data" => $items->isNotEmpty() ? $items->toArray() : []
                ]
            );
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [
            'item' => JournalElement::with('operation')
                ->with('element')
                ->with('author')
                ->withTrashed()
                ->find($id)
        ];
        return view('admin.journal.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
