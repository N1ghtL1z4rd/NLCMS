<?php

namespace App\Http\Controllers\Admin;

use App\AccessType;
use App\Cms\Access;
use App\DataType;
use App\ElementProperty;
use App\Entity;
use App\EntityElement;
use App\Events\EntityElementStore as ElementStore;
use App\Events\EntityElementUpdate as ElementUpdate;
use App\Events\EntityElementDestroy as ElementDestroy;
use App\File;
use App\Http\Requests\EntityElementStore;
use App\Property;
use App\User;
use function Couchbase\defaultDecoder;
use Illuminate\Support\Facades\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

/**
 * Class EntityElementsController
 * Админка -> Данные -> Элементы (элементы сущностей)
 * @package App\Http\Controllers\Admin
 */
class EntityElementsController extends MainController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    public function ajaxColumns($entityId)
    {
        if (\Request::ajax()) {
            $chActionsColumn = [
                [
                    'data' => 'chActions',
                    'name' => 'chActions',
                    'title' => '<input type="checkbox" id="chSelectAllRowsForActions" onclick="selectAllRowsForActions(this)">',
                    'orderable' => false,
                    'class' => 'text-center'
                ]
            ];
            $defaultOrder = [
                [1, 'asc']
            ];
            $selectedColumns = \Request::get('selectedColumns');
            $sColumns = [];
            /**
             * Получить колонки модели
             */
            $entityElement = new EntityElement;
            $modelColumns = $entityElement->getDtTableColumns();
            /**
             * Если есть выбранные колонки
             */
            if (!empty($selectedColumns)) {
                $modelColumnsNames = collect($modelColumns)->pluck('name')->toArray();
                /**
                 * Найти в выбранных колонках те, которые относятся к таблице элементов
                 */
                $modelColumnsSelected = array_intersect($selectedColumns, $modelColumnsNames);
                /**
                 * Добавить в массив выбранные колонки, которые относятся к таблице элементов
                 * Найти в выбранных колонках те, которые относятся к свойствам элементов (отфильтровать все id свойств)
                 */
                if (!empty($modelColumnsSelected)) {
                    $sColumns = collect($modelColumns)->whereIn('name', $modelColumnsSelected)->toArray();
                    $propertiesSelected = array_diff($selectedColumns, $modelColumnsSelected);
                } else {
                    $propertiesSelected = $selectedColumns;
                }
                /**
                 * Свойства
                 */
                if (!empty($propertiesSelected)) {
                    $propertiesDB = Property::find($propertiesSelected);
                    if ($propertiesDB->isNotEmpty()) {
                        foreach ($propertiesDB->keyBy('id')->toArray() as $property) {
                            $sColumns[] = [
                                'data' => $property['code'],
                                'name' => $property['id'],
                                'title' => $property['name'],
                                'orderable' => true,
                            ];
                        }
                    }
                }
            }
            if (empty($sColumns)) {
                $data = [
                    'columns' => array_merge($chActionsColumn, collect($modelColumns)->where('default', true)->toArray()),
                    'order' => $defaultOrder
                ];
            } else {
                $data = [
                    'columns' => array_merge($chActionsColumn, $sColumns),
                    'order' => $defaultOrder
                ];
            }

            return \Response::json($data);
        }
    }

    public function ajaxData($entityId)
    {
        if (\Request::ajax()) {
            $access = [
                'edit' => Access::checkByAccess(AccessType::EDIT),
                'delete' => Access::checkByAccess(AccessType::DELETE)
            ];

            $selectedColumns = \Request::get('selectedColumns');
            /**
             * Получить колонки модели
             */
            $entityElement = new EntityElement;
            $modelColumns = $entityElement->getDtTableColumns();
            $select = ['*'];
            $propertiesSelected = [];
            $propertiesSelectedInfo = [];
            if (!empty($selectedColumns)) {
                /**
                 * Получить колонки модели
                 */
                $modelColumnsNames = collect($modelColumns)->pluck('name')->toArray();
                /**
                 * Найти в выбранных колонках те, которые относятся к таблице элементов
                 */
                $modelColumnsSelected = array_intersect($selectedColumns, $modelColumnsNames);
                /**
                 * Добавить в массив выбранные колонки, которые относятся к таблице элементов
                 * Найти в выбранных колонках те, которые относятся к свойствам элементов
                 * (отфильтровать все id свойств)
                 */
                if (!empty($modelColumnsSelected)) {
                    $propertiesSelected = array_diff($selectedColumns, $modelColumnsSelected);
                }
            }

            $draw = \Request::get('draw');
            /**
             * Количество записей на странице
             */
            $length = (int)\Request::get('length');
            /**
             * Смещение
             */
            $offset = \Request::get('start');
            $query = EntityElement::select($select);
            /**
             * Если -1, то выводить все записи
             */
            $recordsAll = EntityElement::where('entity_id', $entityId)->count();
            if ($length !== -1)
                $query = $query->limit($length)->offset($offset);

            /**
             * Получить все значения свойств, которые выбраны с интерфейса
             */
            if (!empty($propertiesSelected)) {
                $propertiesSelectedInfo = Property::find($propertiesSelected)->keyBy('code')->toArray();
                $query->with(['properties' => function ($query) use ($propertiesSelected) {
                    $query->whereIn('property_id', $propertiesSelected);
                }]);
            }

            /**
             * Поисковый запрос
             */
//            $search = \Request::get('search');
//            /**
//             * Найти все подходящие записи
//             * @todo надо переделать, т.к. нужен поиск по свойствам
//             */
//            if (isset($search['value']) && strlen($search['value']) > 0) {
//                $querySearch = EntityElement::select($select);
//                foreach ($select as $sItem) {
//                    $querySearch->orWhere($sItem, 'like', '%' . $search['value'] . '%');
//                }
//                $itemsSearchDB = $querySearch->where('entity_id', $entityId)->get();
//                $query->whereIn('id', $itemsSearchDB->pluck('id'));
//                $recordsFiltered = $itemsSearchDB->count();
//            }
            /**
             * Сортировка
             */
//            $order = \Request::get('order');
//            $columns = \Request::get('columns');
//            if (!empty($order)) {
//                foreach ($order as $oItem) {
//                    if (isset($oItem['column']) && !empty($columns[$oItem['column']]['name']) && $columns[$oItem['column']]['name'] !== 'chActions')
//                        $query->orderBy($columns[$oItem['column']]['name'], empty($oItem['dir']) ? 'asc' : $oItem['dir']);
//                }
//            }

            $itemsDB = $query->where('entity_id', $entityId)->get();
            $items = collect([]);
            if ($itemsDB->isNotEmpty()) {
                $collection = collect($itemsDB);
                $items = $collection->map(function ($item) use ($access, $propertiesSelectedInfo) {
                    $item->chActions = '';
                    if ((bool)$access['delete'] === true) {
                        $item->chActions = '<input type="checkbox" value="' . $item->id . '" name="setDelete[]"  onclick="selectRowForActions(this)"/>';
                    }
                    if ((bool)$access['edit'] === true) {
                        $item->name = '<a href="' . route('admin-entity-elements.edit', ['id' => $item->id]) . '">' . $item->name . '</a>';
                    }
                    if (!empty($propertiesSelectedInfo)) {
                        $properties = collect($item->properties)->groupBy('property_id')->toArray();
                        foreach ($propertiesSelectedInfo as $pCode => $property) {
                            $item[$pCode] = null;
                            if (!empty($properties[$property['id']]))
                                $item[$pCode] = collect($properties[$property['id']])->implode('value', "<br>");
                        }
                    }
                    unset($item->properties);
                    return $item;
                });
            }
            return \Response::json(
                [
                    "draw" => $draw,
                    "recordsTotal" => $recordsAll,
                    "recordsFiltered" => isset($recordsFiltered) ? $recordsFiltered : $recordsAll,
                    "data" => $items->isNotEmpty() ? $items->toArray() : [],
                ]
            );
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($entityId)
    {
        $entity = Entity::active()->with('entityType')->find($entityId);
        if (empty($entity))
            abort(404);
        $properties = Property::active()
            ->where('entity_id', $entityId)
            ->with('options')
            ->with('relations')
            ->orderBy('sort')
            ->get()
            ->keyBy('id')
            ->toArray();
        $users = User::all()->keyBy('id')->toArray();
        foreach ($properties as $slId => $setting) {
            if ($setting['data_type_id'] == DataType::USER) {
                $properties[$slId]['options'] = $users;
            }
        }
        $data = [
            'entity' => $entity,
            'properties' => $properties
        ];
        return view('admin.entity-elements.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param EntityElementStore $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(EntityElementStore $request)
    {
        $userId = Auth::id();
        $preview_img_Id = null;
        $preview_img_file = $request->file('preview_img');
        if (!empty($preview_img_file) && $preview_img_file->isFile() && $preview_img_file->isValid()) {
            $preview_img_fileName = $preview_img_file->getClientOriginalName();
            $preview_img_path = $preview_img_file->store('public');
            $fileItemPrev = new File;
            $fileItemPrev->name = $preview_img_fileName;
            $fileItemPrev->path = $preview_img_path;
            $fileItemPrev->created_by = $userId;
            $fileItemPrev->save();
            $preview_img_Id = $fileItemPrev->id;
        }
        $detail_img_Id = null;
        $detail_img_file = $request->file('detail_img');
        if (!empty($detail_img_file) && $detail_img_file->isFile() && $detail_img_file->isValid()) {
            $detail_img_fileName = $detail_img_file->getClientOriginalName();
            $detail_img_path = $detail_img_file->store('public');
            $fileItemDetail = new File;
            $fileItemDetail->name = $detail_img_fileName;
            $fileItemDetail->path = $detail_img_path;
            $fileItemDetail->created_by = $userId;
            $fileItemDetail->save();
            $detail_img_Id = $fileItemDetail->id;
        }

        $item = new EntityElement;
        $item->name = $request->name;
        $item->code = $request->code;
        $item->extended_id = $request->extended_id;
        $item->preview_img = $preview_img_Id;
        $item->preview = $request->preview;
        $item->detail_img = $detail_img_Id;
        $item->detail = $request->detail;
        $item->entity_id = $request->entity_id;
        $item->active = (boolean)$request->active;
        $item->sort = $request->sort;
        $item->created_by = $userId;
        $item->save();

        if ($item->id) {
            /**
             * Проверка файлов в данных, которые пришли с формы
             */
            if ($request->hasFile('properties'))
                $files = $request->file('properties');
            /**
             * Все данные с формы кроме файлов
             */
            $properties = $request->get('properties');
            if (!empty($properties)) {
                foreach ($properties as $id => $value) {
                    /**
                     * Множественные
                     */
                    if (is_array($value)) {
                        foreach ($value as $k => $v) {
                            if (isset($v) && strlen($v) > 0) {
                                ElementProperty::create([
                                    'element_id' => $item->id,
                                    'property_id' => $id,
                                    'value' => $v,
                                    'sort' => $k * 10,
                                ]);
                            }
                        }
                    } else {
                        if (isset($value) && strlen($value) > 0) {
                            ElementProperty::create([
                                'element_id' => $item->id,
                                'property_id' => $id,
                                'value' => $value
                            ]);
                        }
                    }
                }
            }
            /**
             * Файлы
             */
            if (!empty($files)) {
                foreach ($files as $id => $value) {
                    /**
                     * Множественные
                     */
                    if (is_array($value)) {
                        foreach ($value as $k => $v) {
                            if ($files[$id][$k]->isValid()) {
                                $file = $files[$id][$k];
                                $fileName = $file->getClientOriginalName();
                                $path = $file->store('public');
                                $fileItem = new File;
                                $fileItem->name = $fileName;
                                $fileItem->path = $path;
                                $fileItem->created_by = $userId;
                                $fileItem->modified_by = $userId;
                                $fileItem->save();
                                $v = $fileItem->id;

                                if (!empty($v)) {
                                    ElementProperty::create([
                                        'element_id' => $item->id,
                                        'property_id' => $id,
                                        'value' => $v,
                                        'sort' => $k * 10,
                                    ]);
                                }
                            }
                        }
                    } else {
                        if (!empty($files[$id])) {
                            if ($files[$id]->isValid()) {
                                $file = $files[$id];
                                $fileName = $file->getClientOriginalName();
                                $path = $file->store('public');
                                $fileItem = new File;
                                $fileItem->name = $fileName;
                                $fileItem->path = $path;
                                $fileItem->created_by = $userId;
                                $fileItem->modified_by = $userId;
                                $fileItem->save();
                                $value = $fileItem->id;
                                if (!empty($value)) {
                                    ElementProperty::create([
                                        'element_id' => $item->id,
                                        'property_id' => $id,
                                        'value' => $value
                                    ]);
                                }
                            }
                        }
                    }
                }
            }
        }
        Event::dispatch(new ElementStore($item));
        return Redirect::route('admin-entities.show', ['id' => $item->entity_id])->with('flash_message', trans('admin.message.record-added'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return void
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['access'] = [
            'delete' => Access::checkByAccess(AccessType::DELETE)
        ];
        $data['element'] = EntityElement::with('author', 'editor', 'preview_img', 'detail_img')
            ->find($id)
            ->toArray();
        $data['entity'] = Entity::find($data['element']['entity_id']);
        $data['properties'] = Property::active()
            ->where('entity_id', $data['element']['entity_id'])
            ->with('options')
            ->with('relations')
            ->orderBy('sort')
            ->get()
            ->keyBy('id')
            ->toArray();
        $propertiesData = ElementProperty::where('element_id', $id)
            ->orderBy('property_id')
            ->orderBy('sort')
            ->get()
            ->groupBy('property_id')
            ->toArray();
        $users = User::all()->keyBy('id')->toArray();
        foreach ($data['properties'] as $slId => $property) {
            if ($property['data_type_id'] == DataType::FILE && !empty($propertiesData[$property['id']])) {
                if (isset($propertiesData[$property['id']])) {
                    if (is_array($propertiesData[$property['id']])) {
                        //@todo здесь переделать с цикла
                        foreach ($propertiesData[$property['id']] as $key => $datum) {
                            if (!empty((int)$datum)) {
                                $file = File::find((int)$datum['value']);
                                $propertiesData[$property['id']][$key]['file'] = !empty($file) ? $file->toArray() : [];
                            }
                        }
                    } else {
                        if (!empty($propertiesData[$property['id']][0])) {
                            $file = File::find((int)$propertiesData[$property['id']][0]['value']);
                            $propertiesData[$property['id']][0]['file'] = !empty($file) ? $file->toArray() : [];
                        }
                    }
                }
            }
            if ($property['data_type_id'] == DataType::USER) {
                $data['properties'][$slId]['options'] = $users;
            }
        }
        $data['propertiesData'] = $propertiesData;
        return view('admin.entity-elements.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param EntityElementStore $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function update(EntityElementStore $request, $id)
    {
        $userId = Auth::id();
        $preview_img_Id = null;
        if (empty($request->input('preview_img.id'))) {
            $preview_img_file = $request->file('preview_img.file');
            if (!empty($preview_img_file) && $preview_img_file->isFile() && $preview_img_file->isValid()) {
                $preview_img_fileName = $preview_img_file->getClientOriginalName();
                $preview_img_path = $preview_img_file->store('public');
                $fileItemPrev = new File;
                $fileItemPrev->name = $preview_img_fileName;
                $fileItemPrev->path = $preview_img_path;
                $fileItemPrev->created_by = $userId;
                $fileItemPrev->save();
                $preview_img_Id = $fileItemPrev->id;
            }
        }
        $detail_img_Id = null;
        if (empty($request->input('detail_img.id'))) {
            $detail_img_file = $request->file('detail_img.file');
            if (!empty($detail_img_file) && $detail_img_file->isFile() && $detail_img_file->isValid()) {
                $detail_img_fileName = $detail_img_file->getClientOriginalName();
                $detail_img_path = $detail_img_file->store('public');
                $fileItemDetail = new File;
                $fileItemDetail->name = $detail_img_fileName;
                $fileItemDetail->path = $detail_img_path;
                $fileItemDetail->created_by = $userId;
                $fileItemDetail->save();
                $detail_img_Id = $fileItemDetail->id;
            }
        }

        $item = EntityElement::with('preview_img')
            ->with('detail_img')
            ->with('properties')
            ->find($id);
        $conditionBefore = $item->toJson();
        $item->name = $request->name;
        $item->code = $request->code;
        $item->extended_id = $request->extended_id;
        $item->preview_img = $preview_img_Id;
        $item->preview = $request->preview;
        $item->detail_img = $detail_img_Id;
        $item->detail = $request->detail;
        $item->entity_id = $request->entity_id;
        $item->active = (boolean)$request->active;
        $item->sort = $request->sort;
        $item->modified_by = $userId;
        $item->save();

        $propertiesForm = $request->get('properties');
        $files = $request->file('properties.files');
        $properties = Property::active()
            ->where('entity_id', $item->entity_id)
            ->with('options')
            ->with('relations')
            ->orderBy('sort')
            ->get()
            ->keyBy('id')
            ->toArray();
        if (!empty($properties)) {
            foreach ($properties as $idProperty => $property) {
                /**
                 * Ранее заполненные данные по свойству
                 */
                $valuesDB = ElementProperty::where(['element_id' => $item->id, 'property_id' => $idProperty,])
                    ->get()
                    ->keyBy('id')
                    ->toArray();

                $value = isset($propertiesForm[$idProperty]) ? $propertiesForm[$idProperty] : null;
                $pFiles = null;
                if ($property['data_type_id'] === DataType::FILE) {
                    $pFiles = isset($files[$idProperty]) ? $files[$idProperty] : null;
                }
                if (isset($value) || !empty($pFiles)) {
                    /**
                     * Обработка множественных свойств
                     */
                    if ($property['multiple']) {
                        /**
                         * Если вдруг не массив
                         */
                        if (!is_array($value))
                            $value = [$value];

                        if (!empty($value)) {
                            foreach ($value as $k => $v) {
                                if (isset($v) && strlen($v) > 0) {
                                    $epItem = ElementProperty::firstOrCreate(
                                        [
                                            'element_id' => $item->id,
                                            'property_id' => $idProperty,
                                            'value' => $v
                                        ],
                                        [
                                            'element_id' => $item->id,
                                            'property_id' => $idProperty,
                                            'value' => $v,
                                            'sort' => ($k + 1) * 100
                                        ]
                                    );
                                    /**
                                     * Удалим из массива старых значений
                                     */
                                    unset($valuesDB[$epItem->id]);
                                }
                            }
                        } else {
                            ElementProperty::where(['element_id' => $item->id, 'property_id' => $idProperty])->delete();
                        }
                        /**
                         * Обработка файлов
                         */
                        if (!empty($pFiles)) {
                            foreach ($pFiles as $k => $pFile) {
                                $v = null;
                                if ($pFile->isValid()) {
                                    $file = $pFile;
                                    $fileName = $file->getClientOriginalName();
                                    $path = $file->store('public');
                                    $fileItem = new File;
                                    $fileItem->name = $fileName;
                                    $fileItem->path = $path;
                                    $fileItem->created_by = $userId;
                                    $fileItem->modified_by = $userId;
                                    $fileItem->save();
                                    $v = $fileItem->id;
                                }
                                if (!empty($v)) {
                                    ElementProperty::create(
                                        [
                                            'element_id' => $item->id,
                                            'property_id' => $idProperty,
                                            'value' => $v,
                                            'sort' => ($k + 1 + count($valuesDB)) * 100
                                        ]
                                    );
                                }
                            }
                        }
                        /**
                         * тут надо удалять не пришедшие с формы значения (удалены на форме)
                         */
                        ElementProperty::destroy(array_keys($valuesDB));
                    } /**
                     * Обработка НЕ множественных свойств
                     */
                    else {
                        /**
                         * По какой-то причине может быть массив
                         */
                        if (is_array($value))
                            $value = array_first($value);
                        /**
                         * Обработка файлов
                         */
                        if ($property['data_type_id'] === DataType::FILE) {
                            /**
                             * Если файл пришел с формы, то заменить текущий (если он есть)
                             *
                             * Если файлов с формы нет, то значение для параметра:
                             * - либо пришло с формы (если файл не менялся)
                             * - либо пустое, если файл удален
                             */
                            if (!empty($files[$idProperty])) {
                                if ($files[$idProperty]->isValid()) {
                                    $file = $files[$idProperty];
                                    $fileName = $file->getClientOriginalName();
                                    $path = $file->store('public');
                                    $fileItem = new File;
                                    $fileItem->name = $fileName;
                                    $fileItem->path = $path;
                                    $fileItem->created_by = $userId;
                                    $fileItem->modified_by = $userId;
                                    $fileItem->save();
                                    $value = $fileItem->id;
                                }
                            }
                        }

                        if (isset($value) && strlen($value) > 0) {
                            ElementProperty::updateOrCreate(
                                [
                                    'element_id' => $item->id,
                                    'property_id' => $idProperty
                                ],
                                [
                                    'element_id' => $item->id,
                                    'property_id' => $idProperty,
                                    'value' => $value
                                ]
                            );
                        } else {
                            //@todo по хорошему тут надо удалять файлы из таблицы
                            ElementProperty::where(['element_id' => $item->id, 'property_id' => $idProperty])->delete();
                        }
                    }
                } /**
                 * Если нет данных с формы по свойству, то удалять
                 */
                else {
                    //@todo по хорошему тут надо удалять файлы из таблицы
                    ElementProperty::where(['element_id' => $item->id, 'property_id' => $idProperty])->delete();
                }
            }
        }
        $element = EntityElement::with('preview_img')
            ->with('detail_img')
            ->with('properties')
            ->find($id);
        $conditionAfter = $element->toJson();
        /**
         * Сравнение состояний до и после обновления
         */
        if ($conditionBefore != $conditionAfter) {
            /**
             *  Вызов события обновления элемента
             */
            Event::dispatch(new ElementUpdate($element));
        }
        return Redirect::route('admin-entity-elements.edit', $id)->with('flash_message', trans('admin.message.changes-saved'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy()
    {
        $itemsForDelete = \Request::get('setDelete');
        if (!empty($itemsForDelete)) {
            foreach ($itemsForDelete as $id) {
                EntityElement::destroy($id);
                $delItem = EntityElement::withTrashed()->find($id);
                event(new ElementDestroy($delItem));
            }
        }
        return redirect(\URL::previous())->with('flash_message', trans('admin.message.records-deleted'));
    }
}
