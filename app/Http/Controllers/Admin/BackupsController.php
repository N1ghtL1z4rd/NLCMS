<?php

namespace App\Http\Controllers\Admin;

use App\AccessType;
use File;
use Illuminate\Http\Request;
use Redirect;
use App\Cms\Access;

class BackupsController extends MainController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['items'] = false;
        if (File::exists('../storage/app/backups'))
            $data['items'] = File::allFiles('../storage/app/backups');

        $data['access'] = [
            'edit' => Access::checkByAccess(AccessType::EDIT),
            'delete' => Access::checkByAccess(AccessType::DELETE)
        ];
        return view('admin.backups.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Spatie\DbDumper\Exceptions\CannotStartDump
     * @throws \Spatie\DbDumper\Exceptions\DumpFailed
     */
    public function create()
    {
        if (!File::exists('../storage/app/backups'))
            File::makeDirectory('../storage/app/backups');
        $connect = \DB::getConfig();
        $date = date('Y-m-d_H-i-s');
        $fileSql = realpath('../storage/app/backups') . '/db-dump-' . $date . '.sql';
        $fileZip = realpath('../storage/app/backups') . '/db-dump-' . $date . '.zip';

        if ($connect['driver'] === 'pgsql') {
            $query = \Spatie\DbDumper\Databases\PostgreSql::create();
        } elseif ($connect['driver'] === 'mysql') {
            $query = \Spatie\DbDumper\Databases\MySql::create();
            if ($connect['dump']['use_single_transaction'])
                $query->useSingleTransaction();
        } else {
            return Redirect::route('admin-backups.index')->with('flash_message', trans('admin.message.backups-error'));
        }
        $query
            ->setDumpBinaryPath($connect['dump']['binary_path'])
            ->setHost($connect['host'])
            ->setPort($connect['port'])
            ->setDbName($connect['database'])
            ->setUserName($connect['username'])
            ->setPassword($connect['password'])
            ->setTimeout($connect['dump']['timeout'])
            ->dumpToFile($fileSql);

        $zip = new \ZipArchive;

        if ($zip->open($fileZip, \ZipArchive::CREATE) !== true) {
            exit("Невозможно открыть <$fileZip>\n");
        }
        $zip->addFile($fileSql, "db-dump-$date.sql");
        $zip->close();
        unlink($fileSql);

        return Redirect::route('admin-backups.index')->with('flash_message', trans('admin.message.backups-success'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param $file
     * @return void
     */
    public function show($file)
    {
        return response()->download('../storage/app/backups/' . $file);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $file
     * @return \Illuminate\Http\Response
     */
    public function destroy($file)
    {
        File::delete('../storage/app/backups/' . $file);
        return redirect()->route('admin-backups.index')->with('flash_message', trans('admin.message.record-deleted'));
    }
}
