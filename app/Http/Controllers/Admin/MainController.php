<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

/**
 * Class MainController
 * Админка: главный контроллер
 * @package App\Http\Controllers\Admin
 */
class MainController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('access');
    }

    /**
     * Главная административной панели
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.index');
    }

    /**
     * Информация о PHP
     */
    public function phpInfo()
    {
        phpinfo();
    }
}
