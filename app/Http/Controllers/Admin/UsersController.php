<?php

namespace App\Http\Controllers\Admin;

use App\AccessType;
use App\Cms\Access;
use App\Events\UserDestroy;
use App\Events\UserUpdate as EventUpdate;
use App\Http\Requests\UserStore;
use App\Http\Requests\UserUpdate;
use App\User;
use App\SettingList;
use App\SettingListObject;
use App\Group;
use App\DataType;
use App\UserGroup;
use App\UserSetting;
use App\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UsersController extends MainController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'dtAjaxSettings' => [
                'url' => route('admin-users.ajax'),
                'columns' => [
                    [
                        'data' => 'chActions',
                        'name' => 'chActions',
                        'title' => '<input type="checkbox" id="chSelectAllRowsForActions" onclick="selectAllRowsForActions(this)">',
                        'orderable' => false,
                        'class' => 'text-center'
                    ],
                    [
                        'data' => 'id',
                        'name' => 'id',
                        'title' => 'ID',
                    ],
                    [
                        'data' => 'name',
                        'name' => 'name',
                        'title' => 'Имя/Никнейм',
                    ],
                    [
                        'data' => 'email',
                        'name' => 'email',
                        'title' => 'E-mail',
                    ],
                    [
                        'data' => 'active',
                        'name' => 'active',
                        'title' => 'Активность',
                        'class' => 'text-center'
                    ],
                    [
                        'data' => 'system',
                        'name' => 'system',
                        'title' => 'Системный',
                        'class' => 'text-center'
                    ],
                    [
                        'data' => 'sort',
                        'name' => 'sort',
                        'title' => 'Сортировка',
                        'class' => 'text-center'
                    ]
                ],
                'order' => [
                    [1, 'asc']
                ]
            ],
            'access' => [
                'edit' => Access::checkByAccess(AccessType::EDIT),
                'delete' => Access::checkByAccess(AccessType::DELETE)
            ]
        ];
        return view('admin.users.index', $data);
    }

    public function ajax()
    {
        if (\Request::ajax()) {
            $select = [
                'id',
                'name',
                'email',
                'active',
                'system',
                'sort',
            ];
            /**
             * Номер страницы
             */
            $page = \Request::get('draw');
            /**
             * Количество записей на странице
             */
            $length = (int)\Request::get('length');
            /**
             * Смещение
             */
            $offset = \Request::get('start');
            $query = User::select($select);
            /**
             * Если -1, то выводить все записи
             */
            $recordsFiltered = User::count();
            if ($length !== -1)
                $query = $query->limit($length)->offset($offset);
            /**
             * Поисковый запрос
             */
            $search = \Request::get('search');
            /**
             * Найти все подходящие записи
             */
            if (isset($search['value']) && strlen($search['value']) > 0) {
                $querySearch = User::select($select);
                foreach ($select as $sItem) {
                    $querySearch->orWhere($sItem, 'like', '%' . $search['value'] . '%');
                }
                $itemsSearchDB = $querySearch->get();
                $query->whereIn('id', $itemsSearchDB->pluck('id'));
                $recordsFiltered = $itemsSearchDB->count();
            }
            /**
             * Сортировка
             */
            $order = \Request::get('order');
            $columns = \Request::get('columns');
            if (!empty($order)) {
                foreach ($order as $oItem) {
                    if (isset($oItem['column']) && !empty($columns[$oItem['column']]['name']) && $columns[$oItem['column']]['name'] !== 'chActions')
                        $query->orderBy($columns[$oItem['column']]['name'], empty($oItem['dir']) ? 'asc' : $oItem['dir']);
                }
            }

            $itemsDB = $query->get();
            $items = collect([]);
            if ($itemsDB->isNotEmpty()) {
                $collection = collect($itemsDB);
                $items = $collection->map(function ($item) {
                    $item->chActions = '';
                    if ((bool)$item->system === false) {
                        $item->chActions = '<input type="checkbox" value="' . $item->id . '" name="setDelete[]"  onclick="selectRowForActions(this)"/>';
                        $item->name = '<a href="' . route('admin-users.edit', ['id' => $item->id]) . '">' . $item->name . '</a>';
                    }
                    return $item;
                });
            }
            return \Response::json(
                [
                    "draw" => $page,
                    "recordsTotal" => User::count(),
                    "recordsFiltered" => $recordsFiltered,
                    "data" => $items->isNotEmpty() ? $items->toArray() : []
                ]
            );
        }
    }

    public function login($id)
    {
        Auth::loginUsingId($id);
        return redirect()->back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $settingListObject = SettingListObject::where('code', SettingListObject::USER)->get()->first();
        $settings = SettingList::with('options')
            ->with('relations')
            ->where('setting_list_object_id', $settingListObject['id'])
            ->active()
            ->orderBy('sort')
            ->get()
            ->keyBy('id')
            ->toArray();
        $users = User::all()->keyBy('id')->toArray();
        foreach ($settings as $slId => $setting) {
            if ($setting['data_type_id'] == DataType::USER) {
                $settings[$slId]['options'] = $users;
            }
        }
        $data = [
            'groups' => Group::active()->get()->toArray(),
            'settings' => $settings
        ];
        return view('admin.users.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UserStore $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(UserStore $request)
    {
        $userId = Auth::id();
        $item = new User;
        $item->name = $request->name;
        $item->email = $request->email;
        $item->password = Hash::make($request->password);
        $item->active = (boolean)$request->active;
        $item->system = (boolean)$request->system;
        $item->sort = (integer)$request->sort;
        $item->created_by = $userId;
        $item->modified_by = $userId;
        $item->save();

        if ($item->id) {
            if (!empty($request->get('groups'))) {
                $groups = [];
                foreach ($request->get('groups') as $group) {
                    $groups[] = [
                        'user_id' => $item->id,
                        'group_id' => $group
                    ];
                }
                UserGroup::insert($groups);
            }

            /**
             * Проверка файлов в данных, которые пришли с формы
             */
            $files = $request->file('properties.files');
            /**
             * Все данные с формы кроме файлов
             */
            $properties = $request->get('properties');
            if (!empty($properties)) {
                foreach ($properties as $id => $value) {
                    /**
                     * Множественные
                     */
                    if (is_array($value)) {
                        foreach ($value as $k => $v) {
                            if (isset($v) && strlen($v) > 0) {
                                UserSetting::create([
                                    'user_id' => $item->id,
                                    'setting_list_id' => $id,
                                    'value' => $v,
                                    'sort' => $k * 10,
                                ]);
                            }
                        }
                    } else {
                        if (isset($value) && strlen($value) > 0) {
                            UserSetting::create([
                                'user_id' => $item->id,
                                'setting_list_id' => $id,
                                'value' => $value
                            ]);
                        }
                    }
                }
            }
            /**
             * Файлы
             */
            if (!empty($files)) {
                foreach ($files as $id => $value) {
                    /**
                     * Множественные
                     */
                    if (is_array($value)) {
                        foreach ($value as $k => $v) {
                            if ($files[$id][$k]->isValid()) {
                                $file = $files[$id][$k];
                                $fileName = $file->getClientOriginalName();
                                $path = $file->store('public');
                                $fileItem = new File;
                                $fileItem->name = $fileName;
                                $fileItem->path = $path;
                                $fileItem->created_by = $userId;
                                $fileItem->modified_by = $userId;
                                $fileItem->save();
                                $v = $fileItem->id;

                                if (!empty($v)) {
                                    UserSetting::create([
                                        'user_id' => $item->id,
                                        'setting_list_id' => $id,
                                        'value' => $v,
                                        'sort' => $k * 10,
                                    ]);
                                }
                            }
                        }
                    } else {
                        if (!empty($files[$id])) {
                            if ($files[$id]->isValid()) {
                                $file = $files[$id];
                                $fileName = $file->getClientOriginalName();
                                $path = $file->store('public');
                                $fileItem = new File;
                                $fileItem->name = $fileName;
                                $fileItem->path = $path;
                                $fileItem->created_by = $userId;
                                $fileItem->modified_by = $userId;
                                $fileItem->save();
                                $value = $fileItem->id;
                                if (!empty($value)) {
                                    UserSetting::create([
                                        'user_id' => $item->id,
                                        'setting_list_id' => $id,
                                        'value' => $value
                                    ]);
                                }
                            }
                        }
                    }
                }
            }
        }
        return Redirect::route('admin-users.index')->with('flash_message', trans('admin.message.record-added'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        if ($user->id == 1 && Auth::id() != 1)
            abort(403);
        $settingListObject = SettingListObject::where('code', SettingListObject::USER)->get()->first();
        $settingsList = SettingList::with('options')
            ->with('relations')
            ->where('setting_list_object_id', $settingListObject['id'])
            ->active()
            ->orderBy('sort')
            ->get()
            ->keyBy('id')
            ->toArray();
        $settingsData = UserSetting::where('user_id', $user->id)
            ->orderBy('setting_list_id')
            ->orderBy('sort')
            ->get()
            ->groupBy('setting_list_id')
            ->toArray();
        $users = User::all()->keyBy('id')->toArray();
        foreach ($settingsList as $slId => $setting) {
            if ($setting['data_type_id'] == DataType::FILE && !empty($settingsData[$setting['id']])) {
                if (isset($settingsData[$setting['id']])) {
                    if (is_array($settingsData[$setting['id']])) {
                        //@todo здесь переделать с цикла
                        foreach ($settingsData[$setting['id']] as $key => $datum) {
                            if (!empty((int)$datum)) {
                                $file = File::find((int)$datum['value']);
                                $settingsData[$setting['id']][$key]['file'] = !empty($file) ? $file->toArray() : [];
                            }
                        }
                    } else {
                        if (!empty($settingsData[$setting['id']][0])) {
                            $file = File::find((int)$settingsData[$setting['id']][0]['value']);
                            $settingsData[$setting['id']][0]['file'] = !empty($file) ? $file->toArray() : [];
                        }
                    }
                }
            }
            if ($setting['data_type_id'] == DataType::USER) {
                $settingsList[$slId]['options'] = $users;
            }
        }
        $data = [
            'user' => $user,
            'userGroups' => UserGroup::where('user_id', $user->id)->get()->keyBy('group_id')->toArray(),
            'groups' => Group::active()->get()->toArray(),
            'settings' => $settingsList,
            'propertiesData' => $settingsData,
            'access' => [
                'edit' => Access::checkByAccess(AccessType::EDIT),
                'delete' => Access::checkByAccess(AccessType::DELETE)
            ]
        ];

        return view('admin.users.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UserUpdate $request
     * @param  \App\User $user
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function update(UserUpdate $request, User $user)
    {
        $userId = Auth::id();
        $item = User::find($user->id);
        $item->name = $request->name;
        $item->email = $request->email;
        if (!empty($request->password))
            $item->password = Hash::make($request->password);
        $item->active = (boolean)$request->active;
        $item->system = (boolean)$request->system;
        $item->sort = (integer)$request->sort;
        $item->created_by = $userId;
        $item->modified_by = $userId;
        $item->save();

        if ($item->id) {
            $row = User::find($item->id);
            $row->groups()->delete();
            if (!empty($request->get('groups'))) {
                $groups = [];
                foreach ($request->get('groups') as $group) {
                    $groups[] = [
                        'user_id' => $item->id,
                        'group_id' => $group
                    ];
                }
                UserGroup::insert($groups);
            }

            $settingsForm = $request->get('properties');
            $files = $request->file('properties.files');

            $settingListObject = SettingListObject::where('code', SettingListObject::USER)->get()->first();
            $settings = SettingList::where('setting_list_object_id', $settingListObject['id'])
                ->active()
                ->orderBy('sort')
                ->get()
                ->keyBy('id')
                ->toArray();

            if (!empty($settings)) {
                foreach ($settings as $id => $setting) {
                    $value = isset($settingsForm[$id]) ? $settingsForm[$id] : null;
                    $valuesDB = UserSetting::where(['user_id' => $item->id, 'setting_list_id' => $id])
                        ->get()
                        ->keyBy('id')
                        ->toArray();

                    $pFiles = null;
                    if ($setting['data_type_id'] === DataType::FILE) {
                        $pFiles = isset($files[$id]) ? $files[$id] : null;
                    }
                    if (isset($value) || !empty($pFiles)) {
                        /**
                         * Обработка множественных
                         */
                        if ($setting['multiple']) {
                            /**
                             * Если вдруг не массив
                             */
                            if (!is_array($value))
                                $value = [$value];

                            if (!empty($value)) {
                                foreach ($value as $k => $v) {
                                    if (isset($v) && strlen($v) > 0) {
                                        $epItem = UserSetting::firstOrCreate(
                                            [
                                                'user_id' => $item->id,
                                                'setting_list_id' => $id,
                                                'value' => $v
                                            ],
                                            [
                                                'user_id' => $item->id,
                                                'setting_list_id' => $id,
                                                'value' => $v,
                                                'sort' => ($k + 1) * 100,
                                            ]
                                        );
                                        /**
                                         * Удалим из массива старых значений
                                         */
                                        unset($valuesDB[$epItem->id]);
                                    }
                                }
                            } else {
                                UserSetting::where(['user_id' => $item->id, 'setting_list_id' => $id])->delete();
                            }
                            /**
                             * Обработка файлов
                             */
                            if (!empty($pFiles)) {
                                foreach ($pFiles as $k => $pFile) {
                                    $v = null;
                                    if ($pFile->isValid()) {
                                        $file = $pFile;
                                        $fileName = $file->getClientOriginalName();
                                        $path = $file->store('public');
                                        $fileItem = new File;
                                        $fileItem->name = $fileName;
                                        $fileItem->path = $path;
                                        $fileItem->created_by = $userId;
                                        $fileItem->modified_by = $userId;
                                        $fileItem->save();
                                        $v = $fileItem->id;
                                    }
                                    if (!empty($v)) {
                                        UserSetting::create(
                                            [
                                                'user_id' => $item->id,
                                                'setting_list_id' => $id,
                                                'value' => $v,
                                                'sort' => ($k + 1 + count($valuesDB)) * 100
                                            ]
                                        );
                                    }
                                }
                            }
                            /**
                             * тут надо удалять не пришедшие с формы значения (удалены на форме)
                             */
                            UserSetting::destroy(array_keys($valuesDB));
                        } /**
                         * Обработка НЕ множественных свойств
                         */
                        else {
                            /**
                             * По какой-то причине может быть массив
                             */
                            if (is_array($value))
                                $value = array_first($value);
                            /**
                             * Обработка файлов
                             */
                            if ($setting['data_type_id'] === DataType::FILE) {
                                /**
                                 * Если файл пришел с формы, то заменить текущий (если он есть)
                                 *
                                 * Если файлов с формы нет, то значение для параметра:
                                 * - либо пришло с формы (если файл не менялся)
                                 * - либо пустое, если файл удален
                                 */
                                if (!empty($files[$id])) {
                                    if ($files[$id]->isValid()) {
                                        $file = $files[$id];
                                        $fileName = $file->getClientOriginalName();
                                        $path = $file->store('public');
                                        $fileItem = new File;
                                        $fileItem->name = $fileName;
                                        $fileItem->path = $path;
                                        $fileItem->created_by = $userId;
                                        $fileItem->modified_by = $userId;
                                        $fileItem->save();
                                        $value = $fileItem->id;
                                    }
                                }
                            }

                            if (isset($value) && strlen($value) > 0) {
                                UserSetting::updateOrCreate(
                                    [
                                        'user_id' => $item->id,
                                        'setting_list_id' => $id,
                                    ],
                                    [
                                        'user_id' => $item->id,
                                        'setting_list_id' => $id,
                                        'value' => $value,
                                    ]
                                );
                            } else {
                                //@todo по хорошему тут надо удалять файлы из таблицы
                                UserSetting::where(['user_id' => $item->id, 'setting_list_id' => $id])->delete();
                            }
                        }
                    } /**
                     * Если нет данных с формы по свойству, то удалять
                     */
                    else {
                        //@todo по хорошему тут надо удалять файлы из таблицы
                        UserSetting::where(['user_id' => $item->id, 'setting_list_id' => $id])->delete();
                    }
                }
            }
        }
        Event::dispatch(new EventUpdate($item));
        return Redirect::route('admin-users.edit', $user->id)->with('flash_message', trans('admin.message.changes-saved'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy()
    {
        $itemsForDelete = \Request::get('setDelete');
        if (!empty($itemsForDelete)) {
            foreach ($itemsForDelete as $id) {
                if ((int)$id !== 1) {
                    User::destroy($id);
                    event(new UserDestroy(User::withTrashed()->find($id)));
                }
            }
        }
        return Redirect::route('admin-users.index')->with('flash_message', trans('admin.message.records-deleted'));
    }
}
