<?php

namespace App\Http\Controllers\Admin;

use App\AccessType;
use App\Cms\Access;
use App\DataType;
use App\ElementProperty;
use App\Entity;
use App\EntityElement;
use App\Events\PropertyDestroy;
use App\Http\Requests\PropertyStore;
use App\Http\Requests\PropertyUpdate;
use App\Property;
use App\PropertyRelation;
use App\PropertyValue;
use App\SettingList;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Redirect;

class PropertiesController extends MainController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($entityId)
    {
        $entity = Entity::active()->with('entityType')->find($entityId);
        if (empty($entity))
            abort(404);
        $data['entity'] = $entity->toArray();
        $data['entity_id'] = $entityId;
        $data['dataTypes'] = DataType::active()
            ->orderBy('sort')
            ->get();
        return view('admin.properties.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PropertyStore $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(PropertyStore $request)
    {
        $userId = Auth::id();
        $item = new Property;
        $item->name = $request->name;
        $item->description = $request->description;
        $item->code = $request->code;
        $item->entity_id = $request->entity_id;
        $item->data_type_id = $request->data_type_id;
        $item->required = (boolean)$request->required;
        $item->multiple = (boolean)$request->multiple;
        $item->active = (boolean)$request->active;
        $item->system = (boolean)$request->system;
        $item->sort = $request->sort;
        $item->created_by = $userId;
        $item->save();

        if ($item->id) {
            $row = Property::find($item->id);
            if ((int)$item->data_type_id === DataType::SELECT) {
                $options = $request->get('options');
                if (!empty($options['name'])) {
                    foreach ($options['name'] as $key => $name) {
                        if (!empty($name)) {
                            $row->options()->create([
                                'name' => $name,
                                'code' => !empty($options['code'][$key]) ? $options['code'][$key] : md5($name),
                                'sort' => !empty($options['sort'][$key]) ? $options['sort'][$key] : 10 * ($key + 1),
                                'created_by' => $userId,
                                'modified_by' => $userId
                            ]);
                        }
                    }
                }
            }
            if ((int)$item->data_type_id === DataType::ENTITY) {
                if (isset($request->relations) && !empty($request->relations)) {
                    foreach ($request->relations as $key => $value) {
                        $row->relations()->create([
                            'value' => (int)$value
                        ]);
                    }
                }
            }
        }
        return Redirect::route('admin-entities.edit', ['id' => $item->entity_id])->with('flash_message', trans('admin.message.record-added'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Property::with('dataType')
            ->with('options')
            ->with('relations')
            ->find($id);
        $entity = Entity::active()->with('entityType')->find($data['entity_id']);
        if (empty($entity))
            abort(404);
        $data['entity'] = $entity->toArray();
        $data['dataTypes'] = DataType::active()
            ->orderBy('sort')
            ->get();
        $data['relationsByValue'] = collect($data['relations'])->keyBy('value');
        return view('admin.properties.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param PropertyUpdate $request
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function update(PropertyUpdate $request, $id)
    {
        $item = Property::find($id);
        $userId = Auth::id();
        $item->name = $request->name;
        $item->description = $request->description;
        $item->code = $request->code;
        $item->entity_id = $request->entity_id;
        $item->data_type_id = $request->data_type_id;
        $item->multiple = (boolean)$request->multiple;
        $item->required = (boolean)$request->required;
        $item->active = (boolean)$request->active;
        $item->system = (boolean)$request->system;
        $item->sort = (integer)$request->sort;
        $item->created_by = $userId;
        $item->modified_by = $userId;
        $item->save();

        $row = Property::find($id);
        if ((int)$item->data_type_id === DataType::SELECT) {
            $options = $request->get('options');
            $optionsDB = PropertyValue::where('property_id', $id)
                ->get()
                ->keyBy('id')
                ->toArray();

            if (!empty($options['name'])) {
                foreach ($options['name'] as $key => $name) {
                    if (!empty($name)) {
                        /**
                         * Если это ранее заполненые значения, то обновить
                         */
                        if (!empty($options['ids'][$key])) {
                            $option = PropertyValue::find($options['ids'][$key]);

                            $option->name = $name;
                            if (!empty($options['code'][$key]))
                                $option->code = $options['code'][$key];
                            if (!empty($options['sort'][$key]))
                                $option->sort = $options['sort'][$key];

                            $option->save();

                            /**
                             * Удалим из массива старых значений
                             */
                            unset($optionsDB[$options['ids'][$key]]);
                        } /**
                         * записать новые значения
                         */
                        else {
                            $row->options()->create([
                                'name' => $name,
                                'code' => !empty($options['code'][$key]) ? $options['code'][$key] : md5($name),
                                'sort' => !empty($options['sort'][$key]) ? $options['sort'][$key] : 500
                            ]);
                        }
                    }
                }

                /**
                 * Удалить те, что не пришли с формы (удалены)
                 */
                if (!empty($optionsDB)) {
                    foreach ($optionsDB as $key => $opt) {
                        ElementProperty::where('property_id', $id)->where('value', (string)$key)->delete();
                        PropertyValue::destroy($key);
                    }
                }
            }
        }
        if ((int)$item->data_type_id === DataType::ENTITY) {

            $relations = $request->get('relations');

            /**
             * Если есть значения, то получить те, что есть в БД, сравнить
             *
             */
            if (!empty($relations)) {
                $relationsDB = array_column(PropertyRelation::where('property_id', $id)
                    ->get()
                    ->toArray(), 'value', 'id');

                foreach ($relations as $relationId) {
                    if (!empty((int)$relationId)) {
                        /**
                         * Если нет такой записи, то добавить
                         */
                        if (!in_array($relationId, $relationsDB)) {
                            $row->relations()->create([
                                'property_id' => $id,
                                'value' => $relationId
                            ]);
                        }
                        /**
                         * Если есть (т.е. совпадает то, что в бд и пришло с формы), то удалить из массива (обработано)
                         */
                        if (in_array($relationId, $relationsDB))
                            unset($relationsDB[array_search($relationId, $relationsDB)]);
                    }
                }
                /**
                 * Удалить те, что не пришли с формы (удалены) - в массиве должны остаться те,
                 * что не совпали с данными с формы
                 */
                if (!empty($relationsDB)) {
                    foreach ($relationsDB as $rId => $entityId) {
                        /**
                         * Ищем элементы, принадлежащие сущности
                         */
                        $elIds = array_column(EntityElement::select(['id'])
                            ->where('entity_id', $entityId)
                            ->get()
                            ->toArray(), (string)'id', 'id');
                        /**
                         * Преобразовать id в строку, т.к. в таблице значений свойств элементов value - текст
                         */
                        $collection = collect($elIds);
                        $collection->transform(function ($id) {
                            return (string)$id;
                        });
                        $elIds = $collection->all();
                        /**
                         * Если есть элементы, то найди и удалить все записи по свойству, которые настроены
                         * на элементы сущности, которая удаляется
                         */
                        if (!empty($elIds)) {
                            ElementProperty::where('property_id', $id)->whereIn('value', $elIds)->delete();
                        }
                        PropertyRelation::destroy($rId);
                    }
                }

            } /**
             * Если ничего нет, то просто удалить параметр и значения в соответствующей таблице
             */
            else {
                ElementProperty::where('property_id', $id)->delete();
                $row->relations()->delete();
            }
        }
        return Redirect::route('admin-entities.edit', ['id' => $item->entity_id])->with('flash_message', trans('admin.message.record-changed'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        Property::destroy($id);
        $item = Property::withTrashed()->find($id);
        Event::dispatch(new PropertyDestroy($item));
        return Redirect::route('admin-entities.edit', ['id' => $item->entity_id])->with('flash_message', trans('admin.message.record-deleted'));
    }
}
