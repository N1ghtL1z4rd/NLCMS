<?php

namespace App\Http\Controllers\Admin;

use App\AccessType;
use App\Cms\Access;
use App\File;
use App\DataType;
use App\SettingList;
use App\SettingListObject;
use App\SiteSetting;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;

class SiteSettingsController extends MainController
{
    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $settingListObject = SettingListObject::where('code', SettingListObject::SITE)->get()->first();
        $settingsList = SettingList::with('options')
            ->with('relations')
            ->where('setting_list_object_id', $settingListObject['id'])
            ->active()
            ->orderBy('sort')
            ->get()
            ->keyBy('id')
            ->toArray();
        $settingsData = SiteSetting::orderBy('setting_list_id')
            ->orderBy('sort')
            ->get()
            ->groupBy('setting_list_id')
            ->toArray();
        $users = User::all()->keyBy('id')->toArray();
        foreach ($settingsList as $slId => $setting) {
            if ($setting['data_type_id'] == DataType::FILE && !empty($settingsData[$setting['id']])) {
                if (isset($settingsData[$setting['id']])) {
                    if (is_array($settingsData[$setting['id']])) {
                        //@todo здесь переделать с цикла
                        foreach ($settingsData[$setting['id']] as $key => $datum) {
                            if (!empty((int)$datum)) {
                                $file = File::find((int)$datum['value']);
                                $settingsData[$setting['id']][$key]['file'] = !empty($file) ? $file->toArray() : [];
                            }
                        }
                    } else {
                        if (!empty($settingsData[$setting['id']][0])) {
                            $file = File::find((int)$settingsData[$setting['id']][0]['value']);
                            $settingsData[$setting['id']][0]['file'] = !empty($file) ? $file->toArray() : [];
                        }
                    }
                }
            }
            if ($setting['data_type_id'] == DataType::USER) {
                $settingsList[$slId]['options'] = $users;
            }
        }
        $data = [
            'items' => $settingsList,
            'propertiesData' => $settingsData,
            'access' => [
                'edit' => Access::checkByAccess(AccessType::EDIT)
            ]
        ];
        return view('admin.site-settings.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return void
     */
    public function edit()
    {
        $settingListObject = SettingListObject::where('code', SettingListObject::SITE)->get()->first();
        $settingsList = SettingList::with('options', 'relations')
            ->where('setting_list_object_id', $settingListObject['id'])
            ->active()
            ->orderBy('sort')
            ->get()
            ->keyBy('id')
            ->toArray();
        $settingsData = SiteSetting::orderBy('setting_list_id')
            ->orderBy('sort')
            ->get()
            ->groupBy('setting_list_id')
            ->toArray();
        $users = User::all()->keyBy('id')->toArray();
        foreach ($settingsList as $slId => $setting) {
            if ($setting['data_type_id'] == DataType::FILE && !empty($settingsData[$setting['id']])) {
                if (isset($settingsData[$setting['id']])) {
                    if (is_array($settingsData[$setting['id']])) {
                        //@todo здесь переделать с цикла
                        foreach ($settingsData[$setting['id']] as $key => $datum) {
                            if (!empty((int)$datum)) {
                                $file = File::find((int)$datum['value']);
                                $settingsData[$setting['id']][$key]['file'] = !empty($file) ? $file->toArray() : [];
                            }
                        }
                    } else {
                        if (!empty($settingsData[$setting['id']][0])) {
                            $file = File::find((int)$settingsData[$setting['id']][0]['value']);
                            $settingsData[$setting['id']][0]['file'] = !empty($file) ? $file->toArray() : [];
                        }
                    }
                }
            }
            if ($setting['data_type_id'] == DataType::USER) {
                $settingsList[$slId]['options'] = $users;
            }
        }
        $data = [
            'items' => $settingsList,
            'propertiesData' => $settingsData,
        ];
        return view('admin.site-settings.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function update(Request $request)
    {
        $userId = Auth::id();
        $settingListObject = SettingListObject::where('code', SettingListObject::SITE)->get()->first();
        $settings = SettingList::where('setting_list_object_id', $settingListObject['id'])
            ->active()
            ->orderBy('sort')
            ->get()
            ->keyBy('id')
            ->toArray();

        $settingsForm = $request->get('properties');
        $files = $request->file('properties.files');

        if (!empty($settings)) {
            foreach ($settings as $id => $setting) {
                $value = isset($settingsForm[$id]) ? $settingsForm[$id] : null;
                $valuesDB = SiteSetting::where('setting_list_id', $id)
                    ->get()
                    ->keyBy('id')
                    ->toArray();

                $pFiles = null;
                if ($setting['data_type_id'] === DataType::FILE) {
                    $pFiles = isset($files[$id]) ? $files[$id] : null;
                }
                if (isset($value) || !empty($pFiles)) {
                    /**
                     * Обработка множественных
                     */
                    if ($setting['multiple']) {
                        /**
                         * Если вдруг не массив
                         */
                        if (!is_array($value))
                            $value = [$value];

                        if (!empty($value)) {
                            foreach ($value as $k => $v) {
                                if (isset($v) && strlen($v) > 0) {
                                    $epItem = SiteSetting::firstOrCreate(
                                        [
                                            'setting_list_id' => $id,
                                            'value' => $v
                                        ],
                                        [
                                            'setting_list_id' => $id,
                                            'value' => $v,
                                            'sort' => ($k + 1) * 100,
                                            'modified_by' => $userId
                                        ]
                                    );
                                    /**
                                     * Удалим из массива старых значений
                                     */
                                    unset($valuesDB[$epItem->id]);
                                }
                            }
                        } else {
                            SiteSetting::where('setting_list_id', $id)->delete();
                        }
                        /**
                         * Обработка файлов
                         */
                        if (!empty($pFiles)) {
                            foreach ($pFiles as $k => $pFile) {
                                $v = null;
                                if ($pFile->isValid()) {
                                    $file = $pFile;
                                    $fileName = $file->getClientOriginalName();
                                    $path = $file->store('public');
                                    $fileItem = new File;
                                    $fileItem->name = $fileName;
                                    $fileItem->path = $path;
                                    $fileItem->created_by = $userId;
                                    $fileItem->modified_by = $userId;
                                    $fileItem->save();
                                    $v = $fileItem->id;
                                }
                                if (!empty($v)) {
                                    SiteSetting::create(
                                        [
                                            'setting_list_id' => $id,
                                            'value' => $v,
                                            'sort' => ($k + 1 + count($valuesDB)) * 100,
                                            'modified_by' => $userId
                                        ]
                                    );
                                }
                            }
                        }
                        /**
                         * тут надо удалять не пришедшие с формы значения (удалены на форме)
                         */
                        SiteSetting::destroy(array_keys($valuesDB));
                    } /**
                     * Обработка НЕ множественных свойств
                     */
                    else {
                        /**
                         * По какой-то причине может быть массив
                         */
                        if (is_array($value))
                            $value = array_first($value);
                        /**
                         * Обработка файлов
                         */
                        if ($setting['data_type_id'] === DataType::FILE) {
                            /**
                             * Если файл пришел с формы, то заменить текущий (если он есть)
                             *
                             * Если файлов с формы нет, то значение для параметра:
                             * - либо пришло с формы (если файл не менялся)
                             * - либо пустое, если файл удален
                             */
                            if (!empty($files[$id])) {
                                if ($files[$id]->isValid()) {
                                    $file = $files[$id];
                                    $fileName = $file->getClientOriginalName();
                                    $path = $file->store('public');
                                    $fileItem = new File;
                                    $fileItem->name = $fileName;
                                    $fileItem->path = $path;
                                    $fileItem->created_by = $userId;
                                    $fileItem->modified_by = $userId;
                                    $fileItem->save();
                                    $value = $fileItem->id;
                                }
                            }
                        }

                        if (isset($value) && strlen($value) > 0) {
                            SiteSetting::updateOrCreate(
                                [
                                    'setting_list_id' => $id,
                                ],
                                [
                                    'setting_list_id' => $id,
                                    'value' => $value,
                                    'modified_by' => $userId
                                ]
                            );
                        } else {
                            //@todo по хорошему тут надо удалять файлы из таблицы
                            SiteSetting::where('setting_list_id', $id)->delete();
                        }
                    }
                } /**
                 * Если нет данных с формы по свойству, то удалять
                 */
                else {
                    //@todo по хорошему тут надо удалять файлы из таблицы
                    SiteSetting::where('setting_list_id', $id)->delete();
                }
            }
        }
        return redirect()->action('Admin\SiteSettingsController@edit')->with('flash_message', trans('admin.message.changes-saved'));
    }
}
