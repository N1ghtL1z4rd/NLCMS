<?php

namespace App\Http\Controllers\Admin;

use App\AccessType;
use App\Cms\Access;
use App\Events\SectionDestroy;
use App\Section;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Redirect;

class SectionsController extends MainController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'dtAjaxSettings' => [
                'url' => route('admin-sections.ajax'),
                'columns' => [
                    [
                        'data' => 'chActions',
                        'name' => 'chActions',
                        'title' => '<input type="checkbox" id="chSelectAllRowsForActions" onclick="selectAllRowsForActions(this)"/>',
                        'orderable' => false,
                        'class' => 'text-center'
                    ],
                    [
                        'data' => 'id',
                        'name' => 'id',
                        'title' => 'ID',
                    ],
                    [
                        'data' => 'name',
                        'name' => 'name',
                        'title' => 'Наименование',
                    ],
                    [
                        'data' => 'url',
                        'name' => 'url',
                        'title' => 'URL',
                    ],
                    [
                        'data' => 'active',
                        'name' => 'active',
                        'title' => 'Активность',
                        'class' => 'text-center'
                    ],
                    [
                        'data' => 'system',
                        'name' => 'system',
                        'title' => 'Системный',
                        'class' => 'text-center'
                    ],
                    [
                        'data' => 'sort',
                        'name' => 'sort',
                        'title' => 'Сортировка',
                        'class' => 'text-center'
                    ]
                ],
                'order' => [
                    [1, 'asc']
                ]
            ],
            'access' => [
                'edit' => Access::checkByAccess(AccessType::EDIT),
                'delete' => Access::checkByAccess(AccessType::DELETE)
            ]
        ];
        return view('admin.sections.index', $data);
    }

    public function ajax()
    {
        if (\Request::ajax()) {
            $select = [
                'id',
                'name',
                'url',
                'active',
                'system',
                'sort',
            ];
            /**
             * Номер страницы
             */
            $page = \Request::get('draw');
            /**
             * Количество записей на странице
             */
            $length = (int)\Request::get('length');
            /**
             * Смещение
             */
            $offset = \Request::get('start');
            $query = Section::select($select);
            /**
             * Если -1, то выводить все записи
             */
            $recordsFiltered = Section::count();
            if ($length !== -1)
                $query = $query->limit($length)->offset($offset);
            /**
             * Поисковый запрос
             */
            $search = \Request::get('search');
            /**
             * Найти все подходящие записи
             */
            if (isset($search['value']) && strlen($search['value']) > 0) {
                $querySearch = Section::select($select);
                foreach ($select as $sItem) {
                    $querySearch->orWhere($sItem, 'like', '%' . $search['value'] . '%');
                }
                $itemsSearchDB = $querySearch->get();
                $query->whereIn('id', $itemsSearchDB->pluck('id'));
                $recordsFiltered = $itemsSearchDB->count();
            }
            /**
             * Сортировка
             */
            $order = \Request::get('order');
            $columns = \Request::get('columns');
            if (!empty($order)) {
                foreach ($order as $oItem) {
                    if (isset($oItem['column']) && !empty($columns[$oItem['column']]['name']) && $columns[$oItem['column']]['name'] !== 'chActions')
                        $query->orderBy($columns[$oItem['column']]['name'], empty($oItem['dir']) ? 'asc' : $oItem['dir']);
                }
            }

            $itemsDB = $query->get();
            $items = collect([]);
            if ($itemsDB->isNotEmpty()) {
                $collection = collect($itemsDB);
                $items = $collection->map(function ($item) {
                    $item->chActions = '';
                    if ((bool)$item->system === false) {
                        $item->chActions = '<input type="checkbox" value="' . $item->id . '" name="setDelete[]"  onclick="selectRowForActions(this)"/>';
                        $item->name = '<a href="' . route('admin-sections.edit', ['id' => $item->id]) . '">' . $item->name . '</a>';
                    }
                    return $item;
                });
            }
            return \Response::json(
                [
                    "draw" => $page,
                    "recordsTotal" => Section::count(),
                    "recordsFiltered" => $recordsFiltered,
                    "data" => $items->isNotEmpty() ? $items->toArray() : []
                ]
            );
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.sections.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Throwable
     */
    public function store(Request $request)
    {
        $userId = Auth::user()->id;
        $item = new Section;
        $item->name = $request->name;
        $item->description = $request->description;
        $item->url = $request->url;
        $item->active = (boolean)$request->active;
        $item->system = (boolean)$request->system;
        $item->sort = (integer)$request->sort;
        $item->created_by = $userId;
        $item->modified_by = $userId;
        $item->saveOrFail();
        return Redirect::route('admin-sections.index')->with('flash_message', trans('admin.message.record-added'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [];
        return view('admin.sections.edit', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Section::find($id);
        return view('admin.sections.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Throwable
     */
    public function update(Request $request, $id)
    {
        $item = Section::find($id);
        $userId = Auth::user()->id;
        $item->name = $request->name;
        $item->description = $request->description;
        $item->url = $request->url;
        $item->active = (boolean)$request->active;
        $item->system = (boolean)$request->system;
        $item->sort = (integer)$request->sort;
        $item->modified_by = $userId;
        $item->save();
        return Redirect::route('admin-sections.edit', $id)->with('flash_message', trans('admin.message.changes-saved'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy()
    {
        $itemsForDelete = \Request::get('setDelete');
        if (!empty($itemsForDelete)) {
            foreach ($itemsForDelete as $id) {
                Section::destroy($id);
                event(new SectionDestroy(Section::withTrashed()->find($id)));
            }
        }
        return Redirect::route('admin-sections.index')->with('flash_message', trans('admin.message.records-deleted'));
    }
}
