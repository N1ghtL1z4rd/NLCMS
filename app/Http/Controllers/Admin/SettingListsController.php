<?php

namespace App\Http\Controllers\Admin;

use App\AccessType;
use App\Cms\Access;
use App\DataType;
use App\Entity;
use App\EntityElement;
use App\Events\SettingListDestroy;
use App\Http\Requests\SettingListStore;
use App\Http\Requests\SettingListUpdate;
use App\SettingList;
use App\SettingListObject;
use App\SettingListRelation;
use App\SettingListValue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Redirect;

class SettingListsController extends MainController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'dtAjaxSettings' => [
                'url' => route('admin-setting-lists.ajax'),
                'columns' => [
                    [
                        'data' => 'chActions',
                        'name' => 'chActions',
                        'title' => '<input type="checkbox" id="chSelectAllRowsForActions" onclick="selectAllRowsForActions(this)">',
                        'orderable' => false,
                        'class' => 'text-center'
                    ],
                    [
                        'data' => 'id',
                        'name' => 'id',
                        'title' => 'ID',
                    ],
                    [
                        'data' => 'name',
                        'name' => 'name',
                        'title' => 'Наименование',
                    ],
                    [
                        'data' => 'code',
                        'name' => 'code',
                        'title' => 'Символьный код',
                    ],
                    [
                        'data' => 'settingListObject',
                        'name' => 'settingListObject',
                        'title' => 'Объект',
                    ],
                    [
                        'data' => 'dataType',
                        'name' => 'dataType',
                        'title' => 'Тип данных',
                    ],
                    [
                        'data' => 'multiple',
                        'name' => 'multiple',
                        'title' => 'Множественное',
                        'class' => 'text-center'
                    ],
                    [
                        'data' => 'active',
                        'name' => 'active',
                        'title' => 'Активность',
                        'class' => 'text-center'
                    ],
                    [
                        'data' => 'system',
                        'name' => 'system',
                        'title' => 'Системный',
                        'class' => 'text-center'
                    ],
                    [
                        'data' => 'sort',
                        'name' => 'sort',
                        'title' => 'Сортировка',
                        'class' => 'text-center'
                    ]
                ],
                'order' => [
                    [1, 'asc']
                ]
            ],
            'access' => [
                'edit' => Access::checkByAccess(AccessType::EDIT),
                'delete' => Access::checkByAccess(AccessType::DELETE)
            ]
        ];
        return view('admin.setting-lists.index', $data);
    }

    public function ajax()
    {
        if (\Request::ajax()) {
            $select = [
                'id',
                'name',
                'code',
                'setting_list_object_id',
                'data_type_id',
                'multiple',
                'active',
                'system',
                'sort',
            ];
            /**
             * Номер страницы
             */
            $draw = \Request::get('draw');
            /**
             * Количество записей на странице
             */
            $length = (int)\Request::get('length');
            /**
             * Смещение
             */
            $offset = \Request::get('start');
            $query = SettingList::select();
            /**
             * Если -1, то выводить все записи
             */
            $recordsFiltered = SettingList::count();
            if ($length !== -1)
                $query = $query->limit($length)->offset($offset);
            /**
             * Поисковый запрос
             */
            $search = \Request::get('search');
            /**
             * Найти все подходящие записи
             */
            if (isset($search['value']) && strlen($search['value']) > 0) {
                $querySearch = SettingList::select($select);
                foreach ($select as $sItem) {
                    $querySearch->orWhere($sItem, 'like', "%{$search['value']}%");
                }
                $itemsSearchDB = $querySearch
                    ->orWhereHas('settingListObject', function ($sFirst) use ($search) {
                        $sFirst->where('name', 'like', "%{$search['value']}%");
                    })
                    ->orWhereHas('dataType', function ($sSecond) use ($search) {
                        $sSecond->where('name', 'like', "%{$search['value']}%");
                    })->get();

                $query->whereIn('setting_lists.id', $itemsSearchDB->pluck('id'));
                $recordsFiltered = $itemsSearchDB->count();
            }
            /**
             * Сортировка
             */
            $order = array_first(\Request::get('order'));
            $columns = \Request::get('columns');
            if (!empty($order)) {
                if (isset($order['column']) && !empty($columns[$order['column']]['name'])) {
                    if (!in_array($columns[$order['column']]['name'], ['chActions', 'settingListObject', 'dataType'])) {
                        $query->orderBy($columns[$order['column']]['name'], empty($order['dir']) ? 'asc' : $order['dir']);
                    } /**
                     * Сортировка по наименованию объекта настроек (столбец "Объект")
                     */
                    elseif ($columns[$order['column']]['name'] === 'settingListObject') {
                        $query
                            ->join('setting_list_objects', 'setting_lists.setting_list_object_id', '=', 'setting_list_objects.id')
                            ->orderBy('setting_list_objects.name', empty($order['dir']) ? 'asc' : $order['dir']);
                    } /**
                     * Сортировка по наименованию типа данных (столбец "Тип данных")
                     */
                    elseif ($columns[$order['column']]['name'] === 'dataType') {
                        $query
                            ->join('data_types', 'setting_lists.data_type_id', '=', 'data_types.id')
                            ->orderBy('data_types.name', empty($order['dir']) ? 'asc' : $order['dir']);
                    }
                }
            }
            $itemsDB = $query->with(['dataType', 'settingListObject'])->get();
            $items = collect([]);
            if ($itemsDB->isNotEmpty()) {
                $collection = collect($itemsDB);
                $items = $collection->map(function ($item) {
                    $item->chActions = '';
                    if ((bool)$item->system === false) {
                        $item->chActions = '<input type="checkbox" value="' . $item->id . '" name="setDelete[]"  onclick="selectRowForActions(this)"/>';
                        $item->name = '<a href="' . route('admin-setting-lists.edit', ['id' => $item->id]) . '">' . $item->name . '</a>';
                    }
                    $item->settingListObject = $item->settingListObject->name;
                    $item->dataType = $item->dataType->name;
                    return $item;
                });
            }
            return \Response::json(
                [
                    "draw" => $draw,
                    "recordsTotal" => SettingList::count(),
                    "recordsFiltered" => $recordsFiltered,
                    "data" => $items->isNotEmpty() ? $items->toArray() : []
                ]
            );
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['settingListObjects'] = SettingListObject::active()
            ->orderBy('sort')
            ->get();
        $data['dataTypes'] = DataType::active()
            ->orderBy('sort')
            ->get();
        $data['entities'] = Entity::active()
            ->orderBy('sort')
            ->get();
        return view('admin.setting-lists.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Throwable
     */
    public function store(SettingListStore $request)
    {
        $userId = Auth::id();
        $item = new SettingList;
        $item->name = $request->name;
        $item->description = $request->description;
        $item->code = $request->code;
        $item->setting_list_object_id = $request->setting_list_object_id;
        $item->data_type_id = $request->data_type_id;
        $item->multiple = (boolean)$request->multiple;
        $item->active = (boolean)$request->active;
        $item->system = (boolean)$request->system;
        $item->sort = (integer)$request->sort;
        $item->created_by = $userId;
        $item->modified_by = $userId;
        $item->save();

        if ($item->id) {
            $row = SettingList::find($item->id);
            if ((int)$item->data_type_id === DataType::SELECT) {
                $options = $request->get('options');
                if (!empty($options['name'])) {
                    foreach ($options['name'] as $key => $name) {
                        if (!empty($name)) {
                            $row->options()->create([
                                'name' => $name,
                                'code' => !empty($options['code'][$key]) ? $options['code'][$key] : md5($name),
                                'sort' => !empty($options['sort'][$key]) ? $options['sort'][$key] : 10 * ($key + 1)
                            ]);
                        }
                    }
                }
            }
            if ((int)$item->data_type_id === DataType::ENTITY) {
                if (isset($request->relations) && !empty($request->relations)) {
                    foreach ($request->relations as $key => $value) {
                        $row->relations()->create([
                            'value' => (int)$value
                        ]);
                    }
                }
            }
        }
        return Redirect::route('admin-setting-lists.index')->with('flash_message', trans('admin.message.record-added'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = SettingList::with('settingListObject')
            ->with('dataType')
            ->with('options')
            ->with('relations')
            ->find($id);
        $data['settingListObjects'] = SettingListObject::active()
            ->orderBy('sort')
            ->get();
        $data['dataTypes'] = DataType::active()
            ->orderBy('sort')
            ->get();
        $data['entities'] = Entity::active()
            ->orderBy('sort')
            ->get();
        $data['relationsByValue'] = collect($data['relations'])->keyBy('value');
        return view('admin.setting-lists.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Throwable
     */
    public function update(SettingListUpdate $request, $id)
    {
        $item = SettingList::find($id);
        $userId = Auth::id();
        $item->name = $request->name;
        $item->description = $request->description;
        $item->code = $request->code;
        $item->setting_list_object_id = $request->setting_list_object_id;
        $item->data_type_id = $request->data_type_id;
        $item->multiple = (boolean)$request->multiple;
        $item->active = (boolean)$request->active;
        $item->sort = (integer)$request->sort;
        $item->created_by = $userId;
        $item->modified_by = $userId;
        $item->save();

        if ($id) {
            $row = SettingList::find($id);
            $relationModel = SettingListObject::find($item->setting_list_object_id)['model'];
            if ((int)$item->data_type_id === DataType::SELECT) {
                $options = $request->get('options');
                $optionsDB = SettingListValue::where('setting_list_id', $id)
                    ->get()
                    ->keyBy('id')
                    ->toArray();

                if (!empty($options['name'])) {
                    foreach ($options['name'] as $key => $name) {
                        if (!empty($name)) {
                            /**
                             * Если это ранее заполненые значения, то обновить
                             */
                            if (!empty($options['ids'][$key])) {
                                $option = SettingListValue::find($options['ids'][$key]);

                                $option->name = $name;
                                if (!empty($options['code'][$key]))
                                    $option->code = $options['code'][$key];
                                if (!empty($options['sort'][$key]))
                                    $option->sort = $options['sort'][$key];

                                $option->save();

                                /**
                                 * Удалим из массива старых значений
                                 */
                                unset($optionsDB[$options['ids'][$key]]);
                            } /**
                             * записать новые значения
                             */
                            else {
                                $row->options()->create([
                                    'name' => $name,
                                    'code' => !empty($options['code'][$key]) ? $options['code'][$key] : md5($name),
                                    'sort' => !empty($options['sort'][$key]) ? $options['sort'][$key] : 10 * ($key + 1)
                                ]);
                            }
                        }
                    }

                    /**
                     * Удалить те, что не пришли с формы (удалены)
                     */
                    if (!empty($optionsDB)) {
                        foreach ($optionsDB as $key => $opt) {
                            $relationModel::where('setting_list_id', $id)->where('value', $key)->delete();
                            SettingListValue::destroy($key);
                        }
                    }
                }
            }
            if ((int)$item->data_type_id === DataType::ENTITY) {
                $relations = $request->get('relations');
                /**
                 * Если есть значения, то получить те, что есть в БД, сравнить
                 */
                if (!empty($relations)) {
                    $relationsDB = array_column(SettingListRelation::where('setting_list_id', $id)
                        ->get()
                        ->toArray(), 'value', 'id');

                    foreach ($relations as $relationId) {
                        if (!empty((int)$relationId)) {
                            /**
                             * Если нет такой записи, то добавить
                             */
                            if (!in_array($relationId, $relationsDB)) {
                                $row->relations()->create([
                                    'setting_list_id' => $id,
                                    'value' => $relationId
                                ]);
                            }
                            /**
                             * Если есть (т.е. совпадает то, что в бд и пришло с формы), то удалить из массива (обработано)
                             */
                            if (in_array($relationId, $relationsDB))
                                unset($relationsDB[array_search($relationId, $relationsDB)]);
                        }
                    }
                    /**
                     * Удалить те, что не пришли с формы (удалены) - в массиве должны остаться те,
                     * что не совпали с данными с формы
                     */
                    if (!empty($relationsDB)) {
                        foreach ($relationsDB as $rId => $entityId) {
                            /**
                             * Ищем элементы, принадлежащие сущности
                             */
                            $elIds = array_column(EntityElement::select(['id'])
                                ->where('entity_id', $entityId)
                                ->get()
                                ->toArray(), 'id', 'id');
                            /**
                             * Если есть элементы, то найди и удалить все записи по параметру, которые настроены
                             * на элементы сущности, которая удаляется
                             */
                            if (!empty($elIds)) {
                                $relationModel::where('setting_list_id', $id)->whereIn('value', $elIds)->delete();
                            }
                            SettingListRelation::destroy($rId);
                        }
                    }

                } /**
                 * Если ничего нет, то просто удалить параметр и значения в соответствующей таблице
                 */
                else {
                    $relationModel::where('setting_list_id', $id)->delete();
                    $row->relations()->delete();
                }
            }
        }
        return Redirect::route('admin-setting-lists.edit', $id)->with('flash_message', trans('admin.message.changes-saved'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy()
    {
        $itemsForDelete = \Request::get('setDelete');
        if (!empty($itemsForDelete)) {
            foreach ($itemsForDelete as $id) {
                SettingList::destroy($id);
                event(new SettingListDestroy(SettingList::withTrashed()->find($id)));
            }
        }
        return Redirect::route('admin-setting-lists.index')->with('flash_message', trans('admin.message.records-deleted'));
    }
}
