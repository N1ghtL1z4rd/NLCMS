<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class EntityElement
 *
 * @package App
 * @property integer id
 * @property string name
 * @property string code
 * @property integer extended_id
 * @property integer preview_img
 * @property string preview
 * @property integer detail_img
 * @property string detail
 * @property integer entity_id
 * @property boolean active
 * @property integer sort
 * @property integer created_by
 * @property integer modified_by
 * @property mixed created_at
 * @property mixed updated_at
 * @property mixed delete_at
 * @property-read \App\User $author
 * @property-read \App\User $editor
 * @property-read \App\Entity $entity
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ElementProperty[] $properties
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EntityElement active()
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\EntityElement onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\EntityElement withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\EntityElement withoutTrashed()
 * @mixin \Eloquent
 */
class EntityElement extends Model
{
    use SoftDeletes;

    /**
     * Атрибуты, которые должны быть преобразованы в даты.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * @var array
     */
    protected $dtTableColumns = [
        [
            'data' => 'id',
            'name' => 'id',
            'title' => 'ID',
            'orderable' => true,
            'default' => true,
            'class' => 'text-center'
        ],
        [
            'data' => 'name',
            'name' => 'name',
            'title' => 'Наименование',
            'orderable' => true,
            'default' => true,
        ],
        [
            'data' => 'code',
            'name' => 'code',
            'title' => 'Символьный код',
            'orderable' => true,
            'default' => false,
        ],
        [
            'data' => 'extended_id',
            'name' => 'extended_id',
            'title' => 'Расширенный ID',
            'orderable' => true,
            'default' => false,
        ],
        [
            'data' => 'preview_img',
            'name' => 'preview_img',
            'title' => 'Картинка для анонса',
            'orderable' => false,
            'default' => false,
        ],
        [
            'data' => 'preview',
            'name' => 'preview',
            'title' => 'Анонс',
            'orderable' => true,
            'default' => false,
        ],
        [
            'data' => 'detail_img',
            'name' => 'detail_img',
            'title' => 'Картинка детальная',
            'orderable' => false,
            'default' => false,
        ],
        [
            'data' => 'detail',
            'name' => 'detail',
            'title' => 'Текст детально',
            'orderable' => true,
            'default' => false,
        ],
        [
            'data' => 'active',
            'name' => 'active',
            'title' => 'Активность',
            'orderable' => true,
            'default' => true,
            'class' => 'text-center'
        ],
        [
            'data' => 'sort',
            'name' => 'sort',
            'title' => 'Сортировка',
            'orderable' => true,
            'default' => true,
            'class' => 'text-center'
        ],
        [
            'data' => 'created_by',
            'name' => 'created_by',
            'title' => 'Кто создал',
            'orderable' => true,
            'default' => false,
        ],
        [
            'data' => 'modified_by',
            'name' => 'modified_by',
            'title' => 'Кто изменил',
            'orderable' => true,
            'default' => false,
        ],
        [
            'data' => 'created_at',
            'name' => 'created_at',
            'title' => 'Дата создания',
            'orderable' => true,
            'default' => true,
        ],
        [
            'data' => 'updated_at',
            'name' => 'updated_at',
            'title' => 'Дата редактирования',
            'orderable' => true,
            'default' => true,
        ],
    ];

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('active', '=', true);
    }

    public function author()
    {
        return $this->hasOne('App\User', 'id', 'created_by');
    }

    public function editor()
    {
        return $this->hasOne('App\User', 'id', 'modified_by');
    }

    /**
     * Сущность, к которой принадлежит элемент
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function entity()
    {
        return $this->hasOne('App\Entity', 'id', 'entity_id');
    }

    /**
     * Значения свойств элемента
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function properties()
    {
        return $this->hasMany('App\ElementProperty', 'element_id', 'id');
    }

    /**
     * Удаленные значения свойств элемента
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function properties_trashed()
    {
        return $this->hasMany('App\ElementProperty', 'element_id', 'id')->withTrashed();
    }

    public function preview_img()
    {
        return $this->hasOne('App\File', 'id', 'preview_img');
    }

    public function detail_img()
    {
        return $this->hasOne('App\File', 'id', 'detail_img');
    }

    /**
     * Текстовый поиск по полям:
     *
     * - entity_elements.name
     * - entity_elements.description
     * - element_properties.value
     * - property_values.name
     * - ee.name
     *
     * @param $q - строка поиска с учетом регистра
     * @param bool $parameters - массив с параметрами фильтрации по таблице entity_elements:
     *
     * - entity_id
     * - active
     * - created_by
     * - modified_by
     *
     * пример:
     *
     * ['entity_id' => 10,'active' => true]
     *
     * @return \Illuminate\Support\Collection
     */
    public function search($q, $parameters = false)
    {
        $query = \DB::table('entity_elements')
            ->select(
                'entity_elements.id     as id',
                'entity_elements.entity_id      as entity_id',
                'element_properties.property_id as property_id',
                'properties.data_type_id        as data_type_id',
                'entity_elements.name           as name',
                'entity_elements.description    as description',
                'element_properties.value       as value',
                'property_values.name           as o_name',
                'ee.name                        as e_name'
            )
            /**
             * Значения свойств
             *
             * join "element_properties" on "entity_elements"."id" = "element_properties"."element_id" and
             * element_properties.deleted_at ISNULL
             */
            ->join('element_properties', function ($join) {
                $join->on('entity_elements.id', '=', 'element_properties.element_id')
                    ->whereNull('element_properties.deleted_at');
            })
            /**
             * Свойства
             *
             * join "properties" on "properties"."id" = "element_properties"."property_id" and
             * properties.deleted_at ISNULL
             */
            ->join('properties', function ($join) {
                $join->on('properties.id', '=', 'element_properties.property_id')
                    ->whereNull('properties.deleted_at');
            })
            /**
             * Записи для типа данных "Список"
             *
             * left join "property_values" on "property_values"."property_id" = "element_properties"."property_id" and
             * "property_values"."id"::VARCHAR = element_properties.value and
             * "properties"."data_type_id" = 7
             */
            ->leftJoin('property_values', function ($join) {
                $join->on('property_values.property_id', 'element_properties.property_id')
                    ->on(\DB::raw('"property_values"."id"::VARCHAR'), "element_properties.value")
                    ->on('properties.data_type_id', \DB::raw(DataType::SELECT));
            })
            /**
             * Записи для типа данных "Привязка к элементам сущностей"
             *
             *left join "entity_elements" as "ee" on "ee"."id"::VARCHAR = element_properties.value and
             * "properties"."data_type_id" = 8
             */
            ->leftJoin('entity_elements as ee', function ($join) {
                $join->on(\DB::raw('"ee"."id"::VARCHAR'), 'element_properties.value')
                    ->on('properties.data_type_id', \DB::raw(DataType::ENTITY));
            })
            ->whereNull('entity_elements.deleted_at')
            ->where('entity_elements.name', 'like', \DB::raw("'%$q%'"))
            ->orWhere('entity_elements.description', 'like', \DB::raw("'%$q%'"))
            ->orWhere([
                ['element_properties.value', 'like', \DB::raw("'%$q%'")],
                ['properties.data_type_id', '!=', \DB::raw(DataType::SELECT)],
                ['properties.data_type_id', '!=', \DB::raw(DataType::ENTITY)],
                ['properties.data_type_id', '!=', \DB::raw(DataType::FILE)],
            ])
            ->orWhere([
                ['property_values.name', 'like', \DB::raw("'%$q%'")],
                ['properties.data_type_id', \DB::raw(DataType::SELECT)],
            ])
            ->orWhere([
                ['ee.name', 'like', \DB::raw("'%$q%'")],
                ['properties.data_type_id2', \DB::raw(DataType::ENTITY)],
            ])
            ->orWhere('ee.name', 'like', \DB::raw("'%$q%'"));
        if ($parameters) {
            foreach ($parameters as $key => $parameter) {
                $query->where($key, $parameter);
            }
        }
        return $items = $query->get();
    }

    /**
     * @return array
     */
    public function getDtTableColumns(): array
    {
        return $this->dtTableColumns;
    }
}
