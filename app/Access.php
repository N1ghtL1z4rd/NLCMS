<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Access
 *
 * @package App
 * @property integer id
 * @property integer access_type_id
 * @property integer object_type_id
 * @property integer object_id
 * @property integer subject_type_id
 * @property integer subject_id
 * @property integer created_by
 * @property integer modified_by
 * @property mixed created_at
 * @property mixed updated_at
 * @property mixed deleted_at
 * @property-read \App\User $author
 * @property-read \App\User $editor
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Access onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Access withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Access withoutTrashed()
 * @mixin \Eloquent
 */
class Access extends Model
{
    use SoftDeletes;

    protected $table = 'access';

    /**
     * Атрибуты, которые должны быть преобразованы в даты.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * Атрибуты, для которых разрешено массовое назначение.
     *
     * @var array
     */
    protected $fillable = [
        'access_type_id',
        'object_type_id',
        'object_id',
        'subject_type_id',
        'subject_id',
        'created_by',
        'modified_by'
    ];

    public function author()
    {
        return $this->hasOne('App\User', 'id', 'created_by');
    }

    public function editor()
    {
        return $this->hasOne('App\User', 'id', 'modified_by');
    }

}
