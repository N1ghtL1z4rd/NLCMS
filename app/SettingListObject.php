<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SettingListObject
 *
 * @package App
 * @property integer id
 * @property string name
 * @property string description
 * @property string code
 * @property string model
 * @property string table
 * @property boolean active
 * @property boolean system
 * @property integer sort
 * @property integer created_by
 * @property integer modified_by
 * @property mixed created_at
 * @property mixed updated_at
 * @property mixed deleted_at
 * @property-read \App\User $author
 * @property-read \App\User $editor
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\SettingList[] $settingList
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SettingListObject active()
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\SettingListObject onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\SettingListObject withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\SettingListObject withoutTrashed()
 * @mixin \Eloquent
 */
class SettingListObject extends Model
{
    const SITE = 'SITE';
    const USER = 'USER';

    use SoftDeletes;

    /**
     * Атрибуты, которые должны быть преобразованы в даты.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('active', '=', true);
    }

    public function author()
    {
        return $this
            ->hasOne('App\User', 'id', 'created_by');
    }

    public function editor()
    {
        return $this
            ->hasOne('App\User', 'id', 'modified_by');
    }

    public function settingList()
    {
        return $this
            ->hasMany('App\SettingList', 'setting_list_object_id', 'id')
            ->orderBy('sort')
            ->with('dataType')
            ->with('options')
            ->with('relations');
    }
}
